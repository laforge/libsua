/***************************************************************************
                          main.cpp  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: main.cpp,v 1.1 2003/01/14 13:47:56 p82609 Exp $
 *
 * SUA Test user part implementation.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * Purpose: This code-file defines the SUA Test User Part application and  
 *          has functions for:
 *          - get single argument from commandline
 *          - get input commandline parameters
 *          - get own host parameters
 *          - main function
 */

#ifdef LINUX
    #include <unistd.h>
#endif

#include "sua.h"
#include "sctp.h"

#include "testuser.h"

#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <string>

#include <netinet/in_systm.h>
#include <netinet/ip.h>         /* includes <netinet/in.h> also ! */
#include <netinet/ip_icmp.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/utsname.h>

#ifdef HAVE_SYS_POLL_H
    #include <sys/poll.h>
#else
    #define POLLIN     0x001
    #define POLLPRI    0x002
    #define POLLOUT    0x004
    #define POLLERR    0x008
#endif

using namespace std;

char own_hostname[512];
int  nr_local_instances = 0; 

/***********************************************************************/
/* TEST USER PART: get argument from commandline                       */
/***********************************************************************/
string get_argument( char *argv)
{
  int num_of_char = 0;
  while  ((argv[num_of_char]) != '\0')
     { 
       // convert to lowercase characters for easier processing
       argv[num_of_char] = tolower(argv[num_of_char]);
       num_of_char++;
     }
  return(argv);
}

/***********************************************************************/
/* TEST USER PART: get input commandline parameters                    */
/***********************************************************************/
int sua_get_arguments(int argc, char **argv)
{
  string address_str;
  string nr_of_streams;
  bool help_flag = false;
  string sua_filename;
  char ch1,ch2;
  string sua_arg;
  int result;
  int num_of_arg = 0;
 
  /* copy all charachters from the comandline into a string */
  while (num_of_arg < argc)
  {
     sua_arg =get_argument(argv[num_of_arg]);
     
#ifdef DEBUG
     //cout << "num_of_arg =" << num_of_arg <<" " << sua_arg   << " \n";
#endif
     
     // look at each of the arguments
     if (num_of_arg == 0)
	 { 
           ch1 = 'i'; // ignore character
	 }
     else
       {
	 if (sua_arg[0] == '-')
	   {
	     ch1 = sua_arg[1];
	     ch2 = sua_arg[2];
#ifdef DEBUG
	     //cout << "character =" << ch1 <<" \n";
#endif 
	   }
	 else
	   {
	     cout << "weird option \n";
	     ch1 = ' '; 
	   }
       }

     switch (ch1)
       {       
       case 's':
	 { // source address option: read a ipv4/v6 address or hostname 
	   num_of_arg++;
	   	 
	   break;
	 }
       case 'd':
	 { // destination address option: read a ipv4/v6 address or hostname 
	   num_of_arg++;
	   	 
	   break;
	 }
       case 'u':
	 { // 
	   break;
	 }
       case 'm':
	 { //
	   break;
	 }
       case 'f':
	 { // file option: read a file with all the info needed
	   num_of_arg++;
	   sua_filename = argv[num_of_arg];
	   result =  sua_read_config_file( argv[num_of_arg]);
				  	  
	   break;
	 }
       case 'i':
         {
	   // ignore character, is not a error, go to next argument
	   break;
	 }
       default:
	 {
	   help_flag = true;
	   break;
	 }
       }
    


     num_of_arg++;
  }
  

  /* no input parameters, display the help text */
  if (help_flag) {
        cout << "usage: \n";
        cout << "  sua [-s sourceaddr1,sourceaddr2,... -l localPort]  \\ \n";
        cout << "      [-d destaddr1,destaddr2 ....-r remotePort]     \\ \n";
        cout << "      [-m number-of-streams] \\ \n";
        cout << "      [-f sua-address-filename] \\ \n";
	cout << "      [-n subsystem-number] \n";
        cout << "use -s option to define source address(es) as IPv4/6 numerical/(hex) addresses.\n";
        cout << "use -d option to define destination address(es) as IPv4/6 numerical/(hex) addresses.\n";
        cout << "use -l option to define the local port where inits are accepted.\n";
        cout << "use -r option to define the remote port where init is sent to.\n";
        cout << "use -n option to define the subsytem number used\n";
        cout << "use -m '#of streams' enter number of streams, default is 15 streams\n";
        cout << "for multihoming multiple source addresses can be entered after the -s option.\n";
        cout << "They must be separated by commas and no spaces may be included.\n";
        cout << "To establish more than one association, use the -f 'sua-address-filename'  file option \n";
        cout << "format  is as follows:\n";
        cout << "-d can be ommitted to start a sua that listens only for incoming inits\n";
        cout << "Default local/remote portnumber = 14001\n";
    }

  return(0);

} /* end of sua_get_arguments */

/***********************************************************************/
/* TEST USER PART: get own host parameters                             */
/***********************************************************************/
int tstup_get_host_parm()
{
  struct hostent  *hptr;
  struct utsname  myhostname;
  char            str[INET6_ADDRSTRLEN];
  char            **pptr;
  
  if (uname(&myhostname) < 0){
    cout << "Determination of own name failed\n";
    return(-1);
  }
  cout << "Own name = " << myhostname.nodename << "\n";
  if ((hptr = gethostbyname(myhostname.nodename)) == NULL){
    cout << "Determination of hostname failed\n";
    return (-2);
  }
  
  cout << "Host OS " << myhostname.sysname << " " << myhostname.release << " " << myhostname.version << "\n";
  cout << "Hostname = " << hptr->h_name << "\n";
  strcpy ( own_hostname, hptr->h_name );
  switch (hptr->h_addrtype){
  case AF_INET:
    cout << "Host ip address type = AF_INET\n";
    break;
  case AF_INET6:
    cout << "Host ip address type = AF_INET6\n";
    break;
  default:
    cout << "Host ip address type = " << hptr->h_addrtype << "\n";
    break;
  }
  cout << "Hostname ip address length = "<< hptr->h_length << "\n";
  pptr = hptr->h_addr_list;
  for ( ; *pptr != NULL;pptr++)
    {
      cout << inet_ntop(hptr->h_addrtype, *pptr, str,sizeof(str)) << "\n";
    }
  return(0);
}

/***********************************************************************/
/* TEST USER PART: main function                                       */
/***********************************************************************/
int main(int argc, char **argv  )
{
  int res;
  Sua_ULP_CallBacks  SUACallbackFunctions;

  cout << "Welcome to Test User Part(TSTUP) for SCCP User Adaptation Layer (SUA)\n";
  
  sua_initialisation();

  
  SUACallbackFunctions.ulp_ClDataIndNotif = &ulp_ClDataIndNotif; 
  SUACallbackFunctions.ulp_ConnIndNotif = &ulp_ConnIndNotif;
  SUACallbackFunctions.ulp_ConnConfIndNotif = &ulp_ConnConfIndNotif;
  SUACallbackFunctions.ulp_ConnDataIndNotif = &ulp_ConnDataIndNotif;
  SUACallbackFunctions.ulp_DisConnIndNotif = &ulp_DisConnIndNotif;
  
  // determine own hostname and address
  res = tstup_get_host_parm();

  // read cmdline paremeters (including a file containing cmds)
  res = sua_get_arguments(argc, argv);

  // register sua and its users

  // register all the local SUA together with their SCTP instances
  res = sua_registerInstance( SUACallbackFunctions
			    );
  // initiate all the SUA associations with their respective peers
  res = sua_associate();
  
  // register sua and its users
  res = sctp_registerUserCallback( fileno(stdin), 
				   &testip_stdin_cb,
				   NULL, 
				   POLLIN|POLLPRI
				 );

  cout << "sctp_registerUserCallback result = "<< res << "\n";

  init_testip_stdin();

  // go into infinite loop( = handle all events)
  cout << "Let Test User Part run free!!!! \n";
  while (sctp_eventLoop() >= 0);

  // close the program
  //close_logging_file();
  exit (0);

}

// end of module sua_main.c











