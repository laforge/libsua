/***************************************************************************
                          testuser.cpp  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: testuser.cpp,v 1.7 2003/09/09 08:44:40 p82609 Exp $
 *
 * SUA Test user part implementation.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * Purpose: This code-file defines the SUA Test User Part application and  
 *          has definitions for:
 *          - Do pingpong with remote TESTUP
 *          - Handle pong
 *          - initialise the Test User part application 
 *          - Test User Part STDIN function -> reading keyboard and 
 *            sending the info to SUA
 *          - Handle TESTUP expired timers
 *          - TESTUP Connectionless Data Ind Notification(from SUA)
 *          - TESTUP CO Connection Ind Notification(from SUA)
 *          - TESTUP CO Connection Confirmation Ind Notification(from SUA)
 *          - TESTUP CO Data Ind Notification(from SUA)
 *          - TESTUP CO DisConnection Ind Notification(from SUA)
 */

#include "sctp.h"
#include "sua.h"

#ifdef LINUX
    #include <unistd.h>
#endif

#include <iostream>
#include <string>




using namespace std;

#include <sys/time.h>
#define MAX_DATA_LENGTH 2000
#define MAX_DATACHUNK_PDU_LENGTH 251

extern char own_hostname[512];

//global variable for ULP instance(one connection only)
unsigned int timerID = 0;
unsigned int refID = 0;
unsigned int sendNo = 10;
unsigned int sendcnt = 0;
unsigned int timeoutval= 1000;
unsigned int rep_timer = 0;
boolean mirror = FALSE;

sua_Path_str pathinfo;  
sccp_addr_str calling_pty_addr,called_pty_addr;
short network_app = 0;
short seq_control_num = 0;
boolean return_option = FALSE;

string disp_ASP_stat[6];
string disp_PC_stat[2];


/***********************************************************************/
/* Test User Part : doPingPong                                         */
/***********************************************************************/
int doPingPong(unsigned int Sua_ConnId )
{
    int length;
    sccp_addr_str co_clg, co_cld;
    sccp_QOS_str co_QOS;
    unsigned int xsxx;
    string cmdline= "ping";

    co_QOS.prot_class = class2;
    co_QOS.in_sequence = false;
    co_QOS.sequence_number = 0;
    co_QOS.return_msg_on_error = 0;
    co_QOS.importance = 0;
    
    co_clg.address_fields_present.pc = no_pc_present;
    co_clg.address_fields_present.name_gt = no_name_present;
    co_clg.address_fields_present.ssn_port = no_sap_present;
    co_clg.address_fields_present.field_in_header = include_nothing;
    
    co_cld.address_fields_present.pc = no_pc_present;
    co_cld.address_fields_present.name_gt = no_name_present;
    co_cld.address_fields_present.ssn_port = no_sap_present;
    co_cld.address_fields_present.field_in_header = include_nothing;
    
    
    char *databuffer =  new char[cmdline.length()];
    cmdline.copy(databuffer, cmdline.length());
    length = cmdline.length();
    
    xsxx = Send_sua_primitive(N_DATA_REQ,
			      Sua_ConnId,
			      co_QOS,
			      co_cld,
			      co_clg,
			      databuffer,
			      length 
			     );

#ifdef DEBUG
    cout << "testuser.c++: Returned "<< xsxx <<" from the send (1==association error, 0==success, -1==could not send)\n";
#endif
    
    delete databuffer; 
    return(xsxx);
}

/***********************************************************************/
/* Test User Part : handle_Pong                                        */
/***********************************************************************/
int handlePong()
{


    return(0);
}
/***********************************************************************/
/* Test User Part : init_testip_stdin                                  */
/***********************************************************************/
void init_testip_stdin()
{
  unsigned int xsxx;

  disp_ASP_stat[0] = "ASPIDL";
  disp_ASP_stat[1] = "ASPDOW";
  disp_ASP_stat[2] = "ASPDHD";
  disp_ASP_stat[3] = "ASPIAC";
  disp_ASP_stat[4] = "ASPIHD";
  disp_ASP_stat[5] = "ASPACT";

  disp_PC_stat[0] = "UNAVAI";
  disp_PC_stat[1] = "AVAILA";

  xsxx=  sua_getPath( 1,
		      pathinfo
		    );

  /*cout << "result getpath = " << xsxx << "\n"; */

  calling_pty_addr.address_fields_present.pc = ipvx_pc_present;
  calling_pty_addr.address_fields_present.name_gt = no_name_present;
  calling_pty_addr.address_fields_present.ssn_port = ssn_present;
  calling_pty_addr.address_fields_present.field_in_header = include_nothing;
  calling_pty_addr.pc.ipvx = pathinfo.local_addr.pc.ipvx;
  calling_pty_addr.pc.ss7 = pathinfo.local_addr.pc.ss7;
  /*calling_pty_addr.name.GT = pathinfo.local_addr.name.GT;*/
  calling_pty_addr.ssn = pathinfo.local_addr.ssn;
  calling_pty_addr.routing_ind = route_on_ssn;
  calling_pty_addr.network_apperance = network_app;
   
  called_pty_addr.address_fields_present.pc = ipvx_pc_present;
  called_pty_addr.address_fields_present.name_gt = no_name_present;
  called_pty_addr.address_fields_present.ssn_port = ssn_present;
  called_pty_addr.address_fields_present.field_in_header = include_nothing;
  called_pty_addr.pc.ipvx = pathinfo.remote_addr.pc.ipvx;
  called_pty_addr.pc.ss7 = pathinfo.remote_addr.pc.ss7;
  called_pty_addr.ssn = pathinfo.remote_addr.ssn;
  called_pty_addr.routing_ind = route_on_ssn;
  called_pty_addr.network_apperance = network_app;
}

/***********************************************************************/
/* Test User Part : testip_disp_assoc_mngt                             */
/***********************************************************************/
void testip_disp_assoc_mngt()
{
  int i, res;
  char output_str[INET6_ADDRSTRLEN];
  const char *ptr;
  pointcode_str  local_pc;

  /* Display the management status of all  SUA instances */
  /* display the local pointcodes and IP address */

  res =  sua_get_sourcepc_addr(1, local_pc);
  cout << "Display SUA administration and Mangagement data of all associations\n";
  cout << "|----------------------------------------|\n";
  cout << "| Local IP addr       : ";
  if (local_pc.ipvx.sa.sa_family == AF_INET)
    { /* ipv4 */
      ptr = inet_ntop( local_pc.ipvx.sa.sa_family,
		       &local_pc.ipvx.sin.sin_addr,
		       output_str,
		       sizeof(output_str)
		       );
      cout.width(16);
      cout.fill(' ');
      cout << output_str;
      cout << " |";
    }
  else
    {	/* ipv6 */
      ptr = inet_ntop( local_pc.ipvx.sa.sa_family,
		       &local_pc.ipvx.sin6.sin6_addr,
		       output_str,
		       sizeof(output_str)
		       );
      cout.width(16);
      cout.fill(' ');
      cout << output_str << " |";
    }
  cout <<"\n";
  cout << "| Local SS7 pointcode : ";
  cout.width(6);
  cout.fill(' ');
  cout << local_pc.ss7.ITU14.pc ;
  cout << "           |\n";;
  cout << "|----------------------------------------|\n";

  /*display the adjacend remote pointcodes and IP addresses */
  cout << "|--------+--------+--------------------+-------+-----|\n";
  cout << "|Assoc-ID| status |   Remote IP addr   |  DPC  | SSN |\n";
  cout << "|--------+--------+--------------------+-------+-----|\n";
  i = 1;
  while ( SUA_PATH_NO_ERROR ==  sua_getPath(i,pathinfo) )
    {
      cout.width(3);
      cout.fill(' ');
      cout << "|   " << i ;
      cout.width(6);
      cout.fill(' ');
      cout <<"   | "<< disp_ASP_stat[pathinfo.ASP_status] << " |   ";
      if (pathinfo.remote_addr.pc.ipvx.sa.sa_family == AF_INET)
	{ /* ipv4 */
	  ptr = inet_ntop( pathinfo.remote_addr.pc.ipvx.sa.sa_family,
			   &pathinfo.remote_addr.pc.ipvx.sin.sin_addr,
			   output_str,
			   sizeof(output_str)
			   );
	  cout.width(16);
	  cout.fill(' ');
	  cout << output_str;
    
	  cout << " |";
	  cout.width(6);
	  cout.fill(' ');
	  cout <<  pathinfo.remote_addr.pc.ss7.ITU14.pc  << " | ";	
	  cout.width(3);
	  cout.fill(' ');
	  cout << pathinfo.remote_addr.ssn << " |\n";						
	}
      else
	{	/* ipv6 */
	  ptr = inet_ntop( pathinfo.remote_addr.pc.ipvx.sa.sa_family,
			   &pathinfo.remote_addr.pc.ipvx.sin6.sin6_addr,
			   output_str,
			   sizeof(output_str)
			   );		
	  cout << "| ";
	  cout.width(16);
	  cout.fill(' ');
	  cout << output_str << "   |";
	  cout.width(6);
	  cout.fill(' ');
	  cout << pathinfo.local_addr.pc.ss7.ITU14.pc  << " | ";	
	  cout.width(3);
	  cout.fill(' ');
	  cout << pathinfo.remote_addr.ssn << " |\n";	
	}						   			
      
      i++;
    }
  cout << "|--------+--------+--------------------+-------+-----|\n";

} 

/***********************************************************************/
/* Test User Part : testip_disp_dest_mngt                              */
/***********************************************************************/
void testip_disp_dest_mngt()
{
  int i, res;
  unsigned int assoc_id, stat;
  char output_str[INET6_ADDRSTRLEN];
  const char *ptr;
  pointcode_str  local_pc, dest_pc;

  res =  sua_get_sourcepc_addr(1, local_pc);

  /* Display the management status of all  SUA destinations */
  /* display the local pointcodes and IP address */
  cout << "Display SUA remote destination SS7 and IP pointcodes\n";
  cout << "|----------------------------------------|\n";
  cout << "| Local IP addr       : ";
  if (local_pc.ipvx.sa.sa_family == AF_INET)
    { /* ipv4 */
      ptr = inet_ntop(local_pc.ipvx.sa.sa_family,
		       &local_pc.ipvx.sin.sin_addr,
		       output_str,
		       sizeof(output_str)
		       );
      cout.width(16);
      cout.fill(' ');
      cout << output_str;
    
      cout << " |";
	
    }
  else
    {	/* ipv6 */
      ptr = inet_ntop( local_pc.ipvx.sa.sa_family,
		       &local_pc.ipvx.sin6.sin6_addr,
		       output_str,
		       sizeof(output_str)
		       );
  
      cout.width(16);
      cout.fill(' ');
      cout << output_str << "   |   ";
    }
  cout <<"\n";
  cout << "| Local SS7 pointcode : ";
  cout.width(6);
  cout.fill(' ');
  cout << local_pc.ss7.ITU14.pc << "           |\n";;
  cout << "|----------------------------------------|\n";

  /*display the adjacend remote pointcodes and IP addresses */
  cout << "|------+--------+--------------------+--------+--------|\n";
  cout << "|Remote| status |   Remote IP addr   |   DPC  |Assoc-ID|\n";
  cout << "|------+--------+--------------------+--------+--------|\n";
  i = 1;
  while ( SUA_GETPC_NO_ERROR ==  sua_get_destpc_addr(i, dest_pc, assoc_id, stat) )
    {
      cout << "| " ;
      cout.width(3);
      cout.fill(' ');
      cout << i ;
      cout <<"  | ";
      cout.width(6);
      cout.fill(' ');
      cout << disp_PC_stat[stat];
      cout.width(0);
      cout.fill(' ');
      cout << " | ";
      if (dest_pc.ipvx.sa.sa_family == AF_INET)
	{ /* ipv4 */
	  ptr = inet_ntop( dest_pc.ipvx.sa.sa_family,
			   &dest_pc.ipvx.sin.sin_addr,
			   output_str,
			   sizeof(output_str)
			   );
	  cout.width(18);
	  cout.fill(' ');
	  cout << output_str;
	  
	  cout << " |";   						
	}
      else
	{	/* ipv6 */
	  ptr = inet_ntop( dest_pc.ipvx.sa.sa_family,
			   &dest_pc.ipvx.sin6.sin6_addr,
			   output_str,
			   sizeof(output_str)
			   );
	  cout.width(18);
	  cout.fill(' ');		
	  cout << output_str;
	  cout << " |";	
	}		
      
      cout.width(7);
      cout.fill(' ');
      cout << dest_pc.ss7.ITU14.pc ;
      cout.width(0);
      cout.fill(' ');
      cout << " | ";
      cout.width(3);
      cout.fill(' ');
      cout << assoc_id ;
      cout <<"    |\n";   

      i++;
    }
  cout << "|------+--------+--------------------+--------+--------|\n";
  
} 



/***********************************************************************/
/* Test User Part : testip_admin_lock_asp                            */
/***********************************************************************/
void testip_admin_lock_asp(unsigned int sua_assoc_id)
{
  unsigned int res;

  res = sua_DeActivate_assoc( sua_assoc_id );
  cout << "Lock " << sua_assoc_id << " Associations\n";
}

/***********************************************************************/
/* Test User Part : testip_admin_unlock_asp                            */
/***********************************************************************/
void testip_admin_unlock_asp(unsigned int sua_assoc_id)
{
  unsigned int res;

  res = sua_Activate_assoc( sua_assoc_id );
  cout << "Unlock " << sua_assoc_id << "  ASP\n";
}
/***********************************************************************/
/* Test User Part : testip_admin_snm_daud                              */
/***********************************************************************/
void testip_snm_daud()
{
  int res;

  cout << "Send a Destination AUDIT to all know remote destinations\n";
  
  res = sua_send_daud();
}

/***********************************************************************/
/* Test User Part : testip_stdin_cb                                    */
/***********************************************************************/
void testip_stdin_cb( int        fd, 
		      short int  revents,
		      short int  *gotevents,
		      void       *dummy
                    )
{
  int length;
  char readBuffer[256];
  unsigned int xsxx;
  sccp_QOS_str udt_QOS, co_QOS;
  unsigned int Sua_ConnId=0, sua_assoc_id;

  cin.getline(readBuffer,256,'\n');
  string cmdline(readBuffer,strlen(readBuffer));

  if (cmdline.length() == 0) 
    {
      cout << ">>";
      cout << flush;

      return;
    }
  else if (cmdline.find("help") != cmdline.npos)
    {
      cout << "Available commands are:\n";
      cout << " help - display this page\n";
      cout << " quit/exit - exit the program\n";
      cout << " destaddr6: - set destination ipv6 address \n";
      cout << " destaddr4: - set destination ipv4 address \n";
      cout << " destname: - set destination hostname\n";
      cout << " destpc: - set destination SS7 pointcode(decimal)\n";
      cout << " destGT: - set destination Global Title\n";
      cout << " some-other-string - send this Connectionless to destaddrx\n";
      cout << " co:some-other-string - send this Connection-oriented\n";
      cout << " disp assoc mngt - display status of all associations\n";
      cout << " disp dest mngt - display status of all destinations\n";
      cout << " lock:x - put association x in inactive/down state\n";
      cout << " enable:x - put association x in activequit state\n";
      cout << " mirror:act - activate testup mirror function\n";
      cout << " mirror:dact - deactivate testup mirror function\\n";
      cout << " return-option: - change return-option(toggle) on/off\n";
      //cout << " rep:x - repeat sending x CLDT/CODT msg to remote\n";
      cout << " term - terminate all the present associations(SCTP & SUA)\n";
      //cout << " restart - restart all the present associations(SCTP & SUA)\n";
    } 
  else if ((cmdline.find("quit") != cmdline.npos) ||
	   (cmdline.find("exit") != cmdline.npos))
    {
      /* add some cleanups */
      sua_terminate();
      cout << "exiting the Test User program\n";
      /* exit program */
      exit(0);
    } 
  else if (cmdline.find("term") != cmdline.npos)
    {
      /* add some cleanups */
      sua_shutdown();
      cout << "Closing down the SCTP & SUA associations\n";
    } 
  else if ((cmdline.find("rep:") != cmdline.npos))
    {
      cout << "Repeat sending CL/CO data to remote end every " << rep_timer << " sec\n";
    } 
  else if ((cmdline.find("mirror:act") != cmdline.npos))
    {
      mirror = TRUE;
      cout << "Mirror activated!\n";
    } 
  else if ((cmdline.find("mirror:dact") != cmdline.npos))
    {
      mirror = FALSE;
      cout << "Mirror dactivated!\n";
    } 
  else if ((cmdline.find("return-option:") != cmdline.npos))
    {
      return_option = !return_option;
      cout << "Return option = " << return_option << " (0= FALSE, 1= TRUE)\n";
    } 
  else if (cmdline.find("lock:") != cmdline.npos)
    {
      /* lock up all SUA instances for ASP management */
      /* get association id  */
      string associd_str( cmdline, (cmdline.find("lock:")+5),cmdline.length());
      char *associd_char = new char [associd_str.length()+1];
      associd_str.copy(associd_char, associd_str.length());
      associd_char[associd_str.length()] = '\0';
      sua_assoc_id = atoi(associd_char);
      testip_admin_lock_asp(sua_assoc_id);
    } 
  else if (cmdline.find("enable:") != cmdline.npos)
    {
      /* unlock all SUA instances for ASP management */
      string associd_str( cmdline, (cmdline.find("enable:")+7),cmdline.length());
      char *associd_char = new char [associd_str.length()+1];
      associd_str.copy(associd_char, associd_str.length());
      associd_char[associd_str.length()] = '\0';
      sua_assoc_id = atoi(associd_char);
      testip_admin_unlock_asp(sua_assoc_id);
    } 
  else if (cmdline.find("snm audit") != cmdline.npos)
    {
      /* do a DAUD for all destinations */
      testip_snm_daud();
    } 
  else if (cmdline.find("disp assoc mngt") != cmdline.npos)
    {
      /* Display the management status of all  SUA instances */
      testip_disp_assoc_mngt();
    } 
  else if (cmdline.find("disp dest mngt") != cmdline.npos)
    {
      /* Display the management status of all  SUA instances */
      testip_disp_dest_mngt();
    } 
  else if (cmdline.find("term") != cmdline.npos)
    {
      /* Terminate the following associations */
      cout << "| Association | SUA management status |\n";
      
    } 
  else if (cmdline.find("destaddr6:") != cmdline.npos)
    {
      /* get destination address, convert it and store in cld */
      string addr_str(cmdline, (cmdline.find("destaddr6:")+10),cmdline.length());
      cout << "dest address = " << addr_str << "\n";
      char *addr_char = new char [addr_str.length()+1];
      addr_str.copy(addr_char, addr_str.length());
      addr_char[addr_str.length()] = '\0';
      called_pty_addr.address_fields_present.pc = ipvx_pc_present;
      called_pty_addr.address_fields_present.name_gt = no_name_present;
      called_pty_addr.address_fields_present.ssn_port = ssn_present;
      called_pty_addr.address_fields_present.field_in_header = include_nothing;
      called_pty_addr.pc.ipvx.sa.sa_family = AF_INET6;
      called_pty_addr.pc.ipvx.sin6.sin6_port = SUA_PORT;
      inet_pton( AF_INET6,
		 addr_char,
		 &called_pty_addr.pc.ipvx.sin6.sin6_addr
	       );
      called_pty_addr.ssn = 255;
      called_pty_addr.routing_ind = route_on_ssn;
      
      calling_pty_addr.ssn = 255;
      calling_pty_addr.routing_ind = route_on_ssn;

      cout << "Address of remote end stored. Msg will be sent to this one\n";
    } 
  else if (cmdline.find("destaddr4:") != cmdline.npos)
    {
      /* get destination address, convert it and store in cld */
      string addr_str( cmdline, (cmdline.find("destaddr4:")+10),cmdline.length());
      cout << "dest address " << addr_str << "\n";
      char *addr_char = new char [addr_str.length()+1];
      addr_str.copy(addr_char, addr_str.length());
      addr_char[addr_str.length()] = '\0';
      called_pty_addr.address_fields_present.pc = ipvx_pc_present;
      called_pty_addr.address_fields_present.name_gt = no_name_present;
      called_pty_addr.address_fields_present.ssn_port = ssn_present;
      called_pty_addr.address_fields_present.field_in_header = include_nothing;
      called_pty_addr.pc.ipvx.sa.sa_family = AF_INET;
      called_pty_addr.pc.ipvx.sin.sin_port = SUA_PORT;
      inet_pton( AF_INET,
		 addr_char,
		 &called_pty_addr.pc.ipvx.sin.sin_addr
	       );
      called_pty_addr.ssn = 255;
      called_pty_addr.routing_ind = route_on_ssn;

      calling_pty_addr.ssn = 255;
      calling_pty_addr.routing_ind = route_on_ssn;
     
      cout << "Address of remote end stored. Msg will be sent to this one\n";
    } 
  else if (cmdline.find("destname:") != cmdline.npos)
    {
      /* get destination hostname, convert it and store in cld */
      string addr_str(cmdline, (cmdline.find("destname:")+9),cmdline.length());

      called_pty_addr.address_fields_present.pc = no_pc_present;
      called_pty_addr.address_fields_present.name_gt = hostname_present;
      called_pty_addr.address_fields_present.ssn_port = ssn_present;
      called_pty_addr.address_fields_present.field_in_header = include_nothing;
      strcpy(called_pty_addr.name.HostName,addr_str.c_str());
      called_pty_addr.ssn =  calling_pty_addr.ssn;
      called_pty_addr.routing_ind = route_on_name_gt;
     
      cout << "DNS/SUA dest hostname = " << addr_str << "\n";
      cout << "Address of remote end stored. Msg will be sent to this one\n";

      string addr2_str = own_hostname;
     
      calling_pty_addr.address_fields_present.name_gt = hostname_present;
      strcpy(calling_pty_addr.name.HostName, own_hostname);
      calling_pty_addr.ssn = 5;
      calling_pty_addr.routing_ind = route_on_name_gt;
      cout << "DNS/SUA source hostname = " << addr2_str << "\n";
     
    } 
  else if (cmdline.find("destGT:") != cmdline.npos)
    {
      /* get destination Global Title, convert it and store in cld */
      string addr_str(cmdline, (cmdline.find("destGT:")+7),cmdline.length());

      called_pty_addr.address_fields_present.pc = no_pc_present;
      called_pty_addr.address_fields_present.name_gt = GT_present;
      called_pty_addr.address_fields_present.ssn_port = ssn_present;
      called_pty_addr.address_fields_present.field_in_header = include_nothing;
      called_pty_addr.name.GT.Translation_Type = 1;
      called_pty_addr.name.GT.Numbering_Plan = 1;
      called_pty_addr.name.GT.Nature_of_Address = 3;
      strcpy(called_pty_addr.name.GT.digits,addr_str.c_str());
      called_pty_addr.name.GT.nr_of_digits= addr_str.length();
      called_pty_addr.ssn =  calling_pty_addr.ssn;
      called_pty_addr.routing_ind = route_on_name_gt;
     
      cout << "DNS/SUA dest Global Title = " << addr_str << "\n";
      cout << "Address of remote end stored. Msg will be sent to this one\n";

      calling_pty_addr.address_fields_present.name_gt = GT_present;

      calling_pty_addr.ssn = 5;
      calling_pty_addr.routing_ind = route_on_name_gt;
      cout << "DNS/SUA source Global Title = " <<  calling_pty_addr.name.GT.digits << "\n";
     
    } 
 else if (cmdline.find("destpc:") != cmdline.npos)
    {
      /* get destination address, convert it and store in cld */
      string addr_str(cmdline, (cmdline.find("destpc:")+7),cmdline.length());
      cout << "dest address = " << addr_str << "\n";
      char *addr_char = new char [addr_str.length()+1];
      addr_str.copy(addr_char, addr_str.length());
      addr_char[addr_str.length()] = '\0';
      called_pty_addr.address_fields_present.pc = ss7_pc_present;
      called_pty_addr.address_fields_present.name_gt = no_name_present;
      called_pty_addr.address_fields_present.ssn_port = ssn_present;
      called_pty_addr.address_fields_present.field_in_header = include_nothing;
      called_pty_addr.pc.ss7.ITU24.family = ITU24bit;
      called_pty_addr.pc.ss7.ITU24.pc = atoi(addr_char);
      called_pty_addr.ssn = calling_pty_addr.ssn;     
      called_pty_addr.routing_ind = route_on_ssn;

      calling_pty_addr.ssn = 5;
      calling_pty_addr.routing_ind = route_on_ssn;
      
      cout << "Address of remote end stored. Msg will be sent to this one\n";
    } 
  else if (cmdline.find("co:") != cmdline.npos)
    {
     
      co_QOS.prot_class = class2;
      co_QOS.in_sequence = false;
      co_QOS.sequence_number = 0;
      co_QOS.return_msg_on_error = return_option;
      co_QOS.importance = 0;

      xsxx=  sua_getPath( 1,
			  pathinfo
			);

      //cout << "result getpath = " << xsxx << "\n";

      calling_pty_addr.address_fields_present.pc =  called_pty_addr.address_fields_present.pc;
      calling_pty_addr.address_fields_present.name_gt =  called_pty_addr.address_fields_present.name_gt;
      calling_pty_addr.routing_ind = called_pty_addr.routing_ind;
      calling_pty_addr.address_fields_present.ssn_port =  called_pty_addr.address_fields_present.ssn_port;
      calling_pty_addr.address_fields_present.field_in_header = called_pty_addr.address_fields_present.field_in_header ;

      if (calling_pty_addr.address_fields_present.name_gt == hostname_present)
	{
	  string addr4_str = own_hostname;
	  strcpy(calling_pty_addr.name.HostName, own_hostname);
	}
      else  if (calling_pty_addr.address_fields_present.name_gt == GT_present)
	{
	  calling_pty_addr.name.GT = pathinfo.local_addr.name.GT;
	}


      calling_pty_addr.pc.ipvx = pathinfo.local_addr.pc.ipvx;
      calling_pty_addr.ssn = pathinfo.local_addr.ssn;
      calling_pty_addr.pc.ss7 = pathinfo.local_addr.pc.ss7 ;

      char *databuffer =  new char[cmdline.length()];
      cmdline.copy(databuffer, cmdline.length());
      length = cmdline.length();

      xsxx = Send_sua_primitive(N_CONNECT_REQ,
				Sua_ConnId,
				co_QOS,
				called_pty_addr,
				calling_pty_addr,
				databuffer,
				length 
				);

      refID = Sua_ConnId;

#ifdef DEBUG
      cout << "testuser.c++: Returned "<< xsxx <<" from the send (1==association error, 0==success, -1==could not send)\n";
#endif
	
      delete databuffer; 
    } 
  else 
    {

      udt_QOS.prot_class = class0;
      udt_QOS.in_sequence = false;
      udt_QOS.sequence_number = seq_control_num++;
      udt_QOS.return_msg_on_error = return_option;
      udt_QOS.importance = 0;

      xsxx=  sua_getPath( 1,
			  pathinfo
			);

#ifdef DEBUG
      cout << "Sequence control parameter = " << udt_QOS.sequence_number << "\n";
#endif

      calling_pty_addr.address_fields_present.pc =  called_pty_addr.address_fields_present.pc;
      calling_pty_addr.address_fields_present.name_gt =  called_pty_addr.address_fields_present.name_gt;
      calling_pty_addr.routing_ind = called_pty_addr.routing_ind;
      calling_pty_addr.address_fields_present.ssn_port =  called_pty_addr.address_fields_present.ssn_port;
      calling_pty_addr.address_fields_present.field_in_header =  called_pty_addr.address_fields_present.field_in_header;
      if (calling_pty_addr.address_fields_present.name_gt == hostname_present)
	{
	  string addr4_str = own_hostname;
	  strcpy(calling_pty_addr.name.HostName, own_hostname);
	}
      else  if (calling_pty_addr.address_fields_present.name_gt == GT_present)
	{
	  calling_pty_addr.name.GT = pathinfo.local_addr.name.GT;
	}

      calling_pty_addr.pc.ipvx = pathinfo.local_addr.pc.ipvx;
      calling_pty_addr.ssn = pathinfo.local_addr.ssn;    
      calling_pty_addr.pc.ss7 = pathinfo.local_addr.pc.ss7 ;
     

      char *databuffer =  new char[cmdline.length()];
      cmdline.copy(databuffer, cmdline.length());
      length = cmdline.length();

      xsxx = Send_sua_primitive(N_UNITDATA,
				Sua_ConnId,
				udt_QOS,
				called_pty_addr,
				calling_pty_addr,
				databuffer,
				length 
				);

#ifdef DEBUG
      cout << "testuser.c++: Returned "<< xsxx <<" from the send (1==association error, 0==success, -1==could not send)\n";
#endif	

      delete databuffer;
    }
  cout << ">";
  cout << flush;

}


#define SEND_EVENTS_WHEN_TIMER_EXPIRES  1
/***********************************************************************/
/* Test user part : timer_expired                                      */
/***********************************************************************/
void timer_expired(unsigned int tID,
		   void         *associationIDvoid, 
		   void         *unused
		  )
{
    int length;
    sccp_addr_str co_clg, co_cld;
    sccp_QOS_str co_QOS;
    string cmdline= "ping";

    unsigned int Sua_ConnId=refID;
    
    cout << "************************************************************\n";
    cout << "**                    Timer expired                       **\n";
    if (sendcnt < sendNo) {
      sendcnt++;
      cout << "Send a PING\n";
      doPingPong(Sua_ConnId);
      timerID = sctp_startTimer( 10,      /* seconds */
				 0,       /* microseconds */
				 &timer_expired,
				 NULL,
				 NULL
			       );
    }
    else
      {
	cout << "Terminate the SUA connection\n";
	unsigned int xsxx;
      
	co_QOS.prot_class = class2;
	co_QOS.in_sequence = false;
	co_QOS.sequence_number = 0;
	co_QOS.return_msg_on_error = 0;
	co_QOS.importance = 0;
       

	char *databuffer =  new char[cmdline.length()];
	cmdline.copy(databuffer, cmdline.length());
	length = cmdline.length();
       
	xsxx = Send_sua_primitive( N_RELEASE_REQ,
				   Sua_ConnId,
				   co_QOS,
				   co_cld,
				   co_clg,
				   databuffer,
				   length 
				 );

#ifdef DEBUG
	cout << "testuser.c++: Returned "<< xsxx <<" from the send (1==association error, 0==success, -1==could not send)\n";
#endif
	
	delete databuffer;
      }
}

/***********************************************************************/
/* Test user part: ulp_ClDataIndNotif                                  */
/***********************************************************************/
void ulp_ClDataIndNotif( unsigned int   local_sua_Id,
			 unsigned int   primitive,
			 unsigned int   datalen
			)
{
  int length=datalen;
  int index;
  unsigned int Sua_ConnId;
  unsigned int xsxx;
  char databuffer[MAX_DATA_LENGTH];
  sccp_QOS_str QOS;
  sccp_addr_str clg,cld;
  
  Receive_sua_primitive ( primitive,
			  Sua_ConnId,
			  QOS,
			  cld,
			  clg,  
			  databuffer,
			  length 
			);
  	
  index = 0;

  cout << "testuser.c++: Hey, we received ClDataIndNotif \n";

  //check for primitive, receive only unitdata's up till know
  switch (primitive)
    {
    case (N_UNITDATA):
      called_pty_addr = clg;
      cout << "************************************************************\n";
      cout << "**              UNITDATA message received                 **\n";
      break;
    default:
      break;
    }

 
  string disp_output(databuffer,index, (length - index));
  cout << "************************************************************\n";
  cout << disp_output << "\n";
  cout << "************************************************************\n";
  cout << ">";
  cout << flush;
  
  /* check if mirror is active */
  if (mirror)
    {
      cout << "UDT msg mirrored\n";
      /* char *databuffer =  new char[cmdline.length()];
      cmdline.copy(databuffer, cmdline.length());
      length = cmdline.length();*/

      xsxx = Send_sua_primitive(N_UNITDATA,
				Sua_ConnId,
				QOS,
				called_pty_addr,
				calling_pty_addr,
				databuffer,
				length 
				); 
    }
}


/***********************************************************************/
/* Test user part: ulp_ConnIndNotif                                  */
/***********************************************************************/
void ulp_ConnIndNotif( unsigned int   local_sua_Id,
		       unsigned int   ConnId,
		       unsigned int   datalen
		     )
{
  int length=datalen;
  int index;
  unsigned int Sua_ConnId, primitive;
  char databuffer[MAX_DATA_LENGTH];
  sccp_QOS_str QOS;
  sccp_addr_str clg,cld;
  
  Receive_sua_primitive ( primitive,
			  Sua_ConnId,
			  QOS,
			  cld,
			  clg,  
			  databuffer,
			  length 
			);
  	
  index = 0;

  string cmdline(databuffer,index, (length - index));

  cout << "testuser.c++: Hey, we received ConnIndNotif \n";

  //check for primitive
  switch (primitive)
    {
    case (N_CONNECT_IND):
      called_pty_addr = clg;
      cld = called_pty_addr;
      clg = calling_pty_addr;
      cout << "************************************************************\n";
      cout << "**           CONNECT REQ IND message received             **\n";
      // send Connect confirm back 
      // this is the responding side of the connection

      sccp_QOS_str co_QOS;
      unsigned int xsxx;
     
      co_QOS.prot_class = class2;
      co_QOS.in_sequence = false;
      co_QOS.sequence_number = 0;
      co_QOS.return_msg_on_error = 0;
      co_QOS.importance = 0;

      if (cmdline.find("refused") != cmdline.npos)
	{	  
	  // mirror received data back to initiator
	  cout << "length = " << length << "\n";
	  
	  xsxx = Send_sua_primitive(N_CONNECT_REFUSED,
				    Sua_ConnId,
				    QOS,
				    cld,
				    clg,
				    databuffer,
				    length 
				  );
	}
      else 
	{
	  xsxx = Send_sua_primitive(N_CONNECT_RESP,
				    Sua_ConnId,
				    QOS,
				    cld,
				    clg,
				    databuffer,
				    length 
				  );  
	}
      
      cout << "testuser.c++: Returned "<< xsxx <<" from the send (1==association error, 0==success, -1==could not send)\n";
    
     
      break;
    default:
      break;
    }

 
  string disp_output(databuffer,index, (length - index));
  cout << "************************************************************\n";
  cout << disp_output << "\n";
  cout << "************************************************************\n";
  cout << ">";
  cout << flush;

}



/***********************************************************************/
/* Test user part: ulp_ConnConfIndNotif                                */
/***********************************************************************/
void ulp_ConnConfIndNotif( unsigned int   local_sua_Id,
			   unsigned int   ConnId,
			   unsigned int   datalen
			 )
{
  int length=datalen;
  int index;
  unsigned int Sua_ConnId, primitive;
  char databuffer[MAX_DATA_LENGTH];
  sccp_QOS_str QOS;
  sccp_addr_str clg,cld;
  
  Receive_sua_primitive ( primitive,
			  Sua_ConnId,
			  QOS,
			  cld,
			  clg,  
			  databuffer,
			  length 
			);
  	
  index = 0;

  cout << "testuser.c++: Hey, we received ConnConfIndNotif\n";

  //check for primitive, receive only unitdata's up till know
  switch (primitive)
    {
    case (N_CONNECT_CONF):
      
      cout << "************************************************************\n";
      cout << "**         CONNECT CONFIRM IND message received           **\n";
      // received the connection confirmed msg
      // this is the initiator of the connection
      // start a timer for sending the data to the responder
      timerID = sctp_startTimer( 15,            /* seconds */
				 0,             /* microseconds */
				 &timer_expired,
				 NULL,
				 NULL
			       );
      
      break;
    default:
      break;
    }

 
  string disp_output(databuffer,index, (length - index));
  cout << "************************************************************\n";
  cout << disp_output << "\n";
  cout << "************************************************************\n";
  cout << ">";
  cout << flush;
   
}


/***********************************************************************/
/* Test user part: ulp_ConnDataIndNotif                                */
/***********************************************************************/
void ulp_ConnDataIndNotif( unsigned int   local_sua_Id,
			   unsigned int   ConnId,
			   unsigned int   datalen
			 )
{
  int length=datalen;
  int index;
  unsigned int Sua_ConnId, primitive;
  char databuffer[MAX_DATA_LENGTH];
  sccp_QOS_str QOS;
  sccp_addr_str clg,cld;
  
  Receive_sua_primitive ( primitive,
			  Sua_ConnId,
			  QOS,
			  cld,
			  clg,  
			  databuffer,
			  length 
			);
  	
  index = 0;
  string cmdline(databuffer,length);

  cout << "testuser.c++: Hey, we received ConnDataIndNotif \n";

  //check for primitive, receive only unitdata's up till know
  switch (primitive)
    {
    case (N_DATA_IND):
      
      cout << "************************************************************\n";
      cout << "**              CO DATA  message received                 **\n";
      // received a connection oriented data
      // if it is a ping send back a pong
      // if it is a pong, start timer for the next data or disconnect
      
      if (cmdline.find("ping") != cmdline.npos)
	{}
      else if (cmdline.find("pong") != cmdline.npos)
	{
	  cout << "End: pong received\n";
	}
      else
	cout << "Not in ping-pong mode\n";

      break;
    default:
      break;
    }

 
  string disp_output(databuffer,index, (length - index));
  cout << "************************************************************\n";
  cout << disp_output << "\n";
  cout << "************************************************************\n";
  cout << ">";
  cout << flush;
   
}


/***********************************************************************/
/* Test user part: ulp_DisconnIndNotif                                  */
/***********************************************************************/
void ulp_DisConnIndNotif( unsigned int   local_sua_Id,
			  unsigned int   ConnId,
			  unsigned int   cause,
			  unsigned int   datalen
			)
{
  int length=datalen;
  int index;
  unsigned int Sua_ConnId, primitive;
  char databuffer[MAX_DATA_LENGTH];
  sccp_QOS_str QOS;
  sccp_addr_str clg,cld;

  Receive_sua_primitive ( primitive,
			  Sua_ConnId,
			  QOS,
			  cld,
			  clg,  
			  databuffer,
			  length 
			);
  	
  index = 0;

  cout << "testuser.c++: Hey, we received DisConnIndNotif\n";

  //check for primitive, receive only unitdata's up till know
  switch (primitive)
    {
    case (N_RELEASE_REQ):
      
      cout << "************************************************************\n";
      cout << "**    RELEASE REQUEST:DISCONNECT IND message received     **\n";
      break;
    case (N_RELEASE_CONF):
      
      cout << "************************************************************\n";
      cout << "**    RELEASE CONFIRM:DISCONNECT IND message received     **\n";
      break;
    default:
      break;
    }

 
  string disp_output(databuffer,index, (length - index));
  cout << "************************************************************\n";
  cout << disp_output << "\n";
  cout << "************************************************************\n";
  cout << ">";
  cout << flush;
   
}

// end of module testuser.c++













