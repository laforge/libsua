/***************************************************************************
                          testuser.h  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: testuser.h,v 1.1 2003/01/14 13:47:56 p82609 Exp $
 *
 * SUA Test user part implementation.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * Purpose: This header-file defines the SUA Test User Part application and  
 *          has definitions for:
 *          - initialise the Test User part application 
 *          - Test User Part STDIN function -> reading keyboard and 
 *            sending the info to SUA
 *          - TESTUP Connectionless Data Ind Notification(from SUA)
 *          - TESTUP CO Connection Ind Notification(from SUA)
 *          - TESTUP CO Connection Confirmation Ind Notification(from SUA)
 *          - TESTUP CO Data Ind Notification(from SUA)
 *          - TESTUP CO DisConnection Ind Notification(from SUA)
 */

#ifndef SUA_TSTUP_H
#define SUA_TSTUP_H

#include "sua.h"

#ifdef LINUX
    #include <unistd.h>
#endif

#include <iostream>
#include <string>




using namespace std;

#include <sys/time.h>

void init_testip_stdin(void );

void testip_stdin_cb( int        fd, 
		      short int  revents,
		      short int  *gotevents,
		      void       *dummy
		    );


void ulp_ClDataIndNotif( unsigned int   local_sua_Id,
			 unsigned int   primitive,
			 unsigned int   datalen
		       );    

void ulp_ConnIndNotif( unsigned int   local_sua_Id,
		       unsigned int   ConnId,
		       unsigned int   datalen
		     );

void ulp_ConnConfIndNotif( unsigned int   local_sua_Id,
			   unsigned int   ConnId,
			   unsigned int   datalen
			 );

void ulp_ConnDataIndNotif( unsigned int   local_sua_Id,
			   unsigned int   ConnId,
			   unsigned int   datalen
			 );

void ulp_DisConnIndNotif( unsigned int   local_sua_Id,
			  unsigned int   ConnId,
			  unsigned int   cause,
			  unsigned int   datalen
			);

#endif  // SUA_TSTUP_H	


// end of module sua_test_user.c













