/***************************************************************************
                          sua_distribution.h  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: sua_distribution.h,v 1.2 2003/08/21 10:33:13 p82609 Exp $
 *
 * SUA implementation according to SUA draft issue 6.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * The alternative comment
 * inspiration : Dorine
 * "When B.B. releases one of his pearls of wisedom and "
 * "starts one of his walks around the block, then this designer says:"
 * "I am getting tired of running in circles even if it is "
 * "a nice block in a nice neighbourhood(maybe his walks are recursive)"
 * 
 * Purpose: This header-file defines the SUA distributor functions for:
 *          - Data arrive notification(from SCTP) -> receive the msg data 
 *            from SCTP(use receive_sua_primitive function below), process it 
 *            and call the aproriate SUA msg handling function
 *          - Network status change Notification(from SCTP)
 *          - Sent Failure Notification(from SCTP)
 *          - Communication Lost Notification(from SCTP)
 *          - Communication Up Notification(from SCTP)
 *          - Communication Error Notification(from SCTP)
 *          - Restart Notification(from SCTP)
 *          - Shutdown Notification(from SCTP)
 *          - Send a SUA primitive -> generate and send the msg to SCTP
 *          - Receive the sua "primitive"/msg data from SCTP     
 */


#ifndef SUA_DISTRIBUTION_H
#define SUA_DISTRIBUTION_H


#include "sua.h"
#include <string>


using namespace std;

typedef struct {
  unsigned int   primitive;
  unsigned int   user_ref;
  sccp_QOS_str   QOS;
  sccp_addr_str  called_pty_address;
  sccp_addr_str  calling_pty_address;
  string  userdata;
} sua_save_str;

/***********************************************************************/
/* sctp_DataArriveNotif                                                */
/***********************************************************************/
void sctp_DataArriveNotif( unsigned int   assoc_id,
			   unsigned short stream_id,
			   unsigned int   len,
			   unsigned short streamSN,
			   unsigned int   tsn,
			   unsigned int   protocol_id,
			   unsigned int   unordered_flag,
			   void *         ulp_data_ptr
			 );


/***********************************************************************/
/* sctp_NetworkStatusChangeNotif                                       */
/***********************************************************************/
void sctp_NetworkStatusChangeNotif( unsigned int   assoc_id,
				    short          dest_addr_index,
				    unsigned short new_path_state,
				    void *         ulp_data_ptr
				  );


/***********************************************************************/
/* sctp_SentFailureNotif                                               */
/***********************************************************************/
void sctp_SentFailureNotif( unsigned int    assoc_id,
			    unsigned char * unsent_data_sent_ptr,
			    unsigned int    len,
			    unsigned int *  sctp_send_context_ptr,
			    void *          ulp_data_ptr
			  );


/***********************************************************************/
/* sctp_communicationLostNotif                                         */
/***********************************************************************/
void sctp_CommunicationLostNotif( unsigned int   assoc_id,
				  unsigned short status_event,
				  void *         ulp_data_ptr 
				);

/***********************************************************************/
/* sctp_CommunicationUpNotif                                           */
/***********************************************************************/
void* sctp_CommunicationUpNotif( unsigned int      assoc_id,
				 int               status_event,
				 unsigned int      nr_of_dest_addr,
				 unsigned short    nr_of_input_streams,
				 unsigned short    nr_of_output_streams,
				 int               support_PRsctp,
				 void *            ulp_data_ptr 
			       );


/***********************************************************************/
/* sctp_CommunicationErrorNotif                                        */
/***********************************************************************/
void sctp_CommunicationErrorNotif( unsigned int   assoc_id,
				   unsigned short error_status,
				   void *         ulp_data_ptr
				 );


/***********************************************************************/
/* sctp_RestartNotif                                                   */
/***********************************************************************/
void sctp_RestartNotif( unsigned int   assoc_id,
			void *         ulp_data_ptr
		      );


/***********************************************************************/
/* sctp_ShutDownCompleteNotif                                       */
/***********************************************************************/
void sctp_ShutDownCompleteNotif( unsigned int   assoc_id,
				 void *         ulp_data_ptr
			       );


/***********************************************************************/
/* sctp_queueStatusChangeNotif                                         */
/***********************************************************************/
void sctp_queueStatusChangeNotif (unsigned int   assoc_id, 
				  int            queue_type, 
				  int            queue_id, 
				  int            queue_length, 
				  void*          ulp_data_ptr
				  );

/***********************************************************************/
/* sctp_asconfStatusNotif                                              */
/***********************************************************************/
void sctp_asconfStatusNotif( unsigned int assoc_id,
			     unsigned int correlation_id,
			     int          result, 
			     void*        temp, 
			     void*        ulp_data_ptr
			     );


#endif  // SUA_DISTRIBUTION_H		  

// end of module sua_distribution.h


