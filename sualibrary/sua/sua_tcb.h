/***************************************************************************
                          sua_tcb.h  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: sua_tcb.h,v 1.1 2003/01/14 14:15:37 p82609 Exp $
 *
 * SUA implementation according to SUA draft issue 6.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * The alternative comment
 * inspiration : Isabelle
 * "
 * ""
 *
 * Purpose: This header-file defines the SUA Transaction Control Block(TCB) 
 *          pool functions for(needed for SUA connection oriented):
 *          - initialising a single TCB
 *          - initialising the TCB pool
 *          - allocate a TCB
 *          - get a TCB
 *          - release a TCB
 *          And for the Saved SUA msg queue pool
 *          - add SUA msg to queue
 *          - get sua msg from queue
 *          - release the SUA msg from the queue     
 */

#ifndef SUA_TCB_H
#define SUA_TCB_H

#include "sua_database.h"
#include "sua_syntax.h"
#include "sua.h"

#include <string>
#include <map>
#include <queue>
#include <arpa/inet.h>

enum tcb_scoc_state_set { 
  scoc_idle,        // idle state
  scoc_incoming, 
  scoc_wait_m_CC, 
  scoc_outgoing, 
  scoc_active, 
  scoc_disconnect
};

class  tcb_Sua_TCB_str 
{
 public:
  tcb_scoc_state_set  state;
  unsigned int        Dest_LR;
  unsigned int        Source_LR;
  unsigned int        User_ref_id;
  unsigned int        sctp_Association_id;
  unsigned int        scoc_tcb_id;
  protocol_class_set  prcl_class;
  unsigned int        seq_number;
  bool                return_option;
  sccp_addr_str       remote_address;
 public:
  void init_TCB_elem();
}; ///:~

typedef  map<int,tcb_Sua_TCB_str> tcb_sua_map_str;

class tcb_Sua_TCB_arr
{
 public:
   tcb_sua_map_str  tcb;
   /*private:*/
   unsigned int     last_allocated_tcb;
 public:
   void init_TCB_pool();
   tcb_Sua_TCB_str  *allocate_TCB(unsigned int &Sua_ConnId);
   tcb_Sua_TCB_str  *get_tcb(unsigned int Sua_ConnId);
   void              release_TCB(unsigned int Sua_ConnId);
}; ///:~ 

typedef pair<unsigned int, tcb_Sua_TCB_str>  tcb_sua_pair_str;

typedef pair<tcb_sua_map_str::iterator,bool> tcb_sua_map_iterator_str;

struct tcb_Sua_msg_elem {
  string     byte;
  int        delivery_type;
  int        stream_id;
  bool       valid;
};
 
typedef queue<tcb_Sua_msg_elem> tcb_Sua_msgqueue;

class  tcb_Sua_msgqueue_pool 
{
 public:
   tcb_Sua_msgqueue instance[db_MAX_REMOTE_SUA];
 public:
  void add_msg( unsigned int       sua_assoc_idx,
	        tcb_Sua_msg_elem   sua_msg
	      );
  tcb_Sua_msg_elem get_msg( unsigned int sua_assoc_idx );
  void delete_msg( unsigned int sua_assoc_idx );
}; ///:~


#endif  // SUA_TCB_H

//end of module sua_tcb.h








