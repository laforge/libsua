/***************************************************************************
                          sua_distribution.cpp  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: sua_distribution.cpp,v 1.6 2003/09/09 08:43:25 p82609 Exp $
 *
 * SUA implementation according to SUA draft issue 6.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * The alternative comment
 * inspiration : Pascale
 * "We are continously looking for Ghostwriters."
 * "When we find one, he will be terminated on the spot. Nobody writes (into)
 * "my lifes variables without my permission."
 *
 * Purpose: This code-file defines the SUA distributor functions for:
 *          - Data arrive notification(from SCTP) -> receive the msg data 
 *            from SCTP(use receive_sua_primitive function below), process it 
 *            and call the aproriate SUA msg handling function
 *          - Network status change Notification(from SCTP)
 *          - Sent Failure Notification(from SCTP)
 *          - Communication Lost Notification(from SCTP)
 *          - Communication Up Notification(from SCTP)
 *          - Communication Error Notification(from SCTP)
 *          - Restart Notification(from SCTP)
 *          - Shutdown Notification(from SCTP)
 *          - Send a SUA primitive -> generate and send the msg to SCTP
 *          - Receive the sua "primitive"/msg data from SCTP     
 */

#include "sua_debug.h"
#include "sua_database.h"
#include "sua_tcb.h"
#include "sua_logging.h"
#include "sua_syntax.h"
#include "sua_cl.h"
#include "sua_co.h"
#include "sua_distribution.h"
#include "sua_asp_mgnt.h"
#include "sua_snm_mgnt.h"


#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <string>
#include <map>
#include <vector>

using namespace std;

#define BUFSIZE 2000

// syntax definitions of the sua message
Sua_container msg;

// storage for received SUA messages
vector<sua_save_str> rec_msg_pool;

extern db_Sua_DatabaseList sua;

extern tcb_Sua_TCB_arr tcb_pool;
extern tcb_Sua_msgqueue_pool  msg_store;

/* cvs shit */
/***********************************************************************/
/* sctp_DataArriveNotif                                                */
/***********************************************************************/
void sctp_DataArriveNotif( unsigned int   sctp_assoc_id,
			   unsigned short stream_id,
			   unsigned int   len,
			   unsigned short data_streamSN,
			   unsigned int   data_tsn,
			   unsigned int   protocol_id,
			   unsigned int   unordered_flag,
			   void *         ulp_data_ptr
			 )
{

  unsigned short result;
  unsigned int Sua_ConnId;
  unsigned int sua_assoc_id;
  unsigned int local_sua_id,remote_sua_id;
  unsigned short streamSN;
  unsigned int tsn;
  tcb_Sua_TCB_str  *tcb_ptr;
  Sua_syntax_error_struct error;
  char databuf[BUFSIZE];
  unsigned int flags = SCTP_MSG_DEFAULT;

#ifdef DEBUG
  cout << "Received a Data notification from SCTP for association " << sctp_assoc_id <<  " : handle and distribute\n";
#endif
  
  

  // receive data from SCTP
  result = sctp_receive( sctp_assoc_id,
			 stream_id,
			 (unsigned char *) databuf,
			 &len,
			 &streamSN,
			 &tsn,
			 flags
			 ); 

  // display byte array
#ifdef DEBUG
  display_byte_array(databuf ,len);
#endif
  char logstring[100];
  sprintf(logstring, "SUA message, just received from SCTP");
  event_log("sua_distribution.c",logstring);
  log_byte_array("sua_distribution.c", databuf,len);


  // copy received data from databuf to msg object -> byte array
  msg.sua_msg.insert(0, databuf, len); 
  // decode data to figure which SUA message we received
  error = msg.sua_decode();

  if (!error.no_error)
    {
      char logstring[100];
      sprintf(logstring, "Decoding error in received SUA message : index = %d,", error.msg_index);
      event_log("sua_distribution.c",logstring);
      /*char *spare = new char[error.error_text.length()];*/
      /*error.error_text.copy(spare, error.error_text.length());*/
      /*sprintf(logstring, "Error string  = %s,", spare);*/
      /*event_log("sua_distribution.c",logstring);*/
      cout << "Decoding error in received SUA message : "<< error.error_text << ", index = " << error.msg_index << "\n";
      msg.sua_prim.hdr_msg_class =   sua_max;
      return;
    }

  // distribute the message based on some parameters
  // Connectionless or connection-oriented
  switch (msg.sua_prim.hdr_msg_class)
    {
    case (sua_cl):

      // connectionless : strip of header and send to user
      switch (msg.sua_prim.hdr_msg_type.cl)
	{
	case cl_data_transfer /*N_UNITDATA*/:
	  {
#ifdef DEBUG
	    cout << "CL: Unitdata message handling\n";
#endif
	    
	    sua_assoc_id = sua.AssocDB.Find_association( sctp_assoc_id,
							 local_sua_id,
							 remote_sua_id
						       );
	    result = process_unitdata_msg ( local_sua_id,
					    sua_assoc_id,
					    msg
					   );
   				 
	    char logstring[100];
	    sprintf(logstring, "Received Unitdata msg");
	    event_log("sua_distribution.c",logstring);

	    break;
	  }
	case cl_data_response /*N_UNITDATA_SERVICE*/:
	  {
#ifdef DEBUG
	    cout << "CL: Unitdata Service  message handling\n";
#endif
	    
	    sua_assoc_id = sua.AssocDB.Find_association( sctp_assoc_id,
							 local_sua_id,
							 remote_sua_id
						       );
	    result = process_UDTService_msg ( local_sua_id,
					    sua_assoc_id,
					    msg
					   );
   				
	    char logstring[100];
	    sprintf(logstring, "Received Unitdata Service msg");
	    event_log("sua_distribution.c",logstring);

	    break;
	  }
	default:
	  {
	    cout << "Unknown connectionless message type !!\n";
	    char logstring[100];
	    sprintf(logstring, "Unknown connectionless message type");
	    event_log("sua_distribution.c",logstring);
	    break;
	  }
	}

      break;

    case (sua_co):
    
      // connectionoriented: get SCOC TCB, do state-event handling and send to use 
      switch (msg.sua_prim.hdr_msg_type.co)
	{
	case co_core /*N_CONNECT_IND */:
	  {
#ifdef DEBUG
	    cout << "CO: Connect indication message handling\n";
#endif
	    char logstring[100];
	    sprintf(logstring, "Received Connect Indication msg");
	    event_log("sua_distribution.c",logstring);

     
	    unsigned int local_sua_id,remote_sua_id;
	    
	    sua_assoc_id = sua.AssocDB.Find_association( sctp_assoc_id,
						       local_sua_id,
						       remote_sua_id
						     );

	    tcb_ptr = tcb_pool.allocate_TCB(Sua_ConnId);

	    result = process_CORE_msg ( sua_assoc_id,
					tcb_ptr,
					Sua_ConnId,
					msg
				      );
	    break;
	  }
	case co_coak /*connect confirm */:
	  {
#ifdef DEBUG
	    cout << "CO: Connection request acknowledge message handling\n";
#endif
	    Sua_ConnId = msg.sua_prim.dest_ref;
	    tcb_ptr = tcb_pool.get_tcb(Sua_ConnId);
	    if (( msg.sua_prim.dest_ref_pres))        
	      {
		char logstring[100];
		sprintf(logstring, "Received Connect Confirm Indication msg");
		event_log("sua_distribution.c",logstring);

	        // call connection request acknowledge message handling 
		result = process_COAK_msg ( sctp_assoc_id,
					    tcb_ptr,
					    Sua_ConnId,
					    msg
					  );
	      }
	    else
	      cout << "ERROR: TCB " << Sua_ConnId << " was not retrieved?!!\n";
	    
	    break;
	  }
	case co_data /*data*/:
	  {
#ifdef DEBUG
	    cout << "CO: Data message handling\n";
#endif
	    Sua_ConnId = msg.sua_prim.dest_ref;
	    tcb_ptr = tcb_pool.get_tcb(Sua_ConnId);
	    if (( msg.sua_prim.dest_ref_pres)/* &&
						( tcb_ptr /= NULL)*/)        
	      {
		char logstring[100];
		sprintf(logstring, "Received Data Indication msg");
		event_log("sua_distribution.c",logstring);

	        // call data message handling function
		result = process_CODATA_msg ( sctp_assoc_id,
					      tcb_ptr,
					      Sua_ConnId,
					      msg
					    );
	      }
	    else
	      cout << "ERROR: TCB " << Sua_ConnId << " was not retrieved?!!\n";
	    
	    break;
	  }
	case co_relre /*release request*/:
	  {
#ifdef DEBUG
	    cout << "CO: Release Request message handling\n";
#endif
	    Sua_ConnId = msg.sua_prim.dest_ref;
	    tcb_ptr = tcb_pool.get_tcb(Sua_ConnId);
	    if (( msg.sua_prim.dest_ref_pres))        
	      {
		char logstring[100];
		sprintf(logstring, "Received Release Request Indication msg");
		event_log("sua_distribution.c",logstring);

	        // call release request message handling function
		result = process_CORELRQ_msg ( sctp_assoc_id,
					       tcb_ptr,
					       Sua_ConnId,
					       msg
					     );
	      }
	    else
	      cout << "ERROR: TCB " << Sua_ConnId << " was not retrieved?!!\n";
	     
	   
	    break;
	  }
	case co_relco /*release completed */:
	  {
#ifdef DEBUG
	    cout << "CO: Release Complete message handling\n";
#endif
	    Sua_ConnId = msg.sua_prim.dest_ref;
	    tcb_ptr = tcb_pool.get_tcb(Sua_ConnId);
	    if (( msg.sua_prim.dest_ref_pres))        
	      {
		char logstring[100];
		sprintf(logstring, "Received Release Confirm Indication msg");
		event_log("sua_distribution.c",logstring);

	        // call release complete message handling function
		result = process_CORELCO_msg ( sctp_assoc_id,
					       tcb_ptr,
					       Sua_ConnId,
					       msg
					     );
	      }
	    else
	      cout << "ERROR: TCB " << Sua_ConnId << " was not retrieved?!!\n";
	     
	   
	    break;
	  }
	case co_coref /*connection refused */:
	  {
#ifdef DEBUG
	    cout << "CO: Connection refused message handling\n";
#endif
	    Sua_ConnId = msg.sua_prim.dest_ref;
	    tcb_ptr = tcb_pool.get_tcb(Sua_ConnId);
	    if (( msg.sua_prim.dest_ref_pres))        
	      {
		char logstring[100];
		sprintf(logstring, "Received Connect Confirm Indication msg");
		event_log("sua_distribution.c",logstring);

	        // call connection request acknowledge message handling 
		result = process_COREF_msg ( sctp_assoc_id,
					     tcb_ptr,
					     Sua_ConnId,
					     msg
					   );
	      }
	    else
	      cout << "ERROR: TCB " << Sua_ConnId << " was not retrieved?!!\n";
	    
	    break;
	  }
	default:
	  {
	    char logstring[100];
	    sprintf(logstring, "Unknown connectionoriented message type");
	    event_log("sua_distribution.c",logstring);
	    cout << "Unknown connectionoriented message type !!\n";
	    break;
	  }
	}

      break;

    case (sua_mngt):
      // SUA management msg
      sua_assoc_id = sua.AssocDB.Find_association( sctp_assoc_id,
						 local_sua_id,
						 remote_sua_id
					       );

      switch (msg.sua_prim.hdr_msg_type.mngt)
	{
	case mngt_error :
	  {
#ifdef DEBUG
	    cout << "SUA Management: ERROR message handling\n";
#endif
	   
   				 
	    char logstring[100];
	    sprintf(logstring, "Received ERROR msg");
	    event_log("sua_distribution.c",logstring);

	    break;
	  }
	case mngt_notify :
	  {
#ifdef DEBUG
	    cout << "SUA Management: NOTIFY message handling\n";
#endif
	   
	    char logstring[100];
	    sprintf(logstring, "Received NOTIFY msg");
	    event_log("sua_distribution.c",logstring);
 
	    break;
	  }
	default:
	  {
	    cout << "Unknown SUA managment message type !!\n";
	    char logstring[100];
	    sprintf(logstring, "Unknown SUA Management message type");
	    event_log("sua_distribution.c",logstring);
	    break;
	  }
	}

      break;



    case (sua_ssnm):
      // Signalling Network Management(SNM) msg
      sua_assoc_id = sua.AssocDB.Find_association( sctp_assoc_id,
						 local_sua_id,
						 remote_sua_id
					       );

      switch (msg.sua_prim.hdr_msg_type.ssnm)
	{
	case ssnm_duna :
	  {
#ifdef DEBUG
	    cout << "SNM: Destination unavialable\n";
#endif
	    
	    result = process_DUNA_msg ( sua_assoc_id,
					local_sua_id,
					remote_sua_id,
					msg
				      );	    				 
	    char logstring[100];
	    sprintf(logstring, "Received DUNA msg");
	    event_log("sua_distribution.c",logstring);

	    break;
	  }
	case ssnm_dava :
	  {
#ifdef DEBUG
	    cout << "SNM: Destination avialable message handling\n";
#endif
	   
	    result = process_DAVA_msg ( sua_assoc_id,
					local_sua_id,
					remote_sua_id,
					msg
				      );	
   
	    char logstring[100];
	    sprintf(logstring, "Received DAVA msg");
	    event_log("sua_distribution.c",logstring);
 
	    break;
	  }
	case ssnm_daud :
	  {
#ifdef DEBUG
	    cout << "SNM: Destination state audit message handling\n";
#endif
	    result = process_DAUD_msg ( sua_assoc_id,
					local_sua_id,
					remote_sua_id,
					msg
				      );	
   
	    char logstring[100];
	    sprintf(logstring, "Received DAUD msg");
	    event_log("sua_distribution.c",logstring);
 
	    break;
	  }
        case ssnm_scon :
	  {
#ifdef DEBUG
	    cout << "SNM: Network congestion message handling\n";
#endif
	   
	    result = process_SCON_msg ( sua_assoc_id,
					local_sua_id,
					remote_sua_id,
					msg
					);	
   
	    char logstring[100];
	    sprintf(logstring, "Received SCON msg");
	    event_log("sua_distribution.c",logstring);
 
	    break;
	  }
	case ssnm_dupu :
	  {
#ifdef DEBUG
	    cout << "SNM: Destination User Part Unavialable message handling\n";
#endif
	   
	    result = process_DUPU_msg ( sua_assoc_id,
					local_sua_id,
					remote_sua_id,
					msg
				      );	
   
	    char logstring[100];
	    sprintf(logstring, "Received DUPU msg");
	    event_log("sua_distribution.c",logstring);
 
	    break;
	  }
	case ssnm_drst :
	  {
#ifdef DEBUG
	    cout << "SNM: Destination restricted message handling\n";
#endif

	    result = process_DRST_msg ( sua_assoc_id,
					local_sua_id,
					remote_sua_id,
					msg
				      );	   	   

	    char logstring[100];
	    sprintf(logstring, "Received DRST msg");
	    event_log("sua_distribution.c",logstring);
 
	    break;
	  }
	default:
	  {
	    cout << "Unknown SUA managment message type !!\n";
	    char logstring[100];
	    sprintf(logstring, "Unknown SUA Management message type");
	    event_log("sua_distribution.c",logstring);
	    break;
	  }
	}

      break;

    case (sua_aspsm):
      // ASP State maintenance msg

      sua_assoc_id = sua.AssocDB.Find_association( sctp_assoc_id,
						 local_sua_id,
						 remote_sua_id
					       );

      switch (msg.sua_prim.hdr_msg_type.aspsm)
	{
	case aspsm_up :
	  {
#ifdef DEBUG
	    cout << "ASPSM: ASP_UP message handling\n";
#endif
	    result = process_ASPUP_msg ( sua_assoc_id,
					 local_sua_id,
					 remote_sua_id,
					 msg
				       );
   				 
	    char logstring[100];
	    sprintf(logstring, "Received ASPUP msg");
	    event_log("sua_distribution.c",logstring);

	    break;
	  }
	case aspsm_up_ack :
	  {
#ifdef DEBUG
	    cout << "ASPSM: ASP_UP_ACK message handling\n";
#endif
	    result = process_ASPUP_ACK_msg ( sua_assoc_id,
					     local_sua_id,
					     remote_sua_id,
					     msg
				           );
   				 
	    char logstring[100];
	    sprintf(logstring, "Received ASPUP ACK msg");
	    event_log("sua_distribution.c",logstring);
 
	    break;
	  }
	case aspsm_down :
	  {
#ifdef DEBUG
	    cout << "ASPSM: ASP_DOWN message handling\n";
#endif
	    result = process_ASPDOWN_msg ( sua_assoc_id,
					   local_sua_id,
					   remote_sua_id,
					   msg
				         );
   				 
	    char logstring[100];
	    sprintf(logstring, "Received ASPDO msg");
	    event_log("sua_distribution.c",logstring);

	    break;
	  }
	case aspsm_down_ack :
	  {
#ifdef DEBUG
	    cout << "ASPSM: ASP_DOWN_ACK message handling\n";
#endif
	    result = process_ASPDOWN_ACK_msg ( sua_assoc_id,
					       local_sua_id,
					       remote_sua_id,
					       msg
				             );
   				 
	    char logstring[100];
	    sprintf(logstring, "Received ASPDOWN ACK msg");
	    event_log("sua_distribution.c",logstring);
 
	    break;
	  }
	case aspsm_beat :
	  {
#ifdef DEBUG
	    cout << "ASPSM: HEARTBEAT message handling\n";
#endif
	    result = process_BEAT_msg ( sua_assoc_id,
					local_sua_id,
					remote_sua_id,
					msg
				      );
   				 
	    char logstring[100];
	    sprintf(logstring, "Received HEARTBEAT msg");
	    event_log("sua_distribution.c",logstring);

	    break;
	  }
	case aspsm_beat_ack :
	  {
#ifdef DEBUG
	    cout << "ASPSM: HEARTBEAT_ACK message handling\n";
#endif
	    result = process_BEAT_ACK_msg ( sua_assoc_id,
					    local_sua_id,
					    remote_sua_id,
					    msg
				           );
   				 
	    char logstring[100];
	    sprintf(logstring, "Received HEARTBEAT ACK msg");
	    event_log("sua_distribution.c",logstring);
 
	    break;
	  }
	default:
	  {
	    cout << "Unknown ASP State Maintenance message type !!\n";
	    char logstring[100];
	    sprintf(logstring, "Unknown ASP State Maintenance message type");
	    event_log("sua_distribution.c",logstring);
	    break;
	  }
	}

      break;

    case (sua_asptm):
       // ASP Traffic maintenance msg

      sua_assoc_id = sua.AssocDB.Find_association( sctp_assoc_id,
						 local_sua_id,
						 remote_sua_id
					       );

      switch (msg.sua_prim.hdr_msg_type.asptm)
	{
	case asptm_act :
	  {
#ifdef DEBUG
	    cout << "ASPTM: ASPAC message handling\n";
#endif
	    result = process_ASPAC_msg ( sua_assoc_id,
					 local_sua_id,
					 remote_sua_id,
					 msg
				       );
   				 
	    char logstring[100];
	    sprintf(logstring, "Received ASPAC msg");
	    event_log("sua_distribution.c",logstring);

	    break;
	  }
	case asptm_act_ack :
	  {
#ifdef DEBUG
	    cout << "ASPSM: ASPAC_ACK message handling\n";
#endif
	    result = process_ASPAC_ACK_msg ( sua_assoc_id,
					     local_sua_id,
					     remote_sua_id,
					     msg
				           );
   				 
	    char logstring[100];
	    sprintf(logstring, "Received ASPAC ACK msg");
	    event_log("sua_distribution.c",logstring);
 
	    break;
	  }
	case asptm_inact :
	  {
#ifdef DEBUG
	    cout << "ASPTM: ASPINAC message handling\n";
#endif
	    result = process_ASPINAC_msg ( sua_assoc_id,
					   local_sua_id,
					   remote_sua_id,
					   msg
				         );
   				 
	    char logstring[100];
	    sprintf(logstring, "Received ASPINAC msg");
	    event_log("sua_distribution.c",logstring);

	    break;
	  }
	case asptm_inact_ack :
	  {
#ifdef DEBUG
	    cout << "ASPSM: ASPINAC_ACK message handling\n";
#endif
	    result = process_ASPINAC_ACK_msg ( sua_assoc_id,
					       local_sua_id,
					       remote_sua_id,
					       msg
				             );
   				 
	    char logstring[100];
	    sprintf(logstring, "Received ASPINAC ACK msg");
	    event_log("sua_distribution.c",logstring);
 
	    break;
	  }
	default:
	  {
	    cout << "Unknown ASP Traffic Maintenance message type !!\n";
	    char logstring[100];
	    sprintf(logstring, "Unknown ASP Traffic Maintenance message type");
	    event_log("sua_distribution.c",logstring);
	    break;
	  }
	}

      break;

    default:
      {
	char logstring[100];
	sprintf(logstring, "Unknown message type");
	event_log("sua_distribution.c",logstring);
	break;
      }
    }
  // return
#ifdef DEBUG
  cout << "Received message processed\n";
#endif
}

/***********************************************************************/
/* sctp_NetworkStatusChangeNotif                                       */
/***********************************************************************/
void sctp_NetworkStatusChangeNotif( unsigned int   assoc_id,
				    short          dest_addr_index,
				    unsigned short new_path_state,
				    void *         ulp_data_ptr
				  )
{
#ifdef DEBUG
  cout << "Received a Network status change notification from SCTP for association " << assoc_id << "\n";
#endif
}

/***********************************************************************/
/* sctp_SentFailureNotif                                               */
/***********************************************************************/
void sctp_SentFailureNotif( unsigned int    assoc_id,
			    unsigned char * unsent_data_sent_ptr,
			    unsigned int    len,
			    unsigned int *  sctp_send_context_ptr,
			    void *          ulp_data_ptr
			  )
{
#ifdef DEBUG
  cout << "Received a send failure notification from SCTP for association " << assoc_id << " Ignore it, not supported.\n";
#endif
}

/***********************************************************************/
/* sctp_communicationLostNotif                                         */
/***********************************************************************/
void sctp_CommunicationLostNotif( unsigned int   sctp_assoc_id,
				  unsigned short status_event,
				  void *         ulp_data_ptr 
				)
{
  unsigned int sua_assoc_id,local_sua_id, remote_sua_id;

#ifdef DEBUG
  cout << "Received indication that communication with remote peer was lost for association " << sctp_assoc_id <<  "\n";
  cout << "Status CommunicationLostNotif = " << status_event << "\n";
#endif
  /* Put in debugging file */
  char logstring[100];
  sprintf(logstring, "CommunicationLostNotif received from SCTP");
  event_log("sua_distribution.c",logstring);
  sprintf(logstring,"assoc_id = %d, status = %d ",sctp_assoc_id,status_event);
  event_log("sua_distribution.c",logstring);

#ifdef SUA_MANAGEMENT
 if ((sua_assoc_id = sua.AssocDB.Find_association(sctp_assoc_id, local_sua_id,remote_sua_id)) != 0)
   {
     /* if we found the sua assoc then the SUA ASP is marked down, */
     /* as the sctp association has failed */ 
     sua.AssocDB.down(sua_assoc_id, 0);
#ifdef DEBUG
     cout << "ASP down for SUA association " << sua_assoc_id <<  "\n";
#endif

   }
#endif
 
}

/***********************************************************************/
/* sctp_CommunicationUpNotif                                           */
/***********************************************************************/
void *sctp_CommunicationUpNotif( unsigned int      sctp_assoc_id,
				 int               status_event,
				 unsigned int      nr_of_dest_addr,
				 unsigned short    nr_of_input_streams,
				 unsigned short    nr_of_output_streams,
				 int               support_PRsctp,
				 void *            ulp_data_ptr 
			       )
{

  unsigned int sua_assoc_id,local_sua_id, remote_sua_id;
  char logstring[100];
  int result;
  tcb_Sua_msg_elem   sua_msg;

#ifdef DEBUG
  cout << "Received Communication Up indication for association " << sctp_assoc_id <<"\n";
  cout << "Status CommunicationUpNotif = " << status_event << ", number of addresses used = " <<  nr_of_dest_addr << "\n";
  cout << "Number of input streams = " <<  nr_of_input_streams << ", number of output streams = " << nr_of_output_streams << "\n";
#endif 

  /* Put in debugging file */
  sprintf(logstring, "CommunicationUpNotif received from SCTP");
  event_log("sua_distribution.c",logstring);
  sprintf(logstring,"assoc_id = %d, status = %d , number of dest addr = %d",sctp_assoc_id,status_event, nr_of_dest_addr);
  event_log("sua_distribution.c",logstring);
  sprintf(logstring,"Number of input streams = %d, number of output streams = %d", nr_of_input_streams,nr_of_output_streams);
  event_log("sua_distribution.c",logstring);  


  
  if ((sua_assoc_id = sua.AssocDB.Find_association(sctp_assoc_id, local_sua_id,remote_sua_id)) == 0) {
#ifdef DEBUG
    cout << "association " << sctp_assoc_id << " is not present\n";
#endif
    sua_assoc_id = sua.AssocDB.passive_associate( sctp_assoc_id,
						sua.local_sua,
						sua.remote_sua,
						nr_of_dest_addr,
						nr_of_input_streams,
						nr_of_output_streams
					      );
  }

  /* code include if SUA management is NOT used */
#ifndef SUA_MANAGEMENT
  if ( sua.AssocDB.instance[sua_assoc_id].status == asp_down_traf_hold)
    {
      /* - get the saved msg ansd send it on the association that is setup */
      sua_msg = msg_store.get_msg ( sua_assoc_id );

      while ( sua_msg.valid)
	{
	  char* databuf = new char[sua_msg.byte.length()];
	  sua_msg.byte.copy(databuf, sua_msg.byte.length());
	  datalen = sua_msg.byte.length(); 
	  
	  /* msg retrieved and copied, may now remove it from queue */
	  msg_store.delete_msg ( sua_assoc_id );
	  
#ifdef DEBUG
	  display_byte_array(databuf , sua_msg.byte.length());
#endif
	  char logstring[100];
	  sprintf(logstring, "SUA encoded message, ready for being send to SCTP assoc %d", sctp_assoc_id);
	  event_log("sua_distribution.c",logstring);
	  log_byte_array("sua_distribution.c", databuf,sua_msg.byte.length());
	  
	  result = sctp_send ( sctp_assoc_id,
			       sua_msg.stream_id,
			       (unsigned char *) databuf,
			       datalen,
			       SUA_PPI,
			       SCTP_USE_PRIMARY,
			       SCTP_NO_CONTEXT,
			       SCTP_INFINITE_LIFETIME,
			       sua_msg.delivery_type,
			       SCTP_BUNDLING_DISABLED
			     );

	  delete databuf;
#ifdef DEBUG
	  cout << "sua_distribution.c:result sctp send = "<< result << "\n";
#endif
	  /* get the next saved msg ansd send if any ?  */
	  sua_msg = msg_store.get_msg ( sua_assoc_id );

	}
      /* SUA ASP is marked down */ 
      sua.AssocDB.down(sua_assoc_id,0);
    }
    
#endif

  /* code include if SUA management IS used */
#ifdef SUA_MANAGEMENT
#ifdef DEBUG
  cout << "sua_distribution.c:should send ASPUP\n";
#endif
#ifndef SG_MODE
  if (sua_assoc_id != 0)
    {
      /* send ASP-UP to remote to start activating local & remote ASP */
      result =  sua_send_ASPUP( sua_assoc_id,
				false,
				0
			      );
    }
#endif
#endif

  return NULL;
}

/***********************************************************************/
/* sctp_CommunicationErrorNotif                                        */
/***********************************************************************/
void sctp_CommunicationErrorNotif( unsigned int   assoc_id,
				   unsigned short error_status,
				   void *         ulp_data_ptr
				 )
{
#ifdef DEBUG
  cout << "Received Error indication for association " << assoc_id << " with error status " << error_status << "\n";
  cout << "Aborting association \n";
#endif
  sctp_deleteAssociation(assoc_id);
}


/***********************************************************************/
/* sctp_RestartNotif                                                   */
/***********************************************************************/
void sctp_RestartNotif( unsigned int   sctp_assoc_id,
			void *         ulp_data_ptr
		      )
{
  unsigned int sua_assoc_id,local_sua_id, remote_sua_id;
  int result;

#ifdef DEBUG
  cout << "Received Restart indication for association " << sctp_assoc_id << "\n";
#endif

#ifdef SUA_MANAGEMENT
 if ((sua_assoc_id = sua.AssocDB.Find_association(sctp_assoc_id, local_sua_id,remote_sua_id)) == 0)
   {
#ifdef DEBUG
     cout << "ASP status = " << sua.AssocDB.instance[sua_assoc_id].asp.status <<" for SUA association " << sua_assoc_id <<  "\n";
#endif

     /* restart association: ASP state should be set to down */
     sua.AssocDB.down(sua_assoc_id,0);

     sua.remote_sua.Activate_dests(sua_assoc_id);
#ifdef DEBUG
     cout << "All destination(s) activated for SUA association " << sua_assoc_id <<  "\n";
#endif

     /* if we found the sua assoc then we restart the ASP bringup sequence */
     /* as the sctp association has come back online */ 
     /* send ASP-UP to remote to start activating local & remote ASP */
     result =  sua_send_ASPUP( sua_assoc_id,
			       false,
			       0
			     );     
   }
#endif

}

/***********************************************************************/
/* sctp_ShutDownCompleteNotif                                          */
/***********************************************************************/
void sctp_ShutDownCompleteNotif( unsigned int   sctp_assoc_id,
				 void *         ulp_data_ptr
			       )
{
  unsigned int sua_assoc_id,local_sua_id, remote_sua_id;


#ifdef DEBUG
  cout << "Received shutdown indication for association " << sctp_assoc_id << "\n";
#endif

#ifdef SUA_MANAGEMENT
 if ((sua_assoc_id = sua.AssocDB.Find_association(sctp_assoc_id, local_sua_id,remote_sua_id)) != 0)
   {
     /* if we found the sua assoc then the SUA ASP is marked down, */
     /* as the sctp association has failed */ 
     sua.AssocDB.down(sua_assoc_id, 0);
#ifdef DEBUG
     cout << "ASP down for SUA association " << sua_assoc_id <<  "\n";
#endif
     sua.remote_sua.Deactivate_dests(sua_assoc_id);
#ifdef DEBUG
     cout << "All destination(s) blocked for SUA association " << sua_assoc_id <<  "\n";
#endif

   }
#endif
 

}

/***********************************************************************/
/* sctp_queueStatusChangeNotif                                         */
/***********************************************************************/
void sctp_queueStatusChangeNotif (unsigned int   assoc_id, 
				  int            queue_type, 
				  int            queue_id, 
				  int            queue_length, 
				  void*          ulp_data_ptr
				)
{
#ifdef DEBUG
  cout << "Received a queuestatus change indication for association " << assoc_id << "\n";
#endif
}

/***********************************************************************/
/* sctp_asconfStatusNotif                                              */
/***********************************************************************/
void sctp_asconfStatusNotif( unsigned int assoc_id,
			     unsigned int correlation_id,
			     int          result, 
			     void*        temp, 
			     void*        ulp_data_ptr
			   )
{
#ifdef DEBUG
  cout << "Received SCTP configuration indication for association " << assoc_id << "\n";
#endif
}

/***********************************************************************/
/* Send_sua_primitive                                                  */
/***********************************************************************/
unsigned int Send_sua_primitive( unsigned int   primitive,
				 unsigned int   &sua_ConnId,
				 sccp_QOS_str   &QOS,
				 sccp_addr_str  &called_pty_address,
				 sccp_addr_str  &calling_pty_address,
				 char           *buffer,
				 unsigned int   len
			       )
{
  int error;
  char logstring[100];
  tcb_Sua_TCB_str  *tcb_ptr;
#ifdef DEBUG
  cout << "Send SUA primitive to local SUA instance\n";
  cout << "ConnId = " << sua_ConnId << "\n";
#endif

  // search for the required local_ref
  if (sua_ConnId != 0)
    {
      //handle connection oriented primitves which have already a TCB
      tcb_ptr =  tcb_pool.get_tcb(sua_ConnId);
    
      switch (primitive) 
	{
	case N_CONNECT_RESP:
	  //send back response on received CORE of connection
	  sprintf(logstring, "CO N-CONNECT RESPONSE primitive req");
          event_log("sua_distribution.c",logstring);

	  error = sua_send_CORESP( QOS,
				   buffer,
				   len,
				   sua_ConnId,
				   tcb_ptr
				 );
	  
	  break;
	case N_DATA_REQ:
	  //send data on existing connection
	  sprintf(logstring, "CO N-DATA REQ primitive req");
	  event_log("sua_distribution.c",logstring);
	  error = sua_send_CODATA( QOS,
				   buffer,
				   len,
				   sua_ConnId,
				   tcb_ptr
				 );
	  
	  break;
	case N_RELEASE_REQ:
	  //Disconnect request on existing connection
	  sprintf(logstring, "CO N-RELEASE REQ primitive req");
          event_log("sua_distribution.c",logstring);
	  error = sua_send_CORELRQ( QOS,
				    buffer,
				    len,
				    sua_ConnId,
				    tcb_ptr
				 );
	  
	  break;
	case N_RELEASE_CONF:
	  //Disconnect confirm request on existing connection
	  sprintf(logstring, "CO N-RELEASE CONFIRM primitive req");
          event_log("sua_distribution.c",logstring);
	  error = sua_send_CORELCO( QOS,
				    buffer,
				    len,
				    sua_ConnId,
				    tcb_ptr
				 );
	  
	  break;
	case N_CONNECT_REFUSED:
	  //send a connect refused on connection
	  sprintf(logstring, "CO N-CONNECT REFUSED primitive req");
          event_log("sua_distribution.c",logstring);

	  error = sua_send_COREF( QOS,
				  buffer,
				  len,
				  sua_ConnId,
				  tcb_ptr
				);
	  
	  break;
	default:
	  sua_ConnId = 0;
	  error = 1;
	  break;
	}
    }
  else
    {
      //handle connectionless primitives
      //handle connection oriented primitives for which no TCB was yet alllocated
      // figured out connectionless/connection-oriented primitived
      switch (primitive) 
	{
	case N_UNITDATA:
          sprintf(logstring, "Unitdata primitive req");
          event_log("sua_distribution.c",logstring);
	  sua_ConnId = 0;
	  error = sua_send_Unitdata(QOS,
				    called_pty_address,
				    calling_pty_address,
				    buffer,
				    len
				   );
	  break;
	case N_CONNECT_REQ:
          sprintf(logstring, "CO N-CONNECT REQ primitive req");
          event_log("sua_distribution.c",logstring);

	  tcb_ptr =  tcb_pool.allocate_TCB(sua_ConnId);

	  error = sua_send_CORE( QOS,
				 called_pty_address,
				 calling_pty_address,
				 buffer,
				 len,
				 sua_ConnId,
				 tcb_ptr
				);
	  break;
        default:
	  sua_ConnId = 0;
	  error = 1;
	  break;
	}
    }

  return(error);

}

/***********************************************************************/
/* receive_sua_primitive                                               */
/***********************************************************************/
unsigned int Receive_sua_primitive( unsigned int   &primitive,
				    unsigned int   &Sua_ConnId,
				    sccp_QOS_str   &QOS,
				    sccp_addr_str  &called_pty_address,
				    sccp_addr_str  &calling_pty_address, 
				    char*          buffer,
				    unsigned int   len
				  )
{
  
  
  // retrieve the stored user data from the received msg list
  sua_save_str temp=rec_msg_pool.front();
  // delete the user data in the received msg list
  rec_msg_pool.erase(rec_msg_pool.begin());

  // primitive and address parameters
  primitive = temp.primitive;
  Sua_ConnId = temp.user_ref;
  QOS = temp.QOS;
  called_pty_address = temp.called_pty_address;
  calling_pty_address = temp.calling_pty_address;
  // put in supplied byte array(space is allocated beforehand by application)
  temp.userdata.copy(buffer, temp.userdata.length());
  len = temp.userdata.length();

  return(len);
}			  

// end of module sua_distribution.c
