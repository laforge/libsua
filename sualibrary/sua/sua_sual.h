/***************************************************************************
                          sua_sual.h  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: sua_sual.h,v 1.1 2003/01/14 14:15:37 p82609 Exp $
 *
 * SUA implementation according to SUA draft issue 6.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * The alternative comment
 * inspiration : Marie-Ange
 * "A ligth weigth version of SUA, just what the doctor ordered"
 * 
 * Purpose: This header-file defines the SUA light interface:
 *          - SUAL address structure
 *          - SUAL Callback function
 *          - SUAL function return values
 *          and SUA light functions for:
 *          - SUAL initialisation
 *          - SUAL Connectionless data Notification
 *          - SUAL CO Connect Notification
 *          - SUAL CO Connect Confirm Notification
 *          - SUAL CO Data Notification
 *          - SUAL CO DisConnect Notification
 *          - SUAL Register Instance
 *          - SUAL Connectionless Data Request to SUA
 *          - SUAL CO Connect Request to SUA
 *          - SUAL CO Connect Confirm Request to SUA
 *          - SUAL CO Data Request to SUA
 *          - SUAL CO DisConnect Request to SUA
 *          - SUAL CO Data read from SUA
 *          - SUAL Connectionless Data read from SUA
 */

#ifndef SUA_SUAL_H
#define SUA_SUAL_H

/*
 ***************************************************************************
 *
 * Module: sua_sual.h
 *
 * Purpose: definition of SUALight API
 *
 ***************************************************************************
 */

#ifdef LINUX
    #include <unistd.h>
#endif

//#include <cdefs.h>

#include <sys/types.h>
#include <netinet/in.h>

#define SUAL_SUALSERVICE_NAME "sual"
#define SUAL_SUALSERVICE_TRANSPORTPCOL "sctp"
#define SUAL_DEF_PORTNUMBER 14002

#define SUA_SCTP_PAYLOAD_PCOL_ID 0x04 
 

/* structures for API functions */
typedef int sual_id_t;
typedef unsigned short sual_port_t;
typedef sual_id_t sual_InstId_t;
typedef sual_id_t sual_ConnId_t;
typedef sual_id_t sual_DataId_t;
typedef int reason_t;

#define SUAL_DEF_NOSTREAMS 127


extern int sual_errno;


typedef struct sual_PeerAddr {
  int AddrType; /* predefined values for AddrType to indicate which
 		type is used in uPeerAddr: SUAL_ADDRTYPE_IPV4,
 		SUAL_ADDRTYPE_IPV6 */
  union uPeerAddr {
      struct in_addr ip4;	 	
      struct in6_addr ip6; 	       
    } uPeerAddr;
} sual_PeerAddr_t;

/* SUAL_ULP_CallBacks definitions */
typedef struct  sual_ULPcallbackFunctions {
		 
  void (*ConnIndNotif)( sual_id_t sual_ConnId, 
			sual_PeerAddr_t *pPeerAddr,
			uint UserDataLen
		      );
  void (*ConnConfNotif)( sual_id_t sual_ConnId, 
			 uint UserDataLen
		       );
  void (*CoDataIndNotif)( sual_id_t sual_ConnId,
			  uint UserDataLen
			);
  void (*DisconnIndNotif)( sual_id_t sual_ConnId, 
			   reason_t reason, 
			   uint UserDataLen
			 );  
  void (*ClDataIndNotif)( sual_id_t sual_DataId, 
			  uint ControlNumer, 
			  sual_PeerAddr_t *pPeerAddr, 
			  uint UserDataLen
			);
} sual_ULPcallbackFunctions_t;


typedef sual_ULPcallbackFunctions_t sualUif_Cbacks_t;

typedef sual_ULPcallbackFunctions_t SUAL_ULP_CallBacks;

typedef enum {
  SUAL_ADDRTYPE_IPV4,
  SUAL_ADDRTYPE_IPV6
} sual_IpAddrType_t;


/* 
 * API-functions  
 */
extern int sual_init(
  unsigned char *pConfFile
);

extern sual_InstId_t sual_RegisterSUAL_Instance( 
  sual_port_t LocalPort,
  sual_ULPcallbackFunctions_t *pULPcallbackFunctions
);


sual_id_t 
sual_ConnReq(		
  sual_id_t InstId,
  sual_PeerAddr_t *pPeerAddr, 
  unsigned char *pUserData,  
  uint UserDataLen
);

int 
sual_ConnResp(
  sual_id_t InstId,
  sual_id_t sual_ConnId,	
  unsigned char  *pUserData, 
  uint UserDataLen
);

int 
sual_DisconnReq(
  sual_id_t InstId,
  sual_id_t sual_ConnId,	
  unsigned char  *pUserData,  
  uint UserDataLen
);

int 
sual_CoDataRead(	
  sual_id_t InstId,
  sual_id_t sual_ConnId,
  unsigned char  *pBuff,
  uint BuffLen 
);

int 
sual_ClDataRead(	
  sual_id_t InstId,
  sual_DataId_t DataId,
  unsigned char  *pBuff,
  uint BuffLen 
);


int 
sual_CoDataReq(
  sual_id_t InstId,
  sual_id_t sual_ConnId,	
  unsigned char  *pUserData,
  uint UserDataLen 
);

int 
sual_ClDataReq(
  sual_id_t InstId,
  sual_PeerAddr_t *pPeerAddr,
  uint  ControlNumber,  
  unsigned char  *pUserData,
  uint UserDataLen 
);

extern int 
sual_snapshot(
  unsigned char *pSnapshotFile /* if NULL: stdout */
);

/*====================== reasons of ulp_DisconnIndNotif: ======================*/

typedef enum {
  SUAL_DISCONN_NOMORE_STREAMS=100,
  SUAL_DISCONN_SCTP_ASSOCIATION_LOST,
  SUAL_DISCONN_USERDATA_NOTREAD,
  SUAL_DISCONN_READUSERDATA_ILLPARAM,
  SUAL_DISCONN_CO_DATAREQ_WITHOUT_PAYLOAD,
  SUAL_DISCONN_WRONGEVENT_FROM_OTHER_PEER,
  SUAL_DISCONN_BY_COMMUNICATION_LOST,
  SUAL_DISCONN_BY_NETWORKSTATUSCHANGE,
  SUAL_DISCONN_COAC_CC_NOT_IN_CONNREQ_SENT, /* new */
  SUAL_DISCONN_BY_COAK_REF, /*new*/
  SUAL_DISCONN_NOMEM,
 
  SUAL_DISCONN_NOTABLE2SEND_SCTP_RELEASEREQUEST_MSG,
  
  SUAL_DISCONN_NOTABLE2SEND_SCTP_CONNECTREQUEST_MSG,
  SUAL_DISCONN_NOTABLE2SEND_SCTP_MSG,
  SUAL_DISCONN_NOTABLE2SEND_SCTP_COAK_REF_MSG,
  
  SUAL_DISCONN_SENDFAILURENOTIF, /* new */
  
  SUAL_DISCONN_LAST
} sual_disconnReason_t;

/*====================== returns of sual-API-functions: ======================*/

#define SUAL_OK 0


typedef enum {
  SUAL_API_RETURNS_FIRST=-1999,  /* is no return */ 	


/*
 * Returns of all function exept sual_init() 
 */
  SUAL_EERR_ENTITYSTATE,  /* entity error */
  SUAL_UERR_WRONG_INSTID, /* ULP error */
  SUAL_ERR_NOMEM, /* no more memory */

/*
 * Returns of all connection oriented api-functions 
 */  
  SUAL_UERR_NOSUCH_CONN, /* obsolete */
  SUAL_UERR_CONNSTATE,
  SUAL_UERR_ILLPARAM_USERDATA,/* concerning UserDataLen, pUserData */
  SUAL_UERR_USERDATA_NOTREAD, /* new */
  
/*
 * sual_init()
 */
  SUAL_EERR_ILLPARAM, /* e.g. Null-pointer */
  SUAL_CFG_ENOFILENAME, 
  SUAL_CFG_EOPENFILE, 
  SUAL_CFG_ENOPARAM, 
  SUAL_CFG_EPARAMVALUE, 
  SUAL_CFG_EPARAMFORMAT, 
  SUAL_CFG_EBUFFERLNG,	 
  SUAL_CERR_NOLOCAL_ADDRS_IN_CONFFILE,
  SUAL_SERR_SCTP_REGISTER, /* sctp error */
/* new */
  SUAL_WARN_LOCKED_REINIT,

/*
 * Returns of sual_RegisterSUAL_Instance() 
 */
  SUAL_UERR_NULLPOINTER_CALLBACK,
  SUAL_UERR_CBMISSING_CONCONFNOTIF,
  SUAL_UERR_CBMISSING_CONINDNOTIF,
  SUAL_UERR_CBMISSING_DISCONNINDNOTIF,
  SUAL_UERR_CBMISSING_CODATAINDNOTIF,
  SUAL_UERR_CBMISSING_CLDATAINDNOTIF,
  
  SUAL_UERR_LOCALPORT_NOTUNIQUE,

/*
 * Returns of sual_ConnReq() 
 */
  SUAL_UERR_NO_PEERADDR,
  SUAL_UERR_IPV4_NOTYET,
  SUAL_UERR_WRONG_ADDRTYPE,
  SUAL_WARN_SHORTAGE_OF_STREAMS,  

/*
 * Returns of sual_ConnResp()
 * see common returns: 
   - SUAL_UERR_ILLPARAM_USERDATA
   - SUAL_UERR_CONNSTATE
 */


/*
 * Returns of sual_DisconnReq()
 * see common returns: 
   - SUAL_UERR_ILLPARAM_USERDATA
   - SUAL_UERR_CONNSTATE
 */

  
/*
 * Returns of sual_CoDataRead() and sual_ClDataRead()
 */
  SUAL_UERR_MISSING_DATABUFFER,
  SUAL_UERR_MISSING_USERDATALEN,
  SUAL_UERR_BUFFLEN_TOOLESS,
 /*
  * Returns of sual_CoDataRead() 
  */
  SUAL_UERR_WRONG_CONNID,
  SUAL_UERR_CO_NODATA2READ,
 /*
  * Returns of sual_ClDataRead()
  */
  SUAL_UERR_WRONG_DATAID,
  SUAL_UERR_CL_NODATA2READ, /* e.g. already read */

/*
 * Returns of sual_CoDataReq() and sual_ClDataReq()
 */
  SUAL_UERR_DATAREQ_MISSING_DATABUFFER, /* obsolete */
  SUAL_UERR_DATAREQ_MISSING_USERDATALEN,/* obsolete */
	
/*
 * Returns of sual_ClDataReq()
   - SUAL_UERR_NO_PEERADDR
   - SUAL_UERR_IPV4_NOTYET
   - SUAL_UERR_WRONG_ADDRTYPE
 */
  SUAL_UERR_CONTROLNUMBER_OUT_OF_RANGE,
  
/*==============================================*/

  
/*
 * errors on LIF 
 */
 
  SUAL_RCVCLDATATRANSFER_USERDATA_NOTREAD, /* new */
 
  SUAL_ERR_NOSUCH_SCTP_ASSOCIATION, 
  SUAL_ERR_ULP_SSN_NOTREGISTERED, 
  SUAL_ERR_STREAMID_TOOBIG,
  SUAL_ERR_NOMEM_TO_RECEIVE_SCTPCHUNK,
  SUAL_ERR_FAILURE_SCTP_ASSOCIATE,
  SUAL_ERR_WRONG_AF_INET_TYPE, 
  SUAL_ERR_SUAL_RELRE_MSG_ON_FREE_STREAM,
  SUAL_ERR_SUAL_COAK_CC_MSG_ON_FREE_STREAM,
  SUAL_ERR_SUAL_RELCO_MSG_ON_FREE_STREAM,
  SUAL_ERR_SUAL_CODT_MSG_ON_FREE_STREAM, 
  SUAL_ERR_COAC_CC_NOT_IN_CONNREQ_SENT,
  SUAL_ERR_RCV_RELCO_BUTNOTWAITING_FOR,
  
  SUAL_SERR_NOTEXIST_DESTADDR,
  SUAL_SERR_NOTSUPPORTED_ADDRFORMAT,  
  SUAL_SERR_WRONG_MSGTYP_ONSTREAM,  
  SUAL_SERR_WRONG_MSGLEN,  
  SUAL_SERR_WRONGEVENT_FROM_OTHER_PEER,  
  SUAL_SERR_NOMORE_STREAMS,  
  SUAL_SERR_NOTABLE2SEND_SCTP_MSG,  
  
  

/*                                                
 * Errors concerning the SUAL-message      
 */	                                          
  SUAL_MSGERR_WRONG_PCOL_VERS=-600,
  SUAL_MSGERR_WRONG_MSGLEN, /* new */           
  SUAL_MSGERR_LEN_TOOLESS_MSGHDR,    
  SUAL_MSGERR_UNKNOWN_MSGTYPE, 
  SUAL_MSGERR_WRONG_VALUES4SPARE,  

/*
 * Errors in syslog
 */                                              
  SUAL_SSN_NOTREGISTERED,
	
/*
 * Program errors, which should newer occur
 */	
  SUAL_PERR_NO_SNAPSHOT_FD=-580,
  SUAL_PERR_ASS_NOTESTABL, 
  SUAL_PERR_ASS_STATE_UNKNOWN, 
  SUAL_PERR_WRONG_STATE, 
  SUAL_PERR_ALREADY_REGISTERED, 
 
  SUAL_PERR_NULLPOINTER, 
  SUAL_PERR_PARAM_NULLPOINTER, 
 
  SUAL_PERR_NULLPOINTER_PEERADDR, 
  SUAL_PERR_NULLPOINTER_SOCKUNION, 
 
  SUAL_PERR_NULLPOINTER_INSTEL, 
  SUAL_PERR_NULLPOINTER_ASSEL, 
  SUAL_PERR_NULLPOINTER_CONNEL, 
  SUAL_PERR_NULLPOINTER_STREAMEL, 
 
  SUAL_PERR_NULLPOINTER_RCVMSG, 
  SUAL_PERR_NULLPOINTER_POIPOIRCVMSG, 
  SUAL_PERR_NULLPOINTER_USERDATA, 

  SUAL_PERR_WRONG_CONNID_INREAD,
  SUAL_PERR_CONNSTATE, 
  SUAL_PERR_INCONSISTENT_LEN, 
  SUAL_PERR_NOASS_IN_CONNEL, 
  SUAL_PERR_ARG_IS_NULL_POINTER, 

  SUAL_PERR_WRONG_ASS_STATE,
  SUAL_PERR_WRONG_EVENT_ONFREESTREAM,
  SUAL_PERR_NOSUCH_SCTPASSOC,
  
  /* 19.06.2001: additional errors: */
  /* sual_init(): */
  SUAL_SERR_SYSTEMFUNCTION,
  SUAL_PERR_ERROR,
  SUAL_CFG_ERROR, 

  SUAL_LAST_RET_VALUE 
} sual_api_returns_t;

#endif  // SUA_SUAL_H	

// end of module sua_sual.h
