/***************************************************************************
                          sua_logging.cpp  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: sua_logging.cpp,v 1.1 2003/01/14 14:15:37 p82609 Exp $
 *
 * SUA implementation according to SUA draft issue 6.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * The alternative comment
 * inspiration : Cindy
 * "KISS (ME * ) methodology applies"
 * " * The extended KISS method only applies to beautifull girls."
 *
 * Purpose: This code-file defines the SUA logging(error or normal stuff)
 *          functions for:
 *          - initialise the logging file
 *          - log events to logging file
 *          - log byte arrya to logging file
 *          - Display byte array to screen
 *          - close logging file     
 */


#include "sua_debug.h"
#include "sua.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>

#include <sys/time.h>
#include <time.h>

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif


#define  MAX_BYTES_IN_LINE  20

using namespace std;

ofstream logfile;
char log_filename[100];

typedef enum { min = -127,
	       max = 128
} sua_byte_str;

/***********************************************************************/
/* init_logging_file                                                   */
/***********************************************************************/
void init_logging_file()
{
  struct timeval tv;
  struct timezone tz;
  struct tm *the_time;
  // random temporary logfile
  //sprintf(log_filename, "./tmp%d.log",(int)getpid());
  sprintf(log_filename, "./tmp2603.log");
  cout << "Logging all errors and events to file " << log_filename <<"\n";
  // creation of random logfile: overwrite file if already exists
  logfile.open(log_filename);
  if (!logfile)
    {
      cout << "Logging file " << log_filename << " could not be created\n";
      exit(0);
    }
  gettimeofday(&tv, &tz);
  the_time = localtime((time_t *) & (tv.tv_sec));
  logfile << the_time->tm_hour << ":" << the_time->tm_min << ":" << the_time->tm_sec << ":" << (int) (tv.tv_usec / 1000) <<  " >> Logfile " << log_filename << " created \n" << flush;

}



/* This function logs events.
   Parameters:
   @param event_log_level : INTERNAL_EVENT_0 INTERNAL_EVENT_1 EXTERNAL_EVENT_X EXTERNAL_EVENT
   @param module_name :     the name of the module that received the event.
   @param log_info :        the info that is printed with the modulename.
   @param anyno :           optional pointer to unsigned int, which is printed along with log_info.
                            The conversion specification must be contained in log_info.
   @author     Hölzlwimmer
*/

/***********************************************************************/
/* event_log                                                           */
/***********************************************************************/
void event_log( char *module_name, char *log_info, ...)
{

#ifdef FILE_DEBUG
  struct timeval tv;
  struct timezone tz;
  struct tm *the_time;

  gettimeofday(&tv, &tz);
  the_time = localtime((time_t *) & (tv.tv_sec));
  logfile << the_time->tm_hour << ":" << the_time->tm_min << ":" << the_time->tm_sec << ":" << (int) (tv.tv_usec / 1000) << " >> " << module_name << " >> " << log_info << " \n" << flush;        
#endif

  return;
}

/***********************************************************************/
/* event_log_byte_array                                                */
/***********************************************************************/
void log_byte_array( char *module_name, char *log_info, int len)
{
#ifdef FILE_DEBUG
  int i,space;
  int li;
  int lines;
  int index;
  struct timeval tv;
  struct timezone tz;
  struct tm *the_time;


  gettimeofday(&tv, &tz);
  the_time = localtime((time_t *) & (tv.tv_sec));
  logfile << the_time->tm_hour << ":" << the_time->tm_min << ":" << the_time->tm_sec << ":" << (int) (tv.tv_usec / 1000) << " >> " << module_name << " >> " ;
  logfile << "Display byte array(in Hex), length " << len << "(hex " << hex << len << ")\n";
 
  lines = (len / MAX_BYTES_IN_LINE) + 1;
  li = 0;
  index = 0;
  logfile.setf(ios::hex);
  while ( li < lines){
    i = 0;
    space = 0;
    while (( i < MAX_BYTES_IN_LINE) && (index < len)) {
      /* what is needed to display a single byte in hex */
      /* convert the char element to a unsigned character : weird??? */
      /* then convert the unsigned char to a unsigned short */
      /* do the display of the unsigned short */
      unsigned char usc = (log_info[index]);
      unsigned short c = usc;
      logfile << hex << setfill('0') << setw(2) << c;
      space++;
      if (space >= 4) {
	logfile << " ";
	space = 0;
      }
      index++;
      i++;
    }
    logfile << "\n";
    li++;
  }
  logfile.setf(ios::dec);
  logfile << flush;
#endif
      
  return;
}


/***********************************************************************/
/* display_byte_array                                                  */
/***********************************************************************/
void display_byte_array( char *log_info, int len)
{
  int i,li, lines,index, space;
    
  lines = (len / MAX_BYTES_IN_LINE) + 1;
  li = 0;
  index = 0;
 

  //cout << "lines = " << lines  << " , len = " << len << "\n";
  cout.setf(ios::hex);
  while ( li < lines){
    i = 0;
    space = 0;
    while (( i < MAX_BYTES_IN_LINE) && (index < len)) {
      /* what is needed to display a single byte in hex */
      /* convert the char element to a unsigned character : weird??? */
      /* then convert the unsigned char to a unsigned short */
      /* do the display of the unsigned short */
      unsigned char usc = (log_info[index]);
      unsigned short c = usc;
      cout << hex << setfill('0') << setw(2) << c;
      space++;
      if (space >= 4) {
	cout << " ";
	space = 0;
      }
      index++;
      i++;
    }
    cout << "\n";
    // cout << dec << "i = " << i << " , li = " << li << " , index = " << index << "\n";
    li++;
  }
  cout.setf(ios::dec);
  cout << "SUA message size = " << len << "\n";

}


/***********************************************************************/
/* close_logging_file                                                  */
/***********************************************************************/
void close_logging_file()
{
  struct timeval tv;
  struct timezone tz;
  struct tm *the_time;
 
  gettimeofday(&tv, &tz);
  the_time = localtime((time_t *) & (tv.tv_sec));
  logfile << the_time->tm_hour << ":" << the_time->tm_min << ":" << the_time->tm_sec << ":" << (int) (tv.tv_usec / 1000) << " >> Logfile " << log_filename << " closed \n" << flush;
  
  // closing of random logfile
  logfile.close();
  
}








