/***************************************************************************
                          sua_debug.h  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: sua_debug.h,v 1.1 2003/01/14 14:15:37 p82609 Exp $
 *
 * SUA implementation according to SUA draft issue 6.
 *
 * Author(s): Lode Coene
 *            Gery Verwimp
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * The alternative comment
 * inspiration : Michelle
 * "The debugger with the cape rides again"
 * "He will hunt bugs as long as they do not reside in dark memory lanes"
 *
 * Purpose: This header-file defines the SUA debugging constants used  
 *          for dumping debug info to the screen, dump debug info to file,
 *          activate ASP management...etc....
 */

#ifndef SUA_DEBUG_H
#define SUA_DEBUG_H

#define DEBUG                /* activates debuging msg on screen */
#define FILE_DEBUG           /* activates debugging info to output file */
#define SUA_MANAGEMENT       /* activates SUA management */
/*#define SG_ASP_MODE           implementation acts as ASP towards a SG */
/*#define SG_MODE               implementation acts as SG */
/*#define IPSP_SINGLE           implementation acts in IPSP single mode */

#endif  // SUA_DEBUG_H	

// end of module sua_debug.h











