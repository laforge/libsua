/***************************************************************************
                          sua_co.h  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: sua_co.h,v 1.1 2003/01/14 14:15:37 p82609 Exp $
 *
 * SUA implementation according to SUA draft issue 6.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * Purpose: This header-file defines the SUA connection-oriented message 
 *          handling:  
 *          - send a COnnect REquest msg to remote node
 *          - send a COnnect RESPonse msg to remote node      
 *          - send a CO DATA msg to remote node
 *          - send a CO RELease REQuest msg to remote node   
 *          - send a CO RELease COnfirm msg to remote node        
 *          - Process a COnnect REquest msg
 *          - Process a COnnect RESPonse msg 
 *          - Process a CO DATA msg
 *          - Process a CO RELease REQuest msg
 *          - Process a CO RELease COnfirm msg
 *          - Process a COnnect REFuse msg  
 */

#ifndef SUA_CO_H
#define SUA_CO_H

#include "sua_database.h"

#ifdef LINUX
    #include <unistd.h>
#endif


#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <string>


using namespace std;


/***********************************************************************/
/* Send sua connection-oriented request primitive to remote side       */
/***********************************************************************/
int sua_send_CORE( sccp_QOS_str     &QOS,
		   sccp_addr_str    &called_pty_address,
		   sccp_addr_str    &calling_pty_address,
		   char             *buffer,
		   unsigned int     len,
		   unsigned int     &Sua_ConnId,
		   tcb_Sua_TCB_str  *tcb_ptr
		 );

int sua_send_CORESP( sccp_QOS_str     &QOS,
		     char             *buffer,
		     unsigned int     len,
		     unsigned int     &Sua_ConnId,
		     tcb_Sua_TCB_str  *tcb_ptr
		   );

int sua_send_CODATA( sccp_QOS_str     &QOS,
		     char             *buffer,
		     unsigned int     len,
		     unsigned int     &Sua_ConnId,
		     tcb_Sua_TCB_str  *tcb_ptr
		   );

int sua_send_CORELRQ( sccp_QOS_str     &QOS,
		      char             *buffer,
		      unsigned int     len,
		      unsigned int     &Sua_ConnId,
		      tcb_Sua_TCB_str  *tcb_ptr
		    );

int sua_send_CORELCO( sccp_QOS_str     &QOS,
		      char             *buffer,
		      unsigned int     len,
		      unsigned int     &Sua_ConnId,
		      tcb_Sua_TCB_str  *tcb_ptr
		    );

int sua_send_COREF( sccp_QOS_str     &QOS,
		    char             *buffer,
		    unsigned int     len,
		    unsigned int     &Sua_ConnId,
		    tcb_Sua_TCB_str  *tcb_ptr
		    );

/***********************************************************************/
/* Receive a sua connection-oriented message from the remote side      */
/***********************************************************************/
short process_CORE_msg( unsigned int     sua_Assoc_id,
			tcb_Sua_TCB_str  *tcb_ptr,
			unsigned int     &Sua_ConnId,
			Sua_container    &sua_msg
		      );

short process_COAK_msg( unsigned int     local_sua_id,
			tcb_Sua_TCB_str  *tcb_ptr,
			unsigned int     &Sua_ConnId,
			Sua_container    &sua_msg
		      );

short process_CODATA_msg( unsigned int     local_sua_id,
			  tcb_Sua_TCB_str  *tcb_ptr,
			  unsigned int     &Sua_ConnId,
			  Sua_container    &sua_msg
			);

short process_CORELRQ_msg( unsigned int     local_sua_id,
			   tcb_Sua_TCB_str  *tcb_ptr,
			   unsigned int     &Sua_ConnId,
			   Sua_container    &sua_msg
		         );

short process_CORELCO_msg( unsigned int     local_sua_id,
			   tcb_Sua_TCB_str  *tcb_ptr,
			   unsigned int     &Sua_ConnId,
			   Sua_container    &sua_msg
		         );

short process_COREF_msg( unsigned int     local_sua_id,
			 tcb_Sua_TCB_str  *tcb_ptr,
			 unsigned int     &Sua_ConnId,
			 Sua_container    &sua_msg
		       );
#endif  // SUA_CO_H

// end of module sua_co.h


