/***************************************************************************
                          sua_database.h  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: sua_database.h,v 1.7 2003/08/28 09:30:20 p82609 Exp $
 *
 * SUA implementation according to SUA draft issue 8.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * The alternative comment
 * inspiration : Ann S.
 * "Don't mention the war."
 *
 * Purpose: This code-file defines the SUA database objects for:
 *          - Sybsystem Number(SSN) Object
 *          - Address Object
 *          - Local SUA Object
 *          - Local SUA List
 *          - Remote SUA object  
 *          - Remote SUA List      
 *          - SUA Name Object(host names/Global Titles)
 *          - SUA Name List     
 *          - SUA Association Object
 *          - SUA Association List
 *          - SUA Application Server Process (ASP) Object
 *          - SUA Application Server (AS) Object
 *          - SUA Application Server (AS) List 
 */

#ifndef SUA_DATABASE_H
#define SUA_DATABASE_H

#include "sua.h"
#include "sua_asp_mgnt.h"
#include "sua_snm_mgnt.h"

#include <string>



typedef enum {nc_empty, nc_ipv4, nc_ipv6} db_net_class;

const int db_MAX_MULTIHOME_ADDR = 2;
const int db_MAX_LOCAL_SUA = 10;
const int db_MAX_REMOTE_SUA = 1000;
const int db_MAX_SSN_SUA = 256;
const int db_MAX_SUA_AS = 100;
const int db_MAX_ASP_IN_1AS = 8; /* maximal 8 ASP's can be covered by a single AS */
const int db_MAX_1ASP_IN_AS = 4; /* 1 ASP can appear in maximal up till 4 different AS's */

/***********************************************************************/
/* Sua_SSNObject                                                       */
/***********************************************************************/
class db_Sua_SSNObject { 
 public:
  short ssn;
 public:
  void read_ssn(string ssn); 
}; ///:~

/***********************************************************************/
/* Sua_AddressObject                                                   */
/***********************************************************************/
class db_Sua_AddressObject { 
 public:
  int nr_of_addrs; // number of ip addresses used(multihoming)
  unsigned char address_string[db_MAX_MULTIHOME_ADDR][SCTP_MAX_IP_LEN];
  ipvxunion addrs[db_MAX_MULTIHOME_ADDR];
  SS7union pc;
 public:
  void read_addr_param(string address);
  void read_pointcode_param(string point_code);
  void read_port_num(string port);
}; ///:~

/***********************************************************************/
/* Sua_LocalObject                                                     */
/***********************************************************************/
class db_Sua_LocalObject{
 public:
  /* ssn supported */
  db_Sua_SSNObject      ssn;
  /* maximum number of streams for this instance */
  unsigned int          max_streams;
  /* SCTP registered local instance name */
  unsigned int          SCTP_instance_name;
  Sua_ULP_CallBacks     SUA_APLCallBack;
  /* local source address(es) of instance */
  db_Sua_AddressObject  Source;
  name_str              Name;
 public:
  // functions of the Class
  void initialize();
}; ///:~

/***********************************************************************/
/* Sua_LocalList                                                       */
/***********************************************************************/
class db_Sua_LocalList{
  public:
  int num_of_instance;
  db_Sua_LocalObject instance[db_MAX_LOCAL_SUA];  
  void initialize();
  void read_ssn(string ssn);
  short register_instance( SCTP_ulp_Callbacks APLCallbackFunctions,
			   Sua_ULP_CallBacks  SUACallbackFunctions
			  );
  short unregister_instance();
  void increase_instance();
  bool found( SS7union  pc);
}; ///:~

/***********************************************************************/
/* Sua_RemoteObject                                                    */
/***********************************************************************/
class db_Sua_RemoteObject{
 public:
  db_Sua_SSNObject       ssn;
  /* contains the association sua id with which it is linked. */
  unsigned int           SUA_assoc_id;
  /* status of the remote node */
  snm_Sua_pc_state_set   status;
  /* dest address of the remote sua node */
  db_Sua_AddressObject   Dest;
 public:
  // functions of the Class
  void initialize();
}; ///:~

/***********************************************************************/
/* Sua_RemoteList                                                      */
/***********************************************************************/
class db_Sua_RemoteList{
  public:
  int num_of_instance;
  public:
  db_Sua_RemoteObject instance[db_MAX_REMOTE_SUA];  
  void initialize();
  void read_ssn(string ssn);
  void increase_instance();
  bool found( SS7union  pc);
  void Dest_Available( SS7union  pc);
  void Dest_Unavailable( SS7union  pc);
  void Activate_dests( unsigned int sua_assoc_id);
  void Deactivate_dests( unsigned int sua_assoc_id);
  snm_Sua_pc_state_set get_Dest_status(SS7union pc);
}; ///:~

/***********************************************************************/
/* Sua_GlobalTitleObject                                                      */
/***********************************************************************/
class db_Sua_GlobalTitle {
 public:
  short                 Translation_Type;
  short                 Numbering_Plan;
  short		       	Nature_of_Address;
  /*short		nr_of_digits;*/
  string                digits;
}; ///:~

/***********************************************************************/
/* Sua_NameObject                                                      */
/***********************************************************************/
class db_Sua_NameObject {
 public:
  unsigned int          SUA_assoc_id;
  boolean               GT_present;
  db_Sua_GlobalTitle	GT;
  string                hostname;
 public:
  void initialize();

}; ///:~

/***********************************************************************/
/* Sua_NameList                                                        */
/***********************************************************************/
class db_Sua_NameList{
  public:
  int num_of_instance;
  db_Sua_NameObject instance[db_MAX_REMOTE_SUA];
 public:
  void initialize();
  void read_host_name( unsigned int  gtname_id,
			 string        name
		       );
  void read_Global_Title( unsigned int gtname_id,
			  unsigned int tt,
			  unsigned int na,
			  unsigned int np,
			  string       digits
			);
  signed int resolve_host_name( hostname_str&  dest_name,
				pointcode_str& dest_pc
			      );
  signed int perform_GTT( global_title_str&  cld_in,
			  global_title_str&  clg_in,
			  global_title_str&  cld_out,
			  global_title_str&  clg_out,
			  pointcode_str&     dest_pc
			);
  void increase_instance();
}; ///:~


/***********************************************************************/
/* Sua_ApplicationServerProcessObject (ASP)                            */
/***********************************************************************/
class db_Sua_ASPObject { 
 public:
  asp_Sua_asp_state_set status; /* status of the ASP */
  short linked_to_AS[db_MAX_1ASP_IN_AS]; /* this ASP may belong to different Application Servers(AS) */
 public:
  void initialize();
  bool activate( unsigned int sua_assoc_id, short mode );
  void deactivate( unsigned int sua_assoc_id, short mode );
  void down( unsigned sua_assoc_id, short mode );
  void up( unsigned sua_assoc_id, short mode );
}; ///:~

/***********************************************************************/
/* Sua_LS_OverRideObject (LS_OverRide)                                 */
/***********************************************************************/
class db_Sua_LS_OverRideObject { 
 private:
  short     ASP_to_use; /* ASP presently being the workhorse)*/
 public:
  void     assign_ASP(short ASP_num);
  short    ASP_used();
}; ///:~

/***********************************************************************/
/* Sua_LS_RoundRobinObject (LS_RoundRobin)                             */
/***********************************************************************/
class db_Sua_LS_RoundRobinObject { 
 private:
  short     ASP_to_use; /* ASP presently being the workhorse)*/
 public:
  short     ASP_used();
}; 

/***********************************************************************/
/* Sua_LoadShareObject (LS_AS)                                         */
/***********************************************************************/
class db_Sua_LoadShareObject { 
 public:
  asp_Sua_loadmode_set        load_mode; /* mode of loadsharing)*/
  db_Sua_LS_OverRideObject    override;  /* AS has a ASP in override */
  db_Sua_LS_RoundRobinObject  RRobin;    /* AS is RR all active ASP's */
}; ///:~


/***********************************************************************/
/* Sua_ApplicationServerObject (AS)                                    */
/***********************************************************************/
class db_Sua_ASObject { 
 public:
  asp_Sua_as_state_set  status; /* status of the AS */
  short ASP_linked_to_this_AS[db_MAX_ASP_IN_1AS];/* AS may contain a couple of ASP's */
  db_Sua_LoadShareObject loadshare;
 public:
  void initialize();
}; ///:~

/***********************************************************************/
/* Sua_ApplicationServerList (AS list)                                 */
/***********************************************************************/
class db_Sua_ASList{
  public:
  int num_of_instance;
  db_Sua_ASObject instance[db_MAX_SUA_AS];  
 public:
  void initialize();
  short read_AS( string       address_str, 
		 unsigned int sua_id 
	       );
  void override_ASP( unsigned int  asp_sua_assoc_id,
		     unsigned int  AS_id,
		     short         mode
		   );
}; ///:~

/***********************************************************************/
/* Sua_AssociationsObject                                              */
/***********************************************************************/
class db_Sua_AssociationObject { 
 public:
  unsigned int          SCTP_assoc_id;
  unsigned int          SCTP_instance_name;
  unsigned int          remote_sua_id;
  unsigned int          local_sua_id;
  short                 nr_of_inbound_streams;
  short                 nr_of_outbound_streams;
  boolean               init_assoc;
  db_Sua_ASPObject      asp;
  db_net_class          netw; // sort of network(v4/v6) to be used
  // ip source/destination addresses , should be the same network type
  db_Sua_AddressObject  Source;
  db_Sua_AddressObject  Dest;
 public:
  void initialize();
  short shutdown();
}; ///:~

/***********************************************************************/
/* Sua_AssociationList                                                 */
/***********************************************************************/
class db_Sua_AssociationList{
  public:
  int num_of_instance;
  db_Sua_AssociationObject instance[db_MAX_REMOTE_SUA];  
 public:
  void initialize();
  void read_Source_addr(string address);
  void read_Dest_addr(string address);
  void read_Source_pointcode(string address);
  void read_Dest_pointcode(string address);
  void read_Source_port(string port);
  void read_Dest_port(string port);
  void read_stream_number(string stream_num);
  unsigned int Find_association( unsigned int sctp_assoc_id,
				 unsigned int &Local_sua_id,
				 unsigned int &Remote_sua_id
			       );
  short associate_instance( db_Sua_LocalList&  local_sua,
			    db_Sua_RemoteList& remote_sua
			  );
  unsigned int passive_associate( unsigned int assoc_id,
				  db_Sua_LocalList& local_sua,
				  db_Sua_RemoteList& remote_sua,
				  unsigned short  nr_of_dest_addr,
				  unsigned short  nr_of_input_streams,
				  unsigned short  nr_of_output_streams
				);
  unsigned int Dynamic_Associate( db_Sua_LocalList&  local_sua,
				  db_Sua_RemoteList& remote_sua,
				  sccp_addr_str&     cld, 
				  sccp_addr_str&     clg,
				  unsigned short     nr_of_dest_addr,
				  unsigned short     nr_of_input_streams,
				  unsigned short     nr_of_output_streams
				);
  short Terminate_remote_assoc_instance();
  short shutdown();
  void increase_instance();
  bool activate( unsigned int sua_id,
		 short mode 
		 );
  void deactivate( unsigned int sua_id,
		   short mode 
		   );
  void down( unsigned int sua_id,
	     short mode 
	     );
  void up( unsigned int sua_id,
	   short mode 
	   );
}; ///:~


/***********************************************************************/
/* Sua_DatabaseList                                                 */
/***********************************************************************/
class db_Sua_DatabaseList{
  public:
  db_Sua_LocalList       local_sua;
  db_Sua_RemoteList      remote_sua;
  db_Sua_AssociationList AssocDB;
  db_Sua_NameList        NameDB;
  db_Sua_ASList          ApplicServ;
  public:
  void initialize();
  unsigned int Find_local_sua( sccp_addr_str&     local_address
			     );
  unsigned int Dynamic_Associate( sccp_addr_str&     cld,
				  sccp_addr_str&     clg,
				  unsigned short     nr_of_dest_addr,
				  unsigned short     nr_of_input_streams,
				  unsigned short     nr_of_output_streams
				);
  signed int route_on_SS7pc ( SS7union&  dest_pc,
			      SS7union&  org_pc,
			      int&       sua_assoc_id
			    );
  signed int route_on_IPpc ( ipvxunion& dest_pc,
			     ipvxunion& org_pc,
			     int&       sua_assoc_id
			   );
  signed int route_on_GTname ( hostname_str&  dest_name,
			       hostname_str&  org_name,
			       int&           sua_assoc_id,
			       pointcode_str& dest_pc,
			       pointcode_str& org_pc
			     );
  signed int route_on_GTT( global_title_str&  dest_GT,
			   global_title_str&  org_GT,
			   int&           		sua_assoc_id,
			   pointcode_str& 		dest_pc,
			   pointcode_str& 		org_pc
			 );
  signed int route_msg( sccp_addr_str& cld,
			sccp_addr_str& clg,
			int&           sua_assoc_id
			);
}; ///:~


#endif  // SUA_DATABASE_H

//end of module sua_database.h













