/***************************************************************************
                          sua_file.cpp  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: sua_file.cpp,v 1.7 2003/09/09 08:43:25 p82609 Exp $
 *
 * SUA implementation according to SUA draft issue 13.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * The alternative comment
 * inspiration : Martine
 * "This is the module of the fallen function with the big files by
 * "Von Craenendonck!"
 * "This module contains some real schoftware"
 *
 * Purpose: This code-file defines the SUA file access functions for:
 *          - get a argument in the line(delimited by a space)
 *          - read the sua file(containing arguments for the SUA database)     
 */


#ifdef LINUX
    #include <unistd.h>
#endif

#include "sua_debug.h"
#include "sua_database.h"

#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <string>
#include <fstream>

#include <netinet/in_systm.h>
#include <netinet/ip.h>         /* includes <netinet/in.h> also ! */
#include <netinet/ip_icmp.h>
#include <netdb.h>
#include <arpa/inet.h>

#define SUA_PORT 14001
#define SUA_PPI 4

using namespace std;

/***********************************************************************/
/* get_argument                                                        */
/***********************************************************************/
string get_argument(string line,
		    int& begin,
		    int& end,
		    unsigned int& num_of_char_in_line)
{ 
  int len = 0;
  bool cont = true;
  while  ((num_of_char_in_line <= line.length()) && (cont))
    { 
     
      if ((line[num_of_char_in_line] == ' ') ||
	  (num_of_char_in_line >= line.length()))
	{
	  end = num_of_char_in_line - 1;
	  len = num_of_char_in_line - begin;
	  cont = false;
	}
      num_of_char_in_line++;

    }

  char *temp = new char[len];
 
  // convert to lowercase characters for easier processing
  int i=0;
  for(i = 0; i < len ; i++)
    temp[i] = tolower(line[begin+i]);
  string sua_arg(temp, len);
  
#ifdef DEBUGFILE
  cout << "indexes = "<< begin << " , " << end << " , " << len << "\n";
  cout << temp << "\n";
  cout << "got argument  = " << sua_arg  << " \n";
  char v;
  cin >> v;
#endif

  begin = end + 2;
  end = 0;
  delete temp;

  return(sua_arg);
}

/***********************************************************************/
/* create host parameter handling                                      */
/***********************************************************************/
void sua_cr_host( string                   host_parm_str,
		  db_Sua_LocalList&        local_sua 		  
		)
{
  int    host_id;
  short  cpy_start, cpy_len;

  /* get host id from param string, use as index in local_sua table */
  if (host_parm_str.find("host_id=") != host_parm_str.npos)
    {
      cpy_start = (host_parm_str.find("host_id=")+8);
      cpy_len = (host_parm_str.find(",org_ip")) - cpy_start ;
      string host_id_str( host_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      /*cout << "Host id = " << host_id_str << "\n";*/
#endif
      host_id = atoi(host_id_str.c_str());
    }
  else
    return;

  /* get all(=multihomed) addresses till next parameter */
  /* fill in IP address as string in local_sua */
  if (host_parm_str.find("org_ip=") != host_parm_str.npos)
    {
      cpy_start = (host_parm_str.find("org_ip=")+7);
      cpy_len = (host_parm_str.find(",org_port")) - cpy_start ;
      string ip_addr_str( host_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      /*cout << "org ip addr = " << ip_addr_str << "\n";*/
#endif
      local_sua.instance[host_id].Source.read_addr_param(ip_addr_str);
    }
  else
    return;
  
  /* get the local port number for this instance */
  if (host_parm_str.find("org_port=") != host_parm_str.npos)
    {
      cpy_start = (host_parm_str.find("org_port=")+9);
      cpy_len = (host_parm_str.find(",ssn")) - cpy_start ;
      string portnumber_str( host_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      /*cout << "org portnumber = " << portnumber_str << "\n";*/
#endif
      local_sua.instance[host_id].Source.read_port_num(portnumber_str);
    }
  else
    local_sua.instance[host_id].Source.read_port_num("14001");
  
  /* get the local ssn for this local sua host instantiation */
  if (host_parm_str.find("ssn=") != host_parm_str.npos)
    {
      cpy_start = (host_parm_str.find("ssn=")+4);
      cpy_len = (host_parm_str.find(",streams")) - cpy_start ;
      string ssn_str( host_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      /*cout << "org ssn = " << ssn_str << "\n";*/
#endif
      local_sua.instance[host_id].ssn.ssn =  atoi(ssn_str.c_str());
    }
  else
    local_sua.instance[host_id].ssn.ssn =  255;
  
  /* get the number of streams supported for this sua instantiation */
  if (host_parm_str.find("streams=") != host_parm_str.npos)
    {
      cpy_start = (host_parm_str.find("streams=")+8);
      cpy_len = (host_parm_str.find(";")) - cpy_start ;
      string max_streams_str( host_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      /*cout << "Max number of streams = " << max_streams_str << "\n";*/
#endif
      local_sua.instance[host_id].max_streams= atoi(max_streams_str.c_str()); 
    }
  else
     local_sua.instance[host_id].max_streams= 16;
  
  local_sua.increase_instance();

  return;

}

/***********************************************************************/
/* create association parameter handling                               */
/***********************************************************************/
void sua_cr_assoc( string                   assoc_parm_str,
		   db_Sua_AssociationList&  Assoc_sua,
		   db_Sua_LocalList&        local_sua 	
		 )
{
  int    assoc_id;
  int    host_id;
  short  cpy_start, cpy_len;

  /* get assoc id from param string, use as index in Assoc_sua table */
  if (assoc_parm_str.find("assoc_id=") != assoc_parm_str.npos)
    {
      cpy_start = (assoc_parm_str.find("assoc_id=")+9);
      cpy_len = (assoc_parm_str.find(",init")) - cpy_start ;
      string assoc_id_str( assoc_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      /*cout << "Assoc id = " << assoc_id_str << "\n";*/
#endif
      assoc_id = atoi(assoc_id_str.c_str());
    }
  else
    return;

  /* get init from param string */
  /* indicates pasive or active association setup*/
  if (assoc_parm_str.find("init=") != assoc_parm_str.npos)
    {
      cpy_start = (assoc_parm_str.find("init=")+5);
      cpy_len = (assoc_parm_str.find(",host_id")) - cpy_start ;
      string init_str( assoc_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      /*cout << "Init = " << init_str << "\n";*/
#endif
      if (init_str.find("true") != init_str.npos)
	Assoc_sua.instance[assoc_id].init_assoc = true;
      else
	Assoc_sua.instance[assoc_id].init_assoc = false;
    }
  else
    Assoc_sua.instance[assoc_id].init_assoc = false;

 /* get host id from param string, use as index  */
  if (assoc_parm_str.find("host_id=") != assoc_parm_str.npos)
    {
      cpy_start = (assoc_parm_str.find("host_id=")+8);
      cpy_len = (assoc_parm_str.find(",dest_ip")) - cpy_start ;
      string host_id_str( assoc_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      /*cout << "Host id = " << host_id_str << "\n";*/
#endif
      host_id = atoi(host_id_str.c_str());
    }
  else
    return;  

  /* fill in the local_sua instance = host_id */
  Assoc_sua.instance[assoc_id].local_sua_id = host_id;

  /* fill in the source address */
  Assoc_sua.instance[assoc_id].Source = local_sua.instance[host_id].Source;

  /* get all(=multihomed) addresses till next parameter */
  /* fill in IP address as string & network in assoc_sua */
  if (assoc_parm_str.find("dest_ip=") != assoc_parm_str.npos)
    {
      cpy_start = (assoc_parm_str.find("dest_ip=")+8);
      cpy_len = (assoc_parm_str.find(",dest_port")) - cpy_start ;
      string ip_addr_str( assoc_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      /*cout << "org ip addr = " << ip_addr_str << "\n";*/
#endif
      Assoc_sua.instance[assoc_id].Dest.read_addr_param(ip_addr_str);
    }
  else
    return;

  /* get the remote port number for this assoc instance */
  if (assoc_parm_str.find("dest_port=") != assoc_parm_str.npos)
    {
      cpy_start = (assoc_parm_str.find("dest_port=")+10);
      cpy_len = (assoc_parm_str.find(";")) - cpy_start ;
      string portnumber_str( assoc_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      /*cout << "org portnumber = " << portnumber_str << "\n";*/
#endif
      Assoc_sua.instance[assoc_id].Dest.read_port_num(portnumber_str);
    }
  else
    Assoc_sua.instance[assoc_id].Dest.read_port_num("14001");
 
  /* fill in the outgoing and incoming stream number */
  Assoc_sua.instance[assoc_id].nr_of_inbound_streams = local_sua.instance[host_id].max_streams;
  Assoc_sua.instance[assoc_id].nr_of_outbound_streams = local_sua.instance[host_id].max_streams;

  /* fill in the remote_sua instance = assoc_id */
  Assoc_sua.instance[assoc_id].remote_sua_id = assoc_id;

  Assoc_sua.increase_instance();

  return;

}

/***********************************************************************/
/* create origination pointcode parameter handling                     */
/***********************************************************************/
void sua_cr_orgpc( string                   orgpc_parm_str,
		   db_Sua_LocalList&        local_sua 	
		   )
{
  int    host_id, pc;
  short  cpy_start, cpy_len;

  /* get host id from param string, use as index local_sua table */
  if (orgpc_parm_str.find("host_id=") != orgpc_parm_str.npos)
    {
      cpy_start = (orgpc_parm_str.find("host_id=")+8);
      cpy_len = (orgpc_parm_str.find(",pc")) - cpy_start ;
      string host_id_str( orgpc_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      cout << "Host id = " << host_id_str << "\n";
#endif
      host_id = atoi(host_id_str.c_str());
    }
  else 
    return;

  /* get origination pointcode from param string  */
  if (orgpc_parm_str.find("pc=") != orgpc_parm_str.npos)
    {
      cpy_start = (orgpc_parm_str.find("pc=")+3);
      cpy_len = (orgpc_parm_str.find(";")) - cpy_start ;
      string orgpc_str( orgpc_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      cout << "Pointcode = " << orgpc_str << "\n";
#endif
      pc = atoi(orgpc_str.c_str());
    }
  else
    return;

  /* fill in the local_sua instance pointcode */
  local_sua.instance[host_id].Source.pc.ITU24.family = ITU14bit;
  local_sua.instance[host_id].Source.pc.ITU24.pc = pc;    
  
  return;

}

/***********************************************************************/
/* create origination Global title handling                            */
/***********************************************************************/
void sua_cr_orgGT( string                   orggt_parm_str,
		   db_Sua_LocalList&        local_sua 	
		   )
{
  int            host_id;
  unsigned int   nature_of_addr, number_plan,translation_type;
  short          cpy_start, cpy_len;

  /* get host id from param string, use as index local_sua table */
  if (orggt_parm_str.find("host_id=") != orggt_parm_str.npos)
    {
      cpy_start = (orggt_parm_str.find("host_id=")+8);
      cpy_len = (orggt_parm_str.find(",")) - cpy_start ;
      string host_id_str( orggt_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      cout << "Host id = " << host_id_str << "\n";
#endif
      host_id = atoi(host_id_str.c_str());
    }
  else 
    return;

  /* get origination Global Title from param string  */
  /* default translation type number = 0 */
  translation_type = 0;
  /* default nature of address = 0 */  
  nature_of_addr = 0;
  /* default number plan(E164) = 0 */
  number_plan = 0;
  
  if (orggt_parm_str.find("tt=") != orggt_parm_str.npos)
    {
      cpy_start = (orggt_parm_str.find("tt=")+3);
      cpy_len = (orggt_parm_str.find(",na")) - cpy_start ;
      string ttn_str( orggt_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      cout << "tt = " << ttn_str << "\n";
#endif
      translation_type = atoi(ttn_str.c_str());
    }
	
  /* get nature of address from param string  */
  if (orggt_parm_str.find("na=") != orggt_parm_str.npos)
    {
      cpy_start = (orggt_parm_str.find("na=")+3);
      cpy_len = (orggt_parm_str.find(",np")) - cpy_start ;
      string nature_addr_str( orggt_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      cout << "Nature of address = " << nature_addr_str << "\n";
#endif
      nature_of_addr = atoi(nature_addr_str.c_str());
    }
  
  /* get numbering plan from param string  */
  if (orggt_parm_str.find("np=") != orggt_parm_str.npos)
    {
      cpy_start = (orggt_parm_str.find("np=")+3);
      cpy_len = (orggt_parm_str.find(",digits")) - cpy_start ;
      string number_plan_str( orggt_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      /*cout << "Nature of address = " << number_plan_str << "\n";*/
#endif
      number_plan = atoi(number_plan_str.c_str());
    }
      
  /* get digits from param string  */
  if (orggt_parm_str.find("digits=") != orggt_parm_str.npos)
    {
      cpy_start = (orggt_parm_str.find("digits=")+7);
      cpy_len = (orggt_parm_str.find(";")) - cpy_start ;
      string digit_str( orggt_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      cout << "Number of digits = " << digit_str.length() << ", Digits = " << digit_str << "\n";
#endif
      /* fill in the local_sua instance Global Title */
      local_sua.instance[host_id].Name.GT.nr_of_digits = digit_str.length() ;
      digit_str.copy(local_sua.instance[host_id].Name.GT.digits, digit_str.length());    
      
    }
  else
    return;

  local_sua.instance[host_id].Name.GT.Translation_Type = translation_type;
  local_sua.instance[host_id].Name.GT.Numbering_Plan = number_plan;
  local_sua.instance[host_id].Name.GT.Nature_of_Address = nature_of_addr;
  
  return;

}

/***********************************************************************/
/* create destination pointcode parameter handling                     */
/***********************************************************************/
void sua_cr_destpc( string                   destpc_parm_str,
		    db_Sua_RemoteList&       remote_sua 	
		  )
{
  int    dest_id, pc;
  short  cpy_start, cpy_len;

  /* get pc id from param string, use as index remote_sua table */
  if (destpc_parm_str.find("dest_id=") != destpc_parm_str.npos)
    {
      cpy_start = (destpc_parm_str.find("dest_id=")+8);
      cpy_len = (destpc_parm_str.find(",pc")) - cpy_start ;
      string dest_id_str( destpc_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      cout << "Destination id = " << dest_id_str << "\n";
#endif
      dest_id = atoi(dest_id_str.c_str());
    }
  else 
    return;

  /* get destination pointcode from param string  */
  if (destpc_parm_str.find("pc=") != destpc_parm_str.npos)
    {
      cpy_start = (destpc_parm_str.find("pc=")+3);
      cpy_len = (destpc_parm_str.find(";")) - cpy_start ;
      string pc_str( destpc_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      cout << "Pointcode = " << pc_str << "\n";
#endif
      pc = atoi(pc_str.c_str());
    }
  else
    return;

  /* fill in the remote_sua instance pointcode */
  remote_sua.instance[dest_id].Dest.pc.ITU24.family = ITU14bit;
  remote_sua.instance[dest_id].Dest.pc.ITU24.pc = pc;

  if (dest_id > remote_sua.num_of_instance) 
    remote_sua.num_of_instance++;

  return;

}


/***********************************************************************/
/* create destination IP address parameter handling                    */
/***********************************************************************/
void sua_cr_destip( string                   destip_parm_str,
		    db_Sua_RemoteList&       remote_sua 	
		  )
{
  int    dest_id;
  short  cpy_start, cpy_len;

  /* get pc id from param string, use as index remote_sua table */
  if (destip_parm_str.find("dest_id=") != destip_parm_str.npos)
    {
      cpy_start = (destip_parm_str.find("dest_id=")+8);
      cpy_len = (destip_parm_str.find(",ip")) - cpy_start ;
      string dest_id_str( destip_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      cout << "Destination id = " << dest_id_str << "\n";
#endif
      dest_id = atoi(dest_id_str.c_str());
    }
  else 
    return;

  /* get destination IP address from param string  */
  if (destip_parm_str.find("ip=") != destip_parm_str.npos)
    {
      cpy_start = (destip_parm_str.find("ip=")+3);
      cpy_len = (destip_parm_str.find(";")) - cpy_start ;
      string ip_str( destip_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      cout << "IP address = " << ip_str << "\n";
#endif

      /* fill in the remote_sua instance ip address */
      remote_sua.instance[dest_id].Dest.read_addr_param(ip_str);  
    }
  else
    return;

 if (dest_id > remote_sua.num_of_instance) 
    remote_sua.num_of_instance++;

  return;

}

/***********************************************************************/
/* create hostname parameter handling                                  */
/***********************************************************************/
void sua_cr_destname( string                   name_parm_str,
		      db_Sua_NameList&         Sua_NameDB
		    )
{
  int            dname_id;
  unsigned int   nature_of_addr, number_plan,translation_type;
  short          cpy_start, cpy_len;

  /* get namegt id from param string, use as index in sua.NameDB table */
  if (name_parm_str.find("dname_id=") != name_parm_str.npos)
    {
      cpy_start = (name_parm_str.find("dname_id=")+9);
      cpy_len = (name_parm_str.find(",")) - cpy_start ;
      string dname_id_str( name_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      cout << "Namegt id = " << dname_id_str << "\n";
#endif
      dname_id = atoi(dname_id_str.c_str());
    }
  else
    return;

  /* get hostname from param string  */
  if (name_parm_str.find("name=") != name_parm_str.npos)
    {
      cpy_start = (name_parm_str.find("name=")+5);
      cpy_len = (name_parm_str.find(";")) - cpy_start ;
      string name_str( name_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      cout << "Hostname = " << name_str << "\n";
#endif
      Sua_NameDB.read_host_name( dname_id, 
				 name_str
			       );
      Sua_NameDB.increase_instance();

    }
  else 
    {
      /* default translation type number = 0 */
      translation_type = 0;
      /* default nature of address = 0 */  
      nature_of_addr = 0;
      /* default number plan(E164) = 0 */
      number_plan = 0;

      if (name_parm_str.find("tt=") != name_parm_str.npos)
	{
	  cpy_start = (name_parm_str.find("tt=")+3);
	  cpy_len = (name_parm_str.find(",na")) - cpy_start ;
	  string ttn_str( name_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
	  cout << "tt = " << ttn_str << "\n";
#endif
	  translation_type = atoi(ttn_str.c_str());
	}
	
      /* get nature of address from param string  */
      if (name_parm_str.find("na=") != name_parm_str.npos)
	{
	  cpy_start = (name_parm_str.find("na=")+3);
	  cpy_len = (name_parm_str.find(",np")) - cpy_start ;
	  string nature_addr_str( name_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
	  cout << "Nature of address = " << nature_addr_str << "\n";
#endif
	  nature_of_addr = atoi(nature_addr_str.c_str());
	}
           
      /* get numbering plan from param string  */
      if (name_parm_str.find("np=") != name_parm_str.npos)
	{
	  cpy_start = (name_parm_str.find("np=")+3);
	  cpy_len = (name_parm_str.find(",digits")) - cpy_start ;
	  string number_plan_str( name_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
	  /*cout << "Nature of address = " << number_plan_str << "\n";*/
#endif
	  number_plan = atoi(number_plan_str.c_str());
	}
      
      /* get digits from param string  */
      if (name_parm_str.find("digits=") != name_parm_str.npos)
	{
	  cpy_start = (name_parm_str.find("digits=")+7);
	  cpy_len = (name_parm_str.find(";")) - cpy_start ;
	  string digit_str( name_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
	  cout << "Number of digits = " << digit_str.length() << ", Digits = " << digit_str << "\n";
#endif
	  Sua_NameDB.read_Global_Title( dname_id,
					translation_type,
					nature_of_addr,
					number_plan,
					digit_str
					);
	  Sua_NameDB.increase_instance();
	}
      else
	cout << "Unknown cr name cmd parameters\n";
    
    }

  return;

}

/***********************************************************************/
/* create route pointcode parameter handling                           */
/***********************************************************************/
void sua_cr_route_PC( string                   route_parm_str,
		      db_Sua_RemoteList&       remote_sua,
		      db_Sua_AssociationList&  assoc_sua
		    )
{
  int    dest_id, sua_assoc_id;
  short  cpy_start, cpy_len;

  /* get pc id from param string, use as index remote sua table */
  if (route_parm_str.find("dest_id=") != route_parm_str.npos)
    {
      cpy_start = (route_parm_str.find("dest_id=")+8);
      cpy_len = (route_parm_str.find(",assoc_id")) - cpy_start ;
      string dest_id_str( route_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      cout << "destination id = " << dest_id_str << "\n";
#endif
      dest_id = atoi(dest_id_str.c_str());
    }
  else
    return;

  /* get destination assoc id from param string  */
  if (route_parm_str.find("assoc_id=") != route_parm_str.npos)
    {
      cpy_start = (route_parm_str.find("assoc_id=")+9);
      cpy_len = (route_parm_str.find(";")) - cpy_start ;
      string assoc_id_str( route_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      cout << "Association Id = " << assoc_id_str << "\n";
#endif
      sua_assoc_id = atoi(assoc_id_str.c_str());
    }
  else
    return;

  if (dest_id > 0){
    remote_sua.instance[dest_id].SUA_assoc_id = sua_assoc_id;
  }
 

  return;

}

/***********************************************************************/
/* create route global title parameter handling                        */
/***********************************************************************/
void sua_cr_route_name( string                   route_parm_str,
			db_Sua_NameList&         Sua_NameDB,
			db_Sua_AssociationList&  Sua_AssocDB
		    )
{
  int    dname_id, sua_assoc_id;
  short  cpy_start, cpy_len;

  /* get namegt id from param string, use as index name-gt table */
  if (route_parm_str.find("dname_id=") != route_parm_str.npos)
    {
      cpy_start = (route_parm_str.find("dname_id=")+9);
      cpy_len = (route_parm_str.find(",")) - cpy_start ;
      string dname_id_str( route_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      cout << "routename id = " << dname_id_str << "\n";
#endif
      dname_id = atoi(dname_id_str.c_str());
    }
  else
    return;

  /* get destination assoc id from param string  */
  if (route_parm_str.find("assoc_id=") != route_parm_str.npos)
    {
      cpy_start = (route_parm_str.find("assoc_id=")+9);
      cpy_len = (route_parm_str.find(";")) - cpy_start ;
      string assoc_id_str( route_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      cout << "Association Id = " << assoc_id_str << "\n";
#endif
      sua_assoc_id = atoi(assoc_id_str.c_str());
    }
  else
    return;

  
  if (dname_id > 0)
    Sua_NameDB.instance[dname_id].SUA_assoc_id = sua_assoc_id;

  return;

}

/***********************************************************************/
/* create routing context parameter handling                           */
/***********************************************************************/
void sua_cr_routing_context( string                   rc_parm_str,
			     db_Sua_DatabaseList&     sua 	
			   )
{
  int    rc_id;
  short  cpy_start, cpy_len;

  /* get pc id from param string, use as index remote_sua table */
  if (rc_parm_str.find("rc_id=") != rc_parm_str.npos)
    {
      cpy_start = (rc_parm_str.find("rc_id=")+8);
      cpy_len = (rc_parm_str.find(",pc")) - cpy_start ;
      string rc_id_str( rc_parm_str, cpy_start, cpy_len);
#ifdef DEBUG
      cout << "Routing Context id = " << rc_id_str << "\n";
#endif
      rc_id = atoi(rc_id_str.c_str());
    }
  else 
    return;

  /* get destination pointcode from param string  */
 

  return;

}


/***********************************************************************/
/* read sua configuration file                                         */
/***********************************************************************/
int read_sua_conf_file( string                filename, 
		        db_Sua_DatabaseList&  sua
		      )
{
  string sua_filename;
  string sua_arg;
  char readBuffer[256];

  cout << "SUA configuration file to read = " << filename  << " \n";

  cout << "SUA local sua instantation parameters(mandatory) \n";
  cout << "cr host:host_id = j, org_ip= t.t.t.t, org_port= yyyyy  \n";
  cout << "cr assoc:assoc_id= i, init= false, host_id = j, dest_ip = x.x.x.x, dest_port= yyyy \n";
  cout << "cr orgpc: pc = wwww, host_id = j\n";
  cout << "cr orggt: host_id=j,tt=tt,na=nn,np=ppp,digits= 123456789abcdef \n"; 
  cout << "cr destpc: pc_id = i,pc = wwww, rc = x \n";
  cout << "cr name: namegt_id=k,host= www.sctp.be \n";
  cout << "cr name: namegt_id=k,tt=tt,na=nn,np=ppp,digits= 123456789abcdef \n";
  cout << "cr route: pc_id = j, assoc_id = i           AND/OR\n";
  cout << "cr route: dest_ip = x.x.x.x, assoc_id = i  AND/OR\n";
  cout << "cr route: namegt_id= k, assoc_id = i  \n";

  ifstream infile(filename.c_str());

  while (infile.getline(readBuffer,256,'\n'))
    {
      string cmdline(readBuffer,strlen(readBuffer));

      if (cmdline.find("/") != cmdline.npos)
	{
	  /* comment line: do not execute */
	}
      else if (cmdline.find("cr host:") != cmdline.npos)
	{
	  string parm_str( cmdline, (cmdline.find("cr host:")+8),cmdline.length());
	  sua_cr_host( parm_str,
		       sua.local_sua
		     );
	} 
      else if (cmdline.find("cr assoc:") != cmdline.npos)
	{
	  string parm_str( cmdline, (cmdline.find("cr assoc:")+9),cmdline.length());
	  sua_cr_assoc( parm_str,
		        sua.AssocDB,
		        sua.local_sua 	
		      );
	} 
      else if (cmdline.find("cr orgpc:") != cmdline.npos)
	{
	 string parm_str( cmdline, (cmdline.find("cr orgpc:")+9),cmdline.length());
	  sua_cr_orgpc( parm_str,
			sua.local_sua 	
		      ); 
	}
      else if (cmdline.find("cr orggt:") != cmdline.npos)
	{
	 string parm_str( cmdline, (cmdline.find("cr orggt:")+9),cmdline.length());
	  sua_cr_orgGT( parm_str,
			sua.local_sua 	
		      ); 
	}  
      else if (cmdline.find("cr destpc:") != cmdline.npos)
	{
	 string parm_str( cmdline, (cmdline.find("cr destpc:")+10),cmdline.length());
	  sua_cr_destpc( parm_str,
		         sua.remote_sua 	
		       ); 
	} 
      else if (cmdline.find("cr destip:") != cmdline.npos)
	{
	 string parm_str( cmdline, (cmdline.find("cr destip:")+10),cmdline.length());
	  sua_cr_destip( parm_str,
		         sua.remote_sua 	
		       ); 
	} 
      else if (cmdline.find("cr destname:") != cmdline.npos)
	{
	  string parm_str( cmdline, (cmdline.find("cr destname:")+12),cmdline.length());
	  sua_cr_destname( parm_str,
			   sua.NameDB 	
			 );
	  parm_str.erase();
	} 
      else if (cmdline.find("cr route:dest_id") != cmdline.npos)
	{
	  string parm_str( cmdline, (cmdline.find("cr route:")+9),cmdline.length());
	  sua_cr_route_PC( parm_str,
			   sua.remote_sua,
			   sua.AssocDB
			 ); 
	} 
      else if (cmdline.find("cr route:dname_id") != cmdline.npos)
	{
	  string parm_str( cmdline, (cmdline.find("cr route:")+9),cmdline.length());
	  sua_cr_route_name( parm_str,
			     sua.NameDB,
			     sua.AssocDB
			   ); 
	} 
      else if (cmdline.find("cr rc:rc_id") != cmdline.npos)
	{
	  string parm_str( cmdline, (cmdline.find("cr rc:")+9),cmdline.length());
	  sua_cr_routing_context( parm_str,
				  sua
				); 
	} 
      else if (cmdline.find("cr xxxx:") != cmdline.npos)
	{
	  ; 
	} 
      else if (cmdline.length() == 0)

	;

      else

	cout << "ERROR: Unknown " << cmdline << "command\n";

      cout << cmdline << "\n";

    }

  return(0);

} /* end of read_sua_conf_file */

// end of module sua_file.c





