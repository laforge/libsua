/***************************************************************************
                          sua.h  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: sua.h,v 1.4 2003/09/09 08:43:25 p82609 Exp $
 *
 * SUA implementation according to SUA draft issue 13.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * The alternative comment
 * inspiration : Marleen
 * "El Loco was here."
 * "He drinks boiling motor oil and likes his meat alive in the cantina."
 *
 * Purpose: This header-file defines the SUA interface used by the 
 *          SUA application for:
 *          - SUA address information structure
 *          - SUA Quality-Of-Service(QOS) structure
 *          - SUA callback function structure
 *          and adaptation functions used by the SUA application for:
 *          - initialising the SUA layer
 *          - reading the SUA configuration file
 *          - registering a local SUA instance
 *          - Setting up the association with a remote SUA node
 *          - get info on a certain (SUA) association
 *          - terminate a association
 *          - Send a SUA primtive to SUA
 *          - Receive a SUA primitive from SUA
 *          - get the SUA destination pointcode and status
 *          - get the SUA origination pointcode
 *          - activate the SUA association
 *          - deactivate the SUA association
 *          - do a SS7 mngt audit(DAUD) on all destinations 
 */

#define __EXTENSIONS__
#ifndef SUA_GLOBAL_H
#define SUA_GLOBAL_H

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <string>

#ifdef  STDC_HEADERS
 #ifdef  HAVE_SYS_TIME_H
  #include <sys/time.h>
  #ifdef TIME_WITH_SYS_TIME
   #include <time.h>
  #endif
 #endif
 #ifdef  HAVE_UNISTD_H
  #include <unistd.h>
 #endif
#endif

#ifdef FreeBSD
#include <netinet/in_systm.h>
#include <sys/types.h>
#endif

#ifdef SOLARIS
#include <netinet/in_systm.h>
#include <stdarg.h>
#endif

#include <netinet/in.h>

#ifdef DARWIN
#include <stdint.h>
#endif

/* turn on  Posix 1g for compatible cmsg structure */
#ifdef USE_RFC2292BIS
   #define _XPG4_2
#endif

#include <sys/socket.h>

/* turn off  Posix 1g for compatible cmsg structure */
#ifdef USE_RFC2292BIS
  #undef _XPG4_2
#endif

#include <netdb.h>
#include <arpa/inet.h>

#include "sctp.h"  /* SCTP library */

using namespace std;

#define SUA_PORT 14001
#define SUA_PPI 4

#define SUA_MAX_MSG_LEN  2000

/*#define HAVE_UNISTD_H TRUE */

#ifndef FALSE
#define FALSE (0)
#define TRUE (!FALSE)
#endif

#define N_UNITDATA 1
#define N_NOTICE 2
#define N_CONNECT_REQ 10
#define N_CONNECT_IND 11
#define N_CONNECT_REFUSED 12
#define N_CONNECT_RESP 13
#define N_CONNECT_CONF 14
#define N_DATA_REQ 20
#define N_DATA_IND 21
#define N_RELEASE_REQ 30
#define N_RELEASE_CONF 31

#define SUA_UDTS_NO_TRANSLATION_FOR_NATURE_OF_ADDRES   0x00
#define SUA_UDTS_NO_TRANSLATION_FOR_SPECIFIC_ADDRES    0x01
#define SUA_UDTS_SUBSYTEM_CONGESTION                   0x02
#define SUA_UDTS_SUBSYTEM_FAILURE                      0x03
#define SUA_UDTS_UNEQUIPPED_USER                       0x04
#define SUA_UDTS_MTP_FAILURE                           0x05
#define SUA_UDTS_NETWORK_CONGESTION                    0x06
#define SUA_UDTS_UNQUALIFIED                           0x07
#define SUA_UDTS_ERROR_IN_MSG_TRANSPORT                0x08
#define SUA_UDTS_ERROR_IN_LOCAL_PROCESSING             0x09
#define SUA_UDTS_DESTINATION_REASSEMBLY_FAILURE        0x0A
#define SUA_UDTS_SCCP_FAILURE                          0x0B
#define SUA_UDTS_HOP_COUNTER_VIOLATION                 0x0C
#define SUA_UDTS_SEGMENTATION_NOT_SUPPORTED            0x0D
#define SUA_UDTS_SEGMENTATION_FAILURE                  0x0E

typedef unsigned char boolean;

#define OK                             0
#define PROTOCOL_CLASS_NOT_SPECIFIED -10
#define INVALID_CLG_ADDRESS          -20
#define INVALID_CLD_ADDRESS          -30
#define MSG_ENCODING_FAILED          -40

/* shortcut macro to specify address field of struct sockaddr */
#define sockunion_family(X)  (X)->sa.sa_family
#define sock2ip(X)   (((struct sockaddr_in *)(X))->sin_addr.s_addr)
#define sock2ip6(X)  (((struct sockaddr_in6 *)(X))->sin6_addr.s6_addr)

#define SUPPORT_ADDRESS_TYPE_IPV4        0x00000001
#define SUPPORT_ADDRESS_TYPE_IPV6        0x00000002
#define SUPPORT_ADDRESS_TYPE_DNS         0x00000004


/* union for handling either type of addresses: ipv4 and ipv6 */
/*#ifndef SOCKUNION_DEFINE*/
#define SOCKUNION_DEFINE    1
/* define structure of IPv4/6 address */
union sockunion
{
  struct sockaddr sa;
  struct sockaddr_in sin;
  struct sockaddr_in6 sin6;
  unsigned char ch[SCTP_MAX_IP_LEN];
};
/*#endif   SOCKUNION_DEFINE */

#define ipvxunion  sockunion

/*union ipvxunion
{
  struct sockaddr sa;
  struct sockaddr_in sin;
  struct sockaddr_in6 sin6;
  unsigned char ch[SCTP_MAX_IP_LEN];
};*/

typedef enum { ITU14bit,
               ITU24bit,  
	       ANSI24bit
} SS7pc_set; 

typedef struct {
  SS7pc_set  family;
  uint32_t   pc;
} SS7_ITU14_str;

/* define structure of SS7 MTP pointcode (ITU/ANSI/other...) */
union SS7union
{
  SS7_ITU14_str ITU14;
  SS7_ITU14_str ITU24;
  SS7_ITU14_str ANSI24;
};

typedef enum { no_pc_present,
               ipvx_pc_present,   // IPvx(4 or 6) present in pc field
	       ss7_pc_present     // SS7 pointcode present in pc field
} pointcode_set; 

typedef enum { no_name_present,
               hostname_present, 
	       GT_present
} name_gt_set; 

typedef enum { no_sap_present,
	       ssn_present,
	       portnumber_presentr
} application_SAP_set; 

typedef enum { include_nothing,
	       include_pc_in_header,
	       include_ssn_port_in_header
} sua_header_set; 

typedef struct {
  pointcode_set            pc;
  name_gt_set              name_gt;
  application_SAP_set      ssn_port;
  sua_header_set           field_in_header;
} addr_elements_str;

typedef struct {
  SS7union               ss7;
  sockunion              ipvx;
  /*ipvxunion              ipvx;*/
} pointcode_str;

typedef char hostname_str[255];

typedef struct {
	short 	        Translation_Type;
	short		Numbering_Plan;
	short		Nature_of_Address;
	short   nr_of_digits;
	char 	 	digits[255];
} global_title_str;

typedef union {
  global_title_str        GT;
  hostname_str            HostName;
}name_str;

typedef short application_sap_str;

typedef enum { route_on_ssn,                  // use pointcode for routing
	       route_on_name_gt,              // use gt/name for routing
	       route_on_name_gt_next_office,  // perform GTT in next node
	       no_address_present             // no routing requested
} routing_type_set;

typedef struct {
  addr_elements_str        address_fields_present; // which fields are present?
  pointcode_str            pc;                     // pointcode field
  name_str                 name;                   //global title/hostname field
  application_sap_str      ssn;                    // application ssn/port
  routing_type_set         routing_ind;            // how must msg be routed
  short                    network_apperance;
} sccp_addr_str;

typedef enum {  class0,   // connectionless transport, non-sequenced
		class1,   // connectionless transport, sequenced
		class2,   // connectionoriented
		class3    // connectionoriented with flow control
} protocol_class_set;

typedef struct {
  protocol_class_set       prot_class;           // class 0, 1 or 2
  bool                     in_sequence;          // msg delivevered in sequence
  short                    sequence_number;      // = stream number
  bool                     return_msg_on_error;  // return a UDT service on err
  short                    importance;           // importance of msg(0..7)
  short                    hopcounter;           // hopcounter
} sccp_QOS_str;

typedef struct {
  void (*ulp_ClDataIndNotif)   ( unsigned int   local_sua_Id,
				 unsigned int   primitive,
				 unsigned int   datalen
				 );
  void (*ulp_ConnIndNotif)     ( unsigned int    local_sua_id,
				 unsigned int    local_sua_ref,
				 unsigned int    datalen
				 );
  void (*ulp_ConnConfIndNotif) ( unsigned int    local_sua_id,
				 unsigned int    local_sua_ref,
				 unsigned int    datalen
				 );
  void (*ulp_ConnDataIndNotif) ( unsigned int    local_sua_id,
				 unsigned int    local_sua_ref,
				 unsigned int    datalen
				 );
  void (*ulp_DisConnIndNotif)  ( unsigned int    local_sua_id,
				 unsigned int    local_sua_ref,
				 unsigned int    reason,
				 unsigned int    datalen
				 );
} Sua_ULP_CallBacks;


void sua_initialisation();

#define SUA_CFG_NO_ERROR        0
#define SUA_CFG_FILE_UNKNOWN    1
#define SUA_CFG_NO_SOURCE_ADDR  2
#define SUA_CFG_NO_SOURCE_SSN   3
#define SUA_CFG_NO_SOURCE_PORT  4

unsigned int  sua_read_config_file
        ( char               *pConfFile
	);

#define SUA_REG_NO_ERROR        0

unsigned int sua_registerInstance
( Sua_ULP_CallBacks  ulp_callback
  );


#define SUA_ASS_NO_ERROR        0

unsigned int sua_associate ( );

typedef struct {
  sccp_addr_str          remote_addr;
  sccp_addr_str          local_addr;
  int                    blabla;
  unsigned int           ASP_status;
} sua_Path_str;

#define SUA_PATH_NO_ERROR               0
#define SUA_PATH_NO_DEST_ADDR_PRESENT   1

unsigned int sua_getPath( unsigned int assoc_id,
			  									sua_Path_str &pathinfo
												);

#define SUA_TERM_NO_ERROR               0
#define SUA_TERM_NO_DEST_ADDR_PRESENT   1


unsigned int sua_shutdown();
unsigned int sua_terminate();


#define SUA_SEND_NO_ERROR               0
#define SUA_SEND_PRIM_NOT_SENT          1

unsigned int Send_sua_primitive( unsigned int   primitive,
				 unsigned int   &sua_ConnId,
				 sccp_QOS_str   &QOS,
				 sccp_addr_str  &called_pty_address,
				 sccp_addr_str  &calling_pty_address,
				 char           *buffer,
				 unsigned int   len
				);

#define SUA_REC_NO_ERROR                0
#define SUA_REC_NO_DATA_PRESENT         1

unsigned int Receive_sua_primitive( unsigned int   &primitive,
				    unsigned int   &sua_ConnId,
				    sccp_QOS_str   &QOS,
				    sccp_addr_str  &called_pty_address,
				    sccp_addr_str  &calling_pty_address,
				    char           *buffer,
				    unsigned int   len
				   );


#define SUA_GETPC_NO_ERROR                0
#define SUA_GETPC_ERROR                   1

unsigned int sua_get_sourcepc_addr( unsigned int  sua_id,
				    pointcode_str &pc
				  );

unsigned int sua_get_destpc_addr( unsigned int  sua_id,
				  pointcode_str &pc,
				  unsigned int  &sua_assoc_id,
				  unsigned int  &status
				);

#define SUA_MNGT_NO_ERROR                0
#define SUA_MNGT_ERROR                   1

unsigned int sua_Activate_assoc( unsigned int  sua_id
			       );

unsigned int sua_DeActivate_assoc( unsigned int  sua_id
   			         );

#define SUA_DAUD_NO_ERROR                0
#define SUA_DAUD_ERROR                   1

unsigned int sua_send_daud();


#endif  // SUA_GLOBAL_H	

// end of module sua.h











