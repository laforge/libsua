/***************************************************************************
                          sua_asp_mgnt.h  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: sua_asp_mgnt.h,v 1.2 2003/09/09 08:43:25 p82609 Exp $
 *
 * SUA implementation according to SUA draft issue 6.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * Purpose: This header-file defines the SUA management handling functions for
 *          sending and receiving Application Server Process(ASP) messages:  
 *          - send a ASP-UP msg to remote node
 *          - send a ASP-ACTIVE msg to remote node
 *          - Receive/process ASP UP msg
 *          - Receive/process ASP UP ACKnowladge msg
 *          - Receive/process ASP DOWN msg
 *          - Receive/process ASP DOWN ACKnowledge msg
 *          - Receive/process Heartbeat msg
 *          - Receive/process Heartbeat ACKnowledge msg
 *          - Receive/process ASP ACTIVE msg
 *          - Receive/process ASP ACTIVE ACKnowladge msg
 *          - Receive/process ASP INactive msg
 *          - Receive/process ASP INactive ACKnowledge msg
 *          Contains also the ASP management states set
 */

#ifndef SUA_ASP_MGNT_H
#define SUA_ASP_MGNT_H

#include "sua.h"
#include "sua_syntax.h"

#include <string>



typedef enum {asp_idle,               // 0
	      asp_down,               // 1 
	      asp_down_traf_hold,     // 2
	      asp_inactive,           // 3
	      asp_inactive_traf_hold, // 4
	      asp_active              // 5
} asp_Sua_asp_state_set;


typedef enum {as_idle,               // 0
	      as_down,               // 1 
	      as_pending,            // 2
	      as_inactive,           // 3
	      as_active              // 4
} asp_Sua_as_state_set;

typedef enum { lm_override,         // 0: standard loadshare: only 1 ASP
	       lm_share_RR,         // 1: Round robin load sharing
	       lm_share_XX,         // 2: unknown load sharing
	       lm_share_BC          // 3: Broadcasting
}asp_Sua_loadmode_set;

int sua_send_ASPUP( unsigned int     Sua_assoc_id,
		    boolean          ASP_id_pres,
		    uint32_t         ASP_id
		  );

int sua_send_ASPDOWN( unsigned int     Sua_assoc_id
		    );

int sua_send_ASPAC( unsigned int            Sua_assoc_id,
		    Sua_traffic_mode_type   traff_mode,
		    boolean                 routing_context_present,
		    uint32_t                routing_context
		  );

int sua_send_ASPINAC( unsigned int            Sua_assoc_id,
		      boolean                 routing_context_present,
		      uint32_t                routing_context
		      );

int sua_send_NOTIFY( unsigned int            Sua_assoc_id,
		     boolean                 ASP_id_pres,
		     uint32_t                ASP_id,
		     boolean                 routing_context_present,
		     uint32_t                routing_context,
		     uint8_t                 status_type,
		     uint8_t                 status_id
		   );

int process_ASPUP_msg ( unsigned int     sua_assoc_id,
			int	         local_sua_id,
			int	         remote_sua_id,
			Sua_container	 sua_asp_msg
		      );

int process_ASPUP_ACK_msg ( unsigned int         sua_assoc_id,
			    int	                 local_sua_id,
			    int	                 remote_sua_id,
			    Sua_container	 sua_asp_msg
			  ); 

int process_ASPDOWN_msg ( unsigned int     sua_assoc_id,
			  int	           local_sua_id,
			  int	           remote_sua_id,
			  Sua_container	   sua_asp_msg
		        );

int process_ASPDOWN_ACK_msg ( unsigned int         sua_assoc_id,
			      int	                 local_sua_id,
			      int	                 remote_sua_id,
			      Sua_container	 sua_asp_msg
			    ); 

int process_BEAT_msg ( unsigned int      sua_assoc_id,
		       int	         local_sua_id,
		       int	         remote_sua_id,
		       Sua_container	 sua_asp_msg
		     );

int process_BEAT_ACK_msg ( unsigned int          sua_assoc_id,
			   int	                 local_sua_id,
			   int	                 remote_sua_id,
			   Sua_container	 sua_asp_msg
			 ); 

int process_ASPAC_msg ( unsigned int     sua_assoc_id,
			int	         local_sua_id,
			int	         remote_sua_id,
			Sua_container	 sua_asp_msg
		      );

int process_ASPAC_ACK_msg ( unsigned int         sua_assoc_id,
			    int	                 local_sua_id,
			    int	                 remote_sua_id,
			    Sua_container	 sua_asp_msg
			  ); 

int process_ASPINAC_msg ( unsigned int     sua_assoc_id,
			  int	         local_sua_id,
			  int	         remote_sua_id,
			  Sua_container	 sua_asp_msg
		        );

int process_ASPINAC_ACK_msg ( unsigned int         sua_assoc_id,
			      int	                 local_sua_id,
			      int	                 remote_sua_id,
			      Sua_container	 sua_asp_msg
			    ); 

void  Asp_mngt_standby ( unsigned int         sua_AS_id,
			 unsigned int         sua_asp_id,
			 short                mode
		       );

void asp_activate_override ( unsigned int asp_sua_assoc_id );

void asp_deactivate ( unsigned int asp_sua_assoc_id);




#endif  // SUA_ASP_MGNT_H

//end of module sua_asp_mgnt.h













