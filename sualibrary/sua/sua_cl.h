/***************************************************************************
                          sua_cl.h  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: sua_cl.h,v 1.2 2003/01/24 11:48:54 p82609 Exp $
 *
 * SUA implementation according to SUA draft issue 6.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * Purpose: This header-file defines the SUA connectionless message handling:
 *          - send a SUA message to SCTP
 *          - route/relay a received SUA msg towards to the correct SCTP association
 *          - send a Unitdata msg to remote node
 *          - send a Unitdata Service msg to remote node
 *          - Process a Unitdata msg
 *          - Process a Unitdata Service msg
 */

#ifndef SUA_CL_H
#define SUA_CL_H

#include "sua_database.h"

#ifdef LINUX
    #include <unistd.h>
#endif


#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <string>


using namespace std;

int sua_send_Message( signed int     sctp_assoc_id,
		      short int      sctp_stream_id,
		      int            sctp_delivery_type,
		      unsigned int   sctp_loadshare,
		      char           *databuf,
		      unsigned int   datalen
		    );

int sua_route_Message( unsigned int   &sctp_assoc_id,
		       unsigned int   local_sua_id,
		       Sua_container  &msg,
		       sccp_addr_str  &called_pty_address,
		       sccp_addr_str  &calling_pty_address
		     );

int sua_send_Unitdata( sccp_QOS_str   &QOS,
		       sccp_addr_str  &called_pty_address,
		       sccp_addr_str  &calling_pty_address,
		       char           *buffer,
		       unsigned int   len
		     );
		     						
int sua_send_UDTService( Sua_container  &org_msg,
			 sccp_addr_str  &called_pty_address,
			 sccp_addr_str  &calling_pty_address,
			 unsigned int   UDTS_reason
		       );		     						

short process_unitdata_msg ( int            local_sua_id,
			     unsigned int   sua_assoc_id,
			     Sua_container  &sua_msg
			   );

short process_UDTService_msg ( int            local_sua_id,
			       unsigned int   sua_assoc_id,
			       Sua_container  &sua_msg
			     );


#endif  // SUA_CL_H

// end of module sua_cl.h




