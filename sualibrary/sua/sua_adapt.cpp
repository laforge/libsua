/***************************************************************************
                          sua_adapt.cpp  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: sua_adapt.cpp,v 1.7 2003/09/09 08:43:25 p82609 Exp $
 *
 * SUA implementation according to SUA draft issue 13.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * The alternative comment
 * inspiration : Katrien
 * "Come on barbie, let's go party"
 *
 * Purpose: This code-file defines the SUA adaptation functions used by  
 *          the SUA application for:
 *          - initialising the SUA layer
 *          - reading the SUA configuration file
 *          - registering a local SUA instance
 *          - Setting up the association with a remote SUA node
 *          - terminate a association
 *          - get SUA path info on a certain (SUA) association
 *          - get SUA destination pointcode (IP and SS7) and status
 *          - get SUA origination pointcode (IP and SS7)
 *          - deactivate a SUA association
 */

// sua_adapt.c++
// SCCP user adaptation program

#include "sua_debug.h"
#include "sua_database.h"
#include "sua_file.h"
#include "sua_distribution.h"
#include "sua_logging.h"

#ifdef LINUX
    #include <unistd.h>
#endif

#include <sctp.h>

#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <string>

#include <netinet/in_systm.h>
#include <netinet/ip.h>         /* includes <netinet/in.h> also ! */
#include <netinet/ip_icmp.h>
#include <netdb.h>
#include <arpa/inet.h>

using namespace std;

/* definition of SUA database objects */
db_Sua_DatabaseList sua;

/***********************************************************************/
/* sua_initialisation                                                  */
/***********************************************************************/
void sua_initialisation()
{
  int                      res;
  SCTP_LibraryParameters   params;

  cout << "Initialising SCTP & SUA\n ";
  res = sctp_initLibrary();

  /* set checksum to CRC32C */
  sctp_getLibraryParameters(&params);
  params.checksumAlgorithm = SCTP_CHECKSUM_ALGORITHM_CRC32C;
  params.supportPRSCTP     = 0;
  sctp_setLibraryParameters(&params);

  init_logging_file();  
  sua.initialize();
  cout << "Finished initialising SUA data\n ";
 
} /* end of sua_initialisation */

/***********************************************************************/
/* sua_read_configuration file                                         */
/***********************************************************************/
unsigned int  sua_read_config_file( char *pConfFile)
{
  unsigned int result = SUA_CFG_NO_ERROR;
  string sua_filename;
  sua_filename = pConfFile;
 
  result = read_sua_conf_file( sua_filename,
			       sua
			       );
  
  return(result);
}

/***********************************************************************/
/* sua_registerInstance                                                */
/***********************************************************************/
unsigned int sua_registerInstance ( Sua_ULP_CallBacks  ulp_callback
				  )
{
  unsigned int result = SUA_REG_NO_ERROR;
  SCTP_ulp_Callbacks SCTPCallbackFunctions;

  SCTPCallbackFunctions.dataArriveNotif = &sctp_DataArriveNotif;
  SCTPCallbackFunctions.networkStatusChangeNotif = &sctp_NetworkStatusChangeNotif;
  SCTPCallbackFunctions.sendFailureNotif = &sctp_SentFailureNotif;
  SCTPCallbackFunctions.communicationLostNotif = &sctp_CommunicationLostNotif;
  SCTPCallbackFunctions.communicationUpNotif = &sctp_CommunicationUpNotif;
  SCTPCallbackFunctions.communicationErrorNotif = &sctp_CommunicationErrorNotif;
  SCTPCallbackFunctions.restartNotif = &sctp_RestartNotif;
  SCTPCallbackFunctions.shutdownCompleteNotif = &sctp_ShutDownCompleteNotif;
  SCTPCallbackFunctions.peerShutdownReceivedNotif = &sctp_ShutDownCompleteNotif;
  SCTPCallbackFunctions.queueStatusChangeNotif = &sctp_queueStatusChangeNotif; 
  SCTPCallbackFunctions.asconfStatusNotif = &sctp_asconfStatusNotif;

  result = sua.local_sua.register_instance ( SCTPCallbackFunctions,
					     ulp_callback
					   );
  
  return(result);
}


/***********************************************************************/
/* sua_associate                                                       */
/***********************************************************************/
unsigned int sua_associate ( )
{
  unsigned int result = SUA_ASS_NO_ERROR;
  result = sua.AssocDB.associate_instance( sua.local_sua,
					   sua.remote_sua
					 );
  return(result);

}


/***********************************************************************/
/* sua_getPath                                                         */
/***********************************************************************/
unsigned int sua_getPath( unsigned int assoc_id,
			  sua_Path_str &pathinfo
			)
{
  unsigned int result = SUA_PATH_NO_ERROR;
  
  if ( sua.AssocDB.instance[assoc_id].Dest.nr_of_addrs > 0)
    {
      /*pathinfo.remote_addr.*/
      pathinfo.remote_addr.pc.ipvx = sua.AssocDB.instance[assoc_id].Dest.addrs[0];
      
      /*memcpy( pathinfo.remote_addr.pc.ipvx.ch, sua.AssocDB.instance[assoc_id].Dest.addrs[0].ch, 24);*/
      
      pathinfo.remote_addr.pc.ss7 = sua.AssocDB.instance[assoc_id].Dest.pc;
      pathinfo.remote_addr.ssn = sua.remote_sua.instance[sua.AssocDB.instance[assoc_id].remote_sua_id].ssn.ssn;
      
      pathinfo.ASP_status = sua.AssocDB.instance[assoc_id].asp.status;
     
    }
  else
    result = SUA_PATH_NO_DEST_ADDR_PRESENT;
  
  pathinfo.local_addr.pc.ipvx = sua.local_sua.instance[sua.AssocDB.instance[assoc_id].local_sua_id].Source.addrs[0];
  
  /*memcpy( pathinfo.local_addr.pc.ipvx.ch, sua.AssocDB.instance[assoc_id].Source.addrs[0].ch,24);*/
  
  pathinfo.local_addr.pc.ss7 = sua.local_sua.instance[sua.AssocDB.instance[assoc_id].local_sua_id].Source.pc;
  pathinfo.local_addr.ssn = sua.local_sua.instance[sua.AssocDB.instance[assoc_id].local_sua_id].ssn.ssn;
  
  pathinfo.local_addr.name.GT = sua.local_sua.instance[sua.AssocDB.instance[assoc_id].local_sua_id].Name.GT;

  return (result);
  
}

/***********************************************************************/
/* get destination pointcode address                                   */
/***********************************************************************/
unsigned int sua_get_destpc_addr( unsigned int  sua_id,
				  pointcode_str &pc,
				  unsigned int  &sua_assoc_id,
				  unsigned int  &status
				)
{
  if (( sua_id > 0) && (sua_id <= ((unsigned int)sua.remote_sua.num_of_instance)))
    {
      pc.ipvx = sua.remote_sua.instance[sua_id].Dest.addrs[0];
      pc.ss7 = sua.remote_sua.instance[sua_id].Dest.pc;
      sua_assoc_id = sua.remote_sua.instance[sua_id].SUA_assoc_id;
      status = (unsigned int ) sua.remote_sua.instance[sua_id].status;
      return(SUA_GETPC_NO_ERROR);
    }
  else
    return(SUA_GETPC_ERROR);
}

/***********************************************************************/
/* get source pointcode address                                        */
/***********************************************************************/
unsigned int sua_get_sourcepc_addr( unsigned int  sua_id,
				    pointcode_str &pc
				  )
{
 if (( sua_id > 0) && (sua_id <= ((unsigned int) sua.local_sua.num_of_instance)))
   {
     pc.ipvx = sua.local_sua.instance[sua_id].Source.addrs[0];
     pc.ss7 = sua.local_sua.instance[sua_id].Source.pc;
     return(SUA_GETPC_NO_ERROR);
   }
 else
   return(SUA_GETPC_ERROR);
}

/***********************************************************************/
/* sua_shutdown                                                       */
/***********************************************************************/
unsigned int sua_shutdown( )
{
  unsigned int result = SUA_TERM_NO_ERROR;

  sua.AssocDB.shutdown();

  return (result);

}


/***********************************************************************/
/* sua_terminate                                                       */
/***********************************************************************/
unsigned int sua_terminate( )
{
  unsigned int result = SUA_TERM_NO_ERROR;

  sua.AssocDB.shutdown();
  close_logging_file();

  return (result);

}

/***********************************************************************/
/* sua_Activate_assoc                                                */
/***********************************************************************/
unsigned int sua_Activate_assoc( unsigned int sua_assoc_id
			       )
{
  unsigned int result = SUA_MNGT_NO_ERROR;
  
  switch (sua.AssocDB.instance[sua_assoc_id].asp.status)
    {
    case(asp_inactive):
    case(asp_inactive_traf_hold):
      { 
	result = sua_send_ASPAC( sua_assoc_id,
				 tmt_unknown,
				 true,
				 1
			       );
	break;
      }
    case( asp_active):
      {
	/* do nothing, association is already active */
	break;
      }
    default:
      {
	result = sua_send_ASPUP( sua_assoc_id ,
				 false,
				 1
			       );
	break;
      }
    }
  return (result);
  
}

/***********************************************************************/
/* sua_DeActivate_assoc                                                */
/***********************************************************************/
unsigned int sua_DeActivate_assoc( unsigned int sua_assoc_id
				 )
{
  unsigned int result = SUA_MNGT_NO_ERROR;
  
  switch (sua.AssocDB.instance[sua_assoc_id].asp.status)
    {
    case(asp_inactive):
    case(asp_inactive_traf_hold):
      { 
	result = sua_send_ASPDOWN( sua_assoc_id );

	break;
      }
    case( asp_active):
      {
	result = sua_send_ASPINAC( sua_assoc_id,
				   true,
				   1
				   );
	break;
      }
    default:
      {
        /* do nothing, association is already inactive(idle or down)*/
	break;
      }
    } 

  return (result);
  
}



// end of module sua_adapt.c++
