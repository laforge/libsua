/***************************************************************************
                          sua_snm_mgnt.cpp  -  description
                             -------------------
    begin                : Tue Nov 4 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/
/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: sua_snm_mgnt.cpp,v 1.6 2003/08/28 09:30:20 p82609 Exp $
 *
 * SUA implementation according to SUA draft issue 14.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * The alternative comment
 * inspiration : Sabine
 * "Due to small parts(Booleans), this code is not suitable for"
 * "childeren under 3 years."
 *
 * Purpose: This code-file defines the SUA management handling functions for
 *          sending and receiving SignallingNetwork Management(SNM) messages:  
 *          - send a Destination avialeble (DAVA) msg to remote node
 *          - Receive/process Destination Unavailable(DUNA) msg
 *          - Receive/process Destination Available(DAVA) msg
 *          - Receive/process Destination State audit(DAUD) msg
 *          - Receive/process Network congestion(SCON) msg
 *          - Receive/process Destination User Part Unavailable(DUPU) msg
 *          - Receive/process Destination Restricted(DRST) msg
 */

#include "sua_debug.h"
#include "sua_snm_mgnt.h"
#include "sua_asp_mgnt.h"
#include "sua_database.h"
#include "sua_syntax.h"
#include "sua_logging.h"
#include "sua_tcb.h"

#include <cstdio>
#include <iostream>
#include <string>

using namespace std;

extern db_Sua_DatabaseList sua;

extern tcb_Sua_msgqueue_pool  msg_store;

/***********************************************************************/
/* Sending SUA Signalling Network Management msg(SNM)                  */
/***********************************************************************/
/***********************************************************************/
/* sua_send_DAVA                                                       */
/***********************************************************************/
int sua_send_DAVA( unsigned int     Sua_assoc_id,
		   SS7union         pc
		  )
{
  Sua_container msg;
  Sua_syntax_error_struct error;
  int error_value = 0;
  int string_size, datalen;
  signed int sctp_assoc_id;
  short stream_id = 0;
  int delivery_type, result;

  // init the message
  msg.sua_init();

  // fill in the main sua header
  msg.sua_prim.hdr_msg_class = sua_ssnm;
  msg.sua_prim.hdr_msg_type.ssnm = ssnm_dava;
  msg.sua_prim.ssnm_parm.aff_pc_pres = TRUE;
  msg.sua_prim.ssnm_parm.aff_pc.num_aff_pc = 1;
  msg.sua_prim.ssnm_parm.aff_pc.pc[0] = pc.ITU24.pc;
  msg.sua_prim.info_pres = FALSE;
  
  // encode the SUA DAVA message
  error = msg.sua_encode();
  string_size = msg.sua_msg.size();
  
  delivery_type = SCTP_UNORDERED_DELIVERY;

  sctp_assoc_id =  sua.AssocDB.instance[Sua_assoc_id].SCTP_assoc_id;

  /* does association exist? */
  if (sctp_assoc_id > 0)
    {
      // send data to SCTP
      char* databuf = new char[msg.sua_msg.length()];
      msg.sua_msg.copy(databuf, msg.sua_msg.length());
      datalen = msg.sua_msg.length();

      /* yes it does, continue, no problem, send the msg */
#ifdef DEBUG
      // display byte array
      display_byte_array(databuf , msg.sua_msg.length());
#endif
      char logstring[100];
      sprintf(logstring, "SUA encoded message, ready for being send to SCTP assoc %d", sctp_assoc_id);
      event_log("sua_snm_mgnt.c",logstring);
      log_byte_array("sua_snm_mgnt.c", databuf,msg.sua_msg.length());
      
      result = sctp_send ( sctp_assoc_id,
			   stream_id,
			   (unsigned char *) databuf,
			   datalen,
			   htonl(SUA_PPI),
			   SCTP_USE_PRIMARY,
			   SCTP_NO_CONTEXT,
			   SCTP_INFINITE_LIFETIME,
			   delivery_type,
			   SCTP_BUNDLING_DISABLED
			   );

      error_value = result;
      delete databuf;
#ifdef DEBUG
      cout << "sua_asp_mgnt.c:result DAVA sctp send = "<< result << "\n";
#endif
    }
  
  return(error_value);
}


/***********************************************************************/
/* sua_send_DUNA                                                       */
/***********************************************************************/
int sua_send_DUNA( unsigned int     Sua_assoc_id,
		   SS7union         pc   
		  )
{
  Sua_container msg;
  Sua_syntax_error_struct error;
  int error_value = 0;
  int string_size, datalen;
  signed int sctp_assoc_id;
  short stream_id = 0;
  int delivery_type, result;

  // init the message
  msg.sua_init();

  // fill in the main sua header
  msg.sua_prim.hdr_msg_class = sua_ssnm;
  msg.sua_prim.hdr_msg_type.ssnm = ssnm_duna;
  msg.sua_prim.ssnm_parm.aff_pc_pres = TRUE;
  msg.sua_prim.ssnm_parm.aff_pc.num_aff_pc = 1;
  msg.sua_prim.ssnm_parm.aff_pc.pc[0] = pc.ITU24.pc;
  msg.sua_prim.info_pres = FALSE;
  
  // encode the SUA DAVA message
  error = msg.sua_encode();
  string_size = msg.sua_msg.size();
  
  delivery_type = SCTP_UNORDERED_DELIVERY;

  sctp_assoc_id =  sua.AssocDB.instance[Sua_assoc_id].SCTP_assoc_id;

  /* does association exist? */
  if (sctp_assoc_id > 0)
    {
      // send data to SCTP
      char* databuf = new char[msg.sua_msg.length()];
      msg.sua_msg.copy(databuf, msg.sua_msg.length());
      datalen = msg.sua_msg.length();

      /* yes it does, continue, no problem, send the msg */
#ifdef DEBUG
      // display byte array
      display_byte_array(databuf , msg.sua_msg.length());
#endif
      char logstring[100];
      sprintf(logstring, "SUA encoded message, ready for being send to SCTP assoc %d", sctp_assoc_id);
      event_log("sua_snm_mgnt.c",logstring);
      log_byte_array("sua_snm_mgnt.c", databuf,msg.sua_msg.length());
      
      result = sctp_send ( sctp_assoc_id,
			   stream_id,
			   (unsigned char *) databuf,
			   datalen,
			   htonl(SUA_PPI),
			   SCTP_USE_PRIMARY,
			   SCTP_NO_CONTEXT,
			   SCTP_INFINITE_LIFETIME,
			   delivery_type,
			   SCTP_BUNDLING_DISABLED
			   );

      error_value = result;
      delete databuf;
#ifdef DEBUG
      cout << "sua_asp_mgnt.c:result DUNA sctp send = "<< result << "\n";
#endif
    }
  
  return(error_value);
}


/***********************************************************************/
/* sua_send_DAUD                                                       */
/***********************************************************************/
unsigned int sua_send_daud()
{
  Sua_container msg;
  Sua_syntax_error_struct error;
  int i, error_value = 0;
  int string_size, datalen;
  signed int sctp_assoc_id, Sua_assoc_id;
  short stream_id = 0;
  int delivery_type, result;
  SS7union  pc;


  i = 1;

  while ( i <= sua.remote_sua.num_of_instance)
    {
      /* get pointcode from sua remote list */
      pc = sua.remote_sua.instance[i].Dest.pc;
      // init the message
      msg.sua_init();
      
      // fill in the main sua header
      msg.sua_prim.hdr_msg_class = sua_ssnm;
      msg.sua_prim.hdr_msg_type.ssnm = ssnm_daud;
      msg.sua_prim.ssnm_parm.aff_pc_pres = TRUE;
      msg.sua_prim.ssnm_parm.aff_pc.num_aff_pc = 1;
      msg.sua_prim.ssnm_parm.aff_pc.pc[0] = pc.ITU24.pc;
      msg.sua_prim.info_pres = FALSE;
      
      // encode the SUA DAUD message
      error = msg.sua_encode();
      string_size = msg.sua_msg.size();
      
      delivery_type = SCTP_UNORDERED_DELIVERY;

      Sua_assoc_id = sua.remote_sua.instance[i].SUA_assoc_id;
      sctp_assoc_id =  sua.AssocDB.instance[Sua_assoc_id].SCTP_assoc_id;
      
      /* does association exist? */
      if (sctp_assoc_id > 0)
	{
	  // send data to SCTP
	  char* databuf = new char[msg.sua_msg.length()];
	  msg.sua_msg.copy(databuf, msg.sua_msg.length());
	  datalen = msg.sua_msg.length();
	  
	  /* yes it does, continue, no problem, send the msg */
#ifdef DEBUG
	  // display byte array
	  display_byte_array(databuf , msg.sua_msg.length());
#endif
	  char logstring[100];
	  sprintf(logstring, "SUA encoded message, ready for being send to SCTP assoc %d", sctp_assoc_id);
	  event_log("sua_snm_mgnt.c",logstring);
	  log_byte_array("sua_snm_mgnt.c", databuf,msg.sua_msg.length());
	  
	  result = sctp_send ( sctp_assoc_id,
			       stream_id,
			       (unsigned char *) databuf,
			       datalen,
			       htonl(SUA_PPI),
			       SCTP_USE_PRIMARY,
			       SCTP_NO_CONTEXT,
			       SCTP_INFINITE_LIFETIME,
			       delivery_type,
			       SCTP_BUNDLING_DISABLED
			       );
	  
	  error_value = error_value + result;
	  delete databuf;
#ifdef DEBUG
	  cout << "sua_asp_mgnt.c:result DAUD sctp send = "<< result << "\n";
#endif
	}
      /* go to next remote destination (if possible) */
      i++;

    }
  
  return(error_value);
}



/***********************************************************************/
/* Receiving SUA Signalling Network Management msg                     */
/***********************************************************************/
/***********************************************************************/
/* sua_process_DUNA_msg                                               */
/***********************************************************************/
int process_DUNA_msg ( unsigned int      sua_assoc_id,
		       int	         local_sua_id,
		       int	         remote_sua_id,
		       Sua_container	 sua_ssnm_msg
		     )
{
  
  int error_value = 0;
  SS7union  rec_pc;
  int       num_rec_pc;

  rec_pc.ITU14.family = ITU14bit;
  num_rec_pc = sua_ssnm_msg.sua_prim.ssnm_parm.aff_pc.num_aff_pc;

  for ( int k = 0; k < num_rec_pc; k++)
    {
      rec_pc.ITU14.pc =  sua_ssnm_msg.sua_prim.ssnm_parm.aff_pc.pc[k];
      sua.remote_sua.Dest_Unavailable(rec_pc);
    }

#ifdef DEBUG
  cout << "sua_snm_mgnt.c:DUNA received, mark internal : no traffic to this destination.\n";
#endif  

 
   
  
  return(error_value);
}

/***********************************************************************/
/* sua_process_DAVA_msg                                                */
/***********************************************************************/
int process_DAVA_msg ( unsigned int      sua_assoc_id,
		       int	         local_sua_id,
		       int	         remote_sua_id,
		       Sua_container	 sua_ssnm_msg
		     )
{
  int error_value = 0;
  SS7union  rec_pc;
  int       num_rec_pc;
  
  rec_pc.ITU14.family = ITU14bit;
  num_rec_pc = sua_ssnm_msg.sua_prim.ssnm_parm.aff_pc.num_aff_pc;
  
  for ( int k = 0; k < num_rec_pc; k++)
    {
      rec_pc.ITU14.pc =  sua_ssnm_msg.sua_prim.ssnm_parm.aff_pc.pc[k];
      sua.remote_sua.Dest_Available(rec_pc);
    }

#ifdef DEBUG
  cout << "sua_snm_mgnt.c:DAVA received, mark internal: traffic can be send to this destination.\n";
#endif  
  
  return(error_value);
}



/***********************************************************************/
/* sua_process_DAUD_msg                                                */
/***********************************************************************/
int process_DAUD_msg ( unsigned int      sua_assoc_id,
		       int	         local_sua_id,
		       int	         remote_sua_id,
		       Sua_container	 sua_ssnm_msg
		     )
{
  
  int       error_value = 0;
  SS7union  rec_pc;
  int       num_rec_pc;

#ifdef DEBUG
  cout << "sua_snm_mgnt.c:DAUD received, send back a DAVA, DUNA or DRST.\n";
#endif  

  rec_pc.ITU14.family = ITU14bit;
  num_rec_pc = sua_ssnm_msg.sua_prim.ssnm_parm.aff_pc.num_aff_pc;

  for ( int k = 0; k < num_rec_pc; k++)
    {
      rec_pc.ITU14.pc =  sua_ssnm_msg.sua_prim.ssnm_parm.aff_pc.pc[k];
   
      /* find the local sua with the specific received PC */
      if ( !sua.local_sua.found(rec_pc))
	{
	  /* if no local found, go through the remote sua's for */
	  /* the specific received PC */
	  
	  if (ssnm_available != sua.remote_sua.get_Dest_status(rec_pc))
	    {
	      /* if not found, then pc is not reachable. */
	      /* let it be known to the requesting side */  
	      error_value = sua_send_DUNA( sua_assoc_id ,
					   rec_pc 
					   );
	      
	    }
	  else
	    {
	      /* found the pc and it is accessible/reachable */
	      /* let it be known to the requesting side */
	      error_value = sua_send_DAVA( sua_assoc_id ,
					   rec_pc 
					   );
	    }
	}
      else
	{
	  /* found the pc and it is accessible/reachable */
	  /* let it be known to the requesting side */
	  error_value = sua_send_DAVA( sua_assoc_id ,
				       rec_pc 
				       );
	}
      
      
#ifdef DEBUG
      cout << "sua_asp_mgnt.c:result send DUNA/DAVA/DRST = "<< error_value << "\n";
#endif
    }

#ifdef DEBUG
  cout << "sua_asp_mgnt.c:DAUD completely processed: " << num_rec_pc  << " pointcodes\n";
#endif

  return(error_value);
}

/***********************************************************************/
/* sua_process_SCON_msg                                                */
/***********************************************************************/
int process_SCON_msg ( unsigned int      sua_assoc_id,
		       int	         local_sua_id,
		       int	         remote_sua_id,
		       Sua_container	 sua_asp_msg
		     )
{
  
  int error_value = 0;

#ifdef DEBUG
  cout << "sua_snm_mgnt.c:SCON received, ignore.\n";
#endif  

  
  return(error_value);
}

/***********************************************************************/
/* sua_process_DUPU_msg                                                */
/***********************************************************************/
int process_DUPU_msg ( unsigned int      sua_assoc_id,
		       int	         local_sua_id,
		       int	         remote_sua_id,
		       Sua_container	 sua_asp_msg
		     )
{
  
  int error_value = 0;

#ifdef DEBUG
  cout << "sua_snm_mgnt.c:DUPU received.\n";
#endif  
  
  return(error_value);
}

/***********************************************************************/
/* sua_process_DRST_msg                                                */
/***********************************************************************/
int process_DRST_msg ( unsigned int      sua_assoc_id,
		       int	         local_sua_id,
		       int	         remote_sua_id,
		       Sua_container	 sua_asp_msg
		     )
{
  
  int error_value = 0;

#ifdef DEBUG
  cout << "sua_snm_mgnt.c:DRST received.\n";
#endif  
  
  return(error_value);
}


//end of module sua_asp_mgnt.c++
















