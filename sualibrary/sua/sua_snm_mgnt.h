/***************************************************************************
                          sua_snm_mgnt.h  -  description
                             -------------------
    begin                : Tue Nov 5 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: sua_snm_mgnt.h,v 1.3 2003/08/28 09:30:20 p82609 Exp $
 *
 * SUA implementation according to SUA draft issue 14.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * Purpose: This header-file defines the SUA management handling functions for
 *          sending and receiving Application Server Process(ASP) messages:  
 */

#ifndef SUA_SNM_MGNT_H
#define SUA_SNM_MGNT_H

#include "sua.h"
#include "sua_syntax.h"

#include <string>

typedef enum { ssnm_unavailable,
	       ssnm_available,
	       ssnm_reserved
} snm_Sua_pc_state_set ;

int sua_send_DAVA( unsigned int     Sua_assoc_id,
		   SS7union         pc
		  );

int sua_send_DUNA( unsigned int     Sua_assoc_id,
		   SS7union         pc
		  );

int process_DUNA_msg ( unsigned int      sua_assoc_id,
		       int	         local_sua_id,
		       int	         remote_sua_id,
		       Sua_container	 sua_asp_msg
		       );

int process_DAVA_msg ( unsigned int      sua_assoc_id,
		       int	         local_sua_id,
		       int	         remote_sua_id,
		       Sua_container	 sua_asp_msg
		       );

int process_DAUD_msg ( unsigned int      sua_assoc_id,
		       int	         local_sua_id,
		       int	         remote_sua_id,
		       Sua_container	 sua_asp_msg
		       );

int process_SCON_msg ( unsigned int      sua_assoc_id,
		       int	         local_sua_id,
		       int	         remote_sua_id,
		       Sua_container	 sua_asp_msg
		       );

int process_DUPU_msg ( unsigned int      sua_assoc_id,
		       int	         local_sua_id,
		       int	         remote_sua_id,
		       Sua_container	 sua_asp_msg
		       );

int process_DRST_msg ( unsigned int      sua_assoc_id,
		       int	         local_sua_id,
		       int	         remote_sua_id,
		       Sua_container	 sua_asp_msg
		       );

#endif  // SUA_SNM_MGNT_H

//end of module sua_snm_mgnt.h













