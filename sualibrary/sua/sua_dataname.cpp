/***************************************************************************
                          sua_dataname.cpp  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: sua_dataname.cpp,v 1.4 2003/09/09 08:43:44 p82609 Exp $
 *
 * SUA implementation according to SUA draft issue 6.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * The alternative comment
 * inspiration : Vicky
 * "This module reuse some code, thus it will mostly reuse trouble."
 *
 * Purpose: This code-file defines the SUA database access functions for:
 *          SUA Name Object (host names/Global Titles):
 *          - initialise Name
 *          SUA Name List:
 *          - initialise Name List
 *          - read hostname
 *					- read global title
 *					- resolve hostname
 *					- perform Global Title Translation
 */

#include "sctp.h"

#include "sua_debug.h"
#include "sua_database.h"
#include "sua_asp_mgnt.h"
#include "sua_logging.h"
#include "sua_adapt.h"

#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <string>

#include "unistd.h"



using namespace std;

/***********************************************************************/
/* functions of the object class SUA name Object                       */
/***********************************************************************/

/***********************************************************************/
/* Sua_NameObject::initalize                                           */
/***********************************************************************/
void db_Sua_NameObject::initialize(){

  // initialise to point to a invalid SUA association
  SUA_assoc_id = 0;

  GT.Translation_Type = 0;
  GT.Numbering_Plan = 0;
  GT.Nature_of_Address = 0;
  /* initialise to empty string */
  GT.digits="";
  hostname = "";

}



/***********************************************************************/
/* functions of the object class SUA NameList                          */
/***********************************************************************/

/***********************************************************************/
/* Sua_NameList::initalize                                             */
/***********************************************************************/
void db_Sua_NameList::initialize(){
  short i;
  num_of_instance = 0;
  for (i=0; i < db_MAX_REMOTE_SUA; i++) {
    instance[i].initialize();
  }
}

/***********************************************************************/
/* Sua_NameList::read_host_name                                        */
/***********************************************************************/
void db_Sua_NameList::read_host_name( unsigned int gtname_id,
				      string name
				    ){

  char            *hostname;
  const char      *ip_addr_ptr;
  struct hostent  *hptr;
  char            str[INET6_ADDRSTRLEN];
  char            **pptr;
  string          addr_str;

  hostname = new char[name.length()+1];
  name.copy(hostname,name.length());
  // we are dealing with char arrays, so....
  hostname[name.length()] = '\0';
	
  if ((hptr = gethostbyname(hostname)) == NULL){
    cout << "Determination of hostname failed\n";
  }     
  else
    {
#ifdef DEBUG
      cout << "Hostname " << hptr->h_name << " has the following IP address(es)\n";
#endif	
      
      pptr = hptr->h_addr_list;
      for ( ; *pptr != NULL;pptr++)
	{
	  ip_addr_ptr = inet_ntop(hptr->h_addrtype, *pptr, str,sizeof(str));
	  addr_str = addr_str + ip_addr_ptr;
	  if ((*(pptr+1)) != NULL)
	    /* another ip address is comming after the present one */
	    addr_str = addr_str + ",";
	  /*else this is the last ip address */
#ifdef DEBUG
	  cout << ip_addr_ptr << "\n";
#endif	
	}	
      
#ifdef DEBUG
      cout << "output IP list = " << addr_str << "\n";
#endif
      
    }

  /* initialise */
  instance[gtname_id].SUA_assoc_id = 0;
  
  /* no GT present -> hostname present */
  instance[gtname_id].GT_present = false;
  instance[gtname_id].hostname = name;
  
  return;
  
}

/***********************************************************************/
/* Sua_NameList::read_Global_Title                                     */
/***********************************************************************/
void db_Sua_NameList::read_Global_Title( unsigned int gtname_id,
					 unsigned int tt,
					 unsigned int na,
					 unsigned int np,
					 string       digits
				       )
{
  /*unsigned short i;*/

  instance[gtname_id].SUA_assoc_id = 0;
  
  /* GT present */
  instance[gtname_id].GT_present = true;
  instance[gtname_id].GT.Translation_Type = tt;
  instance[gtname_id].GT.Numbering_Plan = np;
  instance[gtname_id].GT.Nature_of_Address = na;
  instance[gtname_id].GT.digits.resize( digits.length());
  instance[gtname_id].GT.digits= "";
  instance[gtname_id].GT.digits = digits;
  
}

/***********************************************************************/
/* db_Sua_NameList::resolve_host_name                                  */
/***********************************************************************/
signed int db_Sua_NameList::resolve_host_name ( hostname_str&  dest_name,
						pointcode_str& dest_pc
					      )
{
  struct hostent   *hptr;
  char            **pptr;
  char              str[INET6_ADDRSTRLEN];
  char             *dest_carr;
  
  /* resolving can be done via: */
  /* - local global Titel database */
  /* - resolve hostname via DNS(simplest for single hop translations) */
  dest_carr = dest_name;
  
  if ((hptr = gethostbyname( dest_carr )) == NULL)
    {
      cout << "Hostname " << dest_name << " not known in DNS.\n";
      return(-1);
    }
  
#ifdef DEBUG
  cout << "Hostname " << dest_name << " resolved to dest IP address(es)\n";
  cout << "IP address length = "<< hptr->h_length << "\n";
#endif
  
  /* initialise the length field of the structure: */
  /* length field is NOT always present in every Unix like operating system  */
  dest_pc.ipvx.ch[0] = 0;	
  
  pptr = hptr->h_addr_list;
  for ( ; (*pptr != NULL) ;pptr++)
    {
      inet_ntop(hptr->h_addrtype, *pptr, str,sizeof(str));
#ifdef DEBUG
      cout << str << "\n";
#endif
      /* got a IP address */
      dest_pc.ipvx.sa.sa_family = hptr->h_addrtype;
      if (dest_pc.ipvx.sa.sa_family == AF_INET)
	inet_pton( AF_INET,
		   str,
		   &dest_pc.ipvx.sin.sin_addr
		 );
      else if (dest_pc.ipvx.sa.sa_family == AF_INET6)
	inet_pton( AF_INET6,
		   str,
		   &dest_pc.ipvx.sin6.sin6_addr
		 );
      else
	{
	  cout << "ERROR resolve_host_name: Unknown IP addresstype\n";
	  return(-2);
	}
    }
  return(0);
};

/***********************************************************************/
/* db_Sua_NameList::resolve_GT                                         */
/***********************************************************************/
signed int db_Sua_NameList::perform_GTT ( global_title_str&  cld_in,
					  global_title_str&  clg_in,
					  global_title_str&  cld_out,
					  global_title_str&  clg_out,
					  pointcode_str&     dest_pc
					)
{
  boolean       search_gt = true, gt_entry_found = false;
  int           i, found_gt_entry,res;
  unsigned int  sua_assoc_id, status;
  
  /* resolving can be done via: */
  /* - local global Titel database */
  i= 1;
  while (search_gt){
    gt_entry_found = ((instance[i].GT.Translation_Type == cld_in.Translation_Type) &&
      (instance[i].GT.Numbering_Plan == cld_in.Numbering_Plan) && 
      (instance[i].GT.Nature_of_Address == cld_in.Nature_of_Address) &&
      (instance[i].GT.digits == cld_in.digits));
  
    found_gt_entry = i;
    
    search_gt = ((!gt_entry_found) && ( i < num_of_instance)); 

    /* next entry of name table */
    i++;
  }

  cout << "found_gt_entry = " << found_gt_entry << "\n";
  cout << "tt = " << cld_in.Translation_Type << "\n";
  cout << "NP = " << cld_in.Numbering_Plan << "\n";
  cout << "NA = " << cld_in.Nature_of_Address << "\n";
  cout << "CLD number of digits = " << cld_in.nr_of_digits << "\n";
  cout << "CLD digits = " << cld_in.digits[0];
  cout <<  cld_in.digits[1];
  cout <<  cld_in.digits[2];
  cout <<  cld_in.digits[3];
  cout <<  cld_in.digits[4];
  cout <<  cld_in.digits[5];
  cout <<  cld_in.digits[6];
  cout <<  cld_in.digits[7];
  cout <<  cld_in.digits[8];
  cout <<  cld_in.digits[9];
  cout <<  cld_in.digits[10];
  cout <<  cld_in.digits[11];
  cout << "\n";
  cout << "CLG number of digits = " << clg_in.nr_of_digits << "\n";
  cout << "CLG digits = " << clg_in.digits << "\n";



  if (gt_entry_found) {
    /* get the association number from gt database */
    res = sua_get_destpc_addr(instance[found_gt_entry].SUA_assoc_id,
			      dest_pc,
			      sua_assoc_id,
			      status
			     ); 
    return(0);
  }
  else
    return(-1);
};


/***********************************************************************/
/* db_Sua_NameList::increase_instance                                  */
/***********************************************************************/
void  db_Sua_NameList:: increase_instance(){

 num_of_instance++;

}


// end of module sua_dataname.c




















