/***************************************************************************
                          sua_asp_mgnt.cpp  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: sua_asp_mgnt.cpp,v 1.2 2003/09/09 08:43:25 p82609 Exp $
 *
 * SUA implementation according to SUA draft issue 6.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * The alternative comment
 * inspiration : Sabine
 * "Due to small parts(Booleans), this code is not suitable for"
 * "childeren under 3 years."
 *
 * Purpose: This code-file defines the SUA management handling functions for
 *          sending and receiving Application Server Process(ASP) messages:  
 *          - send a ASP-UP msg to remote node
 *          - send a ASP-UP ACKnowledge msg to remote node
 *          - send a ASP-DOWN ACKnowledge msg to remote node
 *          - send a Hearbeat msg to remote node
 *          - send a Heartbeat ACKnowledge msg to remote node
 *          - send a ASP-ACTIVE msg to remote node
 *          - send a ASP-ACTIVE ACKnowledge msg to remote node
 *          - send a ASP-INactive Acknowledge msg to remote node
 *          - send a NOTIFY msg to remote node
 *          - Receive/process ASP UP msg
 *          - Receive/process ASP UP ACKnowladge msg
 *          - Receive/process ASP DOWN msg
 *          - Receive/process ASP DOWN ACKnowledge msg
 *          - Receive/process Heartbeat msg
 *          - Receive/process Heartbeat ACKnowledge msg
 *          - Receive/process ASP ACTIVE msg
 *          - Receive/process ASP ACTIVE ACKnowladge msg
 *          - Receive/process ASP INactive msg
 *          - Receive/process ASP INactive ACKnowledge msg
 */

#include "sua_debug.h"
#include "sua_asp_mgnt.h"
#include "sua_database.h"
#include "sua_syntax.h"
#include "sua_logging.h"
#include "sua_tcb.h"

#include <cstdio>
#include <iostream>
#include <string>

using namespace std;

extern db_Sua_DatabaseList sua;

extern tcb_Sua_msgqueue_pool  msg_store;

/***********************************************************************/
/* Sending SUA Management msg                                          */
/***********************************************************************/
/***********************************************************************/
/* sua_send_ASPUP                                                      */
/***********************************************************************/
int sua_send_ASPUP( unsigned int     Sua_assoc_id,
		    boolean          ASP_id_pres,
		    uint32_t         ASP_id
		  )
{
  Sua_container msg;
  Sua_syntax_error_struct error;
  int error_value = 0;
  int string_size, datalen;
  signed int sctp_assoc_id;
  short stream_id = 0;
  int delivery_type, result;

  // init the message
  msg.sua_init();

  // fill in the main sua header
  msg.sua_prim.hdr_msg_class = sua_aspsm;
  msg.sua_prim.hdr_msg_type.aspsm = aspsm_up;
  msg.sua_prim.ASP_id_pres = ASP_id_pres;
  if ( ASP_id_pres)
    msg.sua_prim.ASP_id = ASP_id;
 
  msg.sua_prim.asp_cap_pres = FALSE;
  msg.sua_prim.info_pres = FALSE;
  
  // encode the SUA unitdata message
  error = msg.sua_encode();
  string_size = msg.sua_msg.size();
  
  delivery_type = SCTP_UNORDERED_DELIVERY;

  sctp_assoc_id =  sua.AssocDB.instance[Sua_assoc_id].SCTP_assoc_id;

  /* does association exist? */
  if (sctp_assoc_id > 0)
    {
      // send data to SCTP
      char* databuf = new char[msg.sua_msg.length()];
      msg.sua_msg.copy(databuf, msg.sua_msg.length());
      datalen = msg.sua_msg.length();

      /* yes it does, continue, no problem, send the msg */
#ifdef DEBUG
      // display byte array
      display_byte_array(databuf , msg.sua_msg.length());
#endif
      char logstring[100];
      sprintf(logstring, "SUA encoded message, ready for being send to SCTP assoc %d", sctp_assoc_id);
      event_log("sua_asp_mgnt.c",logstring);
      log_byte_array("sua_asp_mgnt.c", databuf,msg.sua_msg.length());
      
      result = sctp_send ( sctp_assoc_id,
			   stream_id,
			   (unsigned char *) databuf,
			   datalen,
			   htonl(SUA_PPI),
			   SCTP_USE_PRIMARY,
			   SCTP_NO_CONTEXT,
			   SCTP_INFINITE_LIFETIME,
			   delivery_type,
			   SCTP_BUNDLING_DISABLED
			   );

      error_value = result;
      delete databuf;
#ifdef DEBUG
      cout << "sua_asp_mgnt.c:result ASPUP sctp send = "<< result << "\n";
#endif
    }
  
  return(error_value);
}

/***********************************************************************/
/* sua_send_ASPUP_ACK                                                  */
/***********************************************************************/
int sua_send_ASPUP_ACK( unsigned int     Sua_assoc_id,
			boolean          ASP_id_pres,
			uint32_t         ASP_id
		      )
{
  Sua_container msg;
  Sua_syntax_error_struct error;
  int error_value = 0;
  int string_size, datalen;
  signed int sctp_assoc_id;
  short stream_id = 0;
  int delivery_type, result;
  
  // init the message
  msg.sua_init();

  // fill in the main sua header
  msg.sua_prim.hdr_msg_class = sua_aspsm;
  msg.sua_prim.hdr_msg_type.aspsm = aspsm_up_ack;
  /* rest is optional */
  msg.sua_prim.ASP_id_pres = ASP_id_pres;
  if ( ASP_id_pres)
    msg.sua_prim.ASP_id = ASP_id;
    
  msg.sua_prim.info_pres = FALSE;
  
  // encode the SUA unitdata message
  error = msg.sua_encode();
  string_size = msg.sua_msg.size();
  
  delivery_type = SCTP_UNORDERED_DELIVERY;

  sctp_assoc_id =  sua.AssocDB.instance[Sua_assoc_id].SCTP_assoc_id;

  /* does association exist? */
  if (sctp_assoc_id > 0)
    {
      // send data to SCTP
      char* databuf = new char[msg.sua_msg.length()];
      msg.sua_msg.copy(databuf, msg.sua_msg.length());
      datalen = msg.sua_msg.length();

      /* yes it does, continue, no problem, send the msg */
#ifdef DEBUG
      // display byte array
      display_byte_array(databuf , msg.sua_msg.length());
#endif
      char logstring[100];
      sprintf(logstring, "SUA encoded message, ready for being send to SCTP assoc %d", sctp_assoc_id);
      event_log("sua_asp_mgnt.c",logstring);
      log_byte_array("sua_asp_mgnt.c", databuf,msg.sua_msg.length());
      
      result = sctp_send ( sctp_assoc_id,
			   stream_id,
			   (unsigned char *) databuf,
			   datalen,
			   htonl(SUA_PPI),
			   SCTP_USE_PRIMARY,
			   SCTP_NO_CONTEXT,
			   SCTP_INFINITE_LIFETIME,
			   delivery_type,
			   SCTP_BUNDLING_DISABLED
			   );

      error_value = result;
      delete databuf;
#ifdef DEBUG
      cout << "sua_asp_mgnt.c:result ASPUP_ACK sctp send = "<< result << "\n";
#endif
    }
  
  return(error_value);
}

/***********************************************************************/
/* sua_send_ASPDOWN                                                    */
/***********************************************************************/
int sua_send_ASPDOWN( unsigned int     Sua_assoc_id
		    )
{
  Sua_container msg;
  Sua_syntax_error_struct error;
  int error_value = 0;
  int string_size, datalen;
  signed int sctp_assoc_id;
  short stream_id = 0;
  int delivery_type, result;

  // init the message
  msg.sua_init();

  // fill in the main sua header
  msg.sua_prim.hdr_msg_class = sua_aspsm;
  msg.sua_prim.hdr_msg_type.aspsm = aspsm_down;
 
  msg.sua_prim.asp_cap_pres = FALSE;
  msg.sua_prim.info_pres = FALSE;
  
  // encode the SUA unitdata message
  error = msg.sua_encode();
  string_size = msg.sua_msg.size();
  
  delivery_type = SCTP_UNORDERED_DELIVERY;

  sctp_assoc_id =  sua.AssocDB.instance[Sua_assoc_id].SCTP_assoc_id;

  /* does association exist? */
  if (sctp_assoc_id > 0)
    {
      // send data to SCTP
      char* databuf = new char[msg.sua_msg.length()];
      msg.sua_msg.copy(databuf, msg.sua_msg.length());
      datalen = msg.sua_msg.length();

      /* yes it does, continue, no problem, send the msg */
#ifdef DEBUG
      // display byte array
      display_byte_array(databuf , msg.sua_msg.length());
#endif
      char logstring[100];
      sprintf(logstring, "SUA encoded message, ready for being send to SCTP assoc %d", sctp_assoc_id);
      event_log("sua_asp_mgnt.c",logstring);
      log_byte_array("sua_asp_mgnt.c", databuf,msg.sua_msg.length());
      
      result = sctp_send ( sctp_assoc_id,
			   stream_id,
			   (unsigned char *) databuf,
			   datalen,
			   htonl(SUA_PPI),
			   SCTP_USE_PRIMARY,
			   SCTP_NO_CONTEXT,
			   SCTP_INFINITE_LIFETIME,
			   delivery_type,
			   SCTP_BUNDLING_DISABLED
			   );

      error_value = result;
      delete databuf;
#ifdef DEBUG
      cout << "sua_asp_mgnt.c:result ASPDOWN sctp send = "<< result << "\n";
#endif
    }
  
  return(error_value);
}


/***********************************************************************/
/* sua_send_ASPDOWN_ACK                                                */
/***********************************************************************/
int sua_send_ASPDOWN_ACK( unsigned int     Sua_assoc_id
		        )
{
  Sua_container msg;
  Sua_syntax_error_struct error;
  int error_value = 0;
  int string_size, datalen;
  signed int sctp_assoc_id;
  short stream_id = 0;
  int delivery_type, result;
  
  // init the message
  msg.sua_init();

  // fill in the main sua header
  msg.sua_prim.hdr_msg_class = sua_aspsm;
  msg.sua_prim.hdr_msg_type.aspsm = aspsm_down_ack;
  msg.sua_prim.info_pres = FALSE;
  
  // encode the SUA unitdata message
  error = msg.sua_encode();
  string_size = msg.sua_msg.size();
  
  delivery_type = SCTP_UNORDERED_DELIVERY;

  sctp_assoc_id =  sua.AssocDB.instance[Sua_assoc_id].SCTP_assoc_id;

  /* does association exist? */
  if (sctp_assoc_id > 0)
    {
      // send data to SCTP
      char* databuf = new char[msg.sua_msg.length()];
      msg.sua_msg.copy(databuf, msg.sua_msg.length());
      datalen = msg.sua_msg.length();

      /* yes it does, continue, no problem, send the msg */
#ifdef DEBUG
      // display byte array
      display_byte_array(databuf , msg.sua_msg.length());
#endif
      char logstring[100];
      sprintf(logstring, "SUA encoded message, ready for being send to SCTP assoc %d", sctp_assoc_id);
      event_log("sua_asp_mgnt.c",logstring);
      log_byte_array("sua_asp_mgnt.c", databuf,msg.sua_msg.length());
      
      result = sctp_send ( sctp_assoc_id,
			   stream_id,
			   (unsigned char *) databuf,
			   datalen,
			   htonl(SUA_PPI),
			   SCTP_USE_PRIMARY,
			   SCTP_NO_CONTEXT,
			   SCTP_INFINITE_LIFETIME,
			   delivery_type,
			   SCTP_BUNDLING_DISABLED
			   );

      error_value = result;
      delete databuf;
#ifdef DEBUG
      cout << "sua_asp_mgnt.c:result ASPDOWN_ACK sctp send = "<< result << "\n";
#endif
    }
  
  return(error_value);
}

/***********************************************************************/
/* sua_send_BEAT                                                       */
/***********************************************************************/
int sua_send_BEAT( unsigned int     Sua_assoc_id
		 )
{
  Sua_container msg;
  Sua_syntax_error_struct error;
  int error_value = 0;
  int string_size, datalen;
  signed int sctp_assoc_id;
  short stream_id = 0;
  int delivery_type, result;

  // init the message
  msg.sua_init();

  // fill in the main sua header
  msg.sua_prim.hdr_msg_class = sua_aspsm;
  msg.sua_prim.hdr_msg_type.aspsm = aspsm_beat;
  msg.sua_prim.hb_data_pres = FALSE;
  
  // encode the SUA unitdata message
  error = msg.sua_encode();
  string_size = msg.sua_msg.size();
  
  delivery_type = SCTP_UNORDERED_DELIVERY;

  sctp_assoc_id =  sua.AssocDB.instance[Sua_assoc_id].SCTP_assoc_id;

  /* does association exist? */
  if (sctp_assoc_id > 0)
    {
      // send data to SCTP
      char* databuf = new char[msg.sua_msg.length()];
      msg.sua_msg.copy(databuf, msg.sua_msg.length());
      datalen = msg.sua_msg.length();

      /* yes it does, continue, no problem, send the msg */
#ifdef DEBUG
      // display byte array
      display_byte_array(databuf , msg.sua_msg.length());
#endif
      char logstring[100];
      sprintf(logstring, "SUA encoded message, ready for being send to SCTP assoc %d", sctp_assoc_id);
      event_log("sua_asp_mgnt.c",logstring);
      log_byte_array("sua_asp_mgnt.c", databuf,msg.sua_msg.length());
      
      result = sctp_send ( sctp_assoc_id,
			   stream_id,
			   (unsigned char *) databuf,
			   datalen,
			   htonl(SUA_PPI),
			   SCTP_USE_PRIMARY,
			   SCTP_NO_CONTEXT,
			   SCTP_INFINITE_LIFETIME,
			   delivery_type,
			   SCTP_BUNDLING_DISABLED
			   );

      error_value = result;
      delete databuf;
#ifdef DEBUG
      cout << "sua_asp_mgnt.c:result BEAT sctp send = "<< result << "\n";
#endif
    }
  
  return(error_value);
}

/***********************************************************************/
/* sua_send_BEAT_ACK                                                   */
/***********************************************************************/
int sua_send_BEAT_ACK( unsigned int     Sua_assoc_id,
		       bool             HB_data_present,
		       string           HB_data
		     )
{
  Sua_container msg;
  Sua_syntax_error_struct error;
  int error_value = 0;
  int string_size, datalen;
  signed int sctp_assoc_id;
  short stream_id = 0;
  int delivery_type, result;
  
  // init the message
  msg.sua_init();

  // fill in the main sua header
  msg.sua_prim.hdr_msg_class = sua_aspsm;
  msg.sua_prim.hdr_msg_type.aspsm = aspsm_beat_ack;
  msg.sua_prim.hb_data_pres = HB_data_present; 
  if (HB_data_present)
    msg.sua_prim.hb_data = HB_data; 
  
  // encode the SUA unitdata message
  error = msg.sua_encode();
  string_size = msg.sua_msg.size();
  
  delivery_type = SCTP_UNORDERED_DELIVERY;

  sctp_assoc_id =  sua.AssocDB.instance[Sua_assoc_id].SCTP_assoc_id;

  /* does association exist? */
  if (sctp_assoc_id > 0)
    {
      // send data to SCTP
      char* databuf = new char[msg.sua_msg.length()];
      msg.sua_msg.copy(databuf, msg.sua_msg.length());
      datalen = msg.sua_msg.length();

      /* yes it does, continue, no problem, send the msg */
#ifdef DEBUG
      // display byte array
      display_byte_array(databuf , msg.sua_msg.length());
#endif
      char logstring[100];
      sprintf(logstring, "SUA encoded message, ready for being send to SCTP assoc %d", sctp_assoc_id);
      event_log("sua_asp_mgnt.c",logstring);
      log_byte_array("sua_asp_mgnt.c", databuf,msg.sua_msg.length());
      
      result = sctp_send ( sctp_assoc_id,
			   stream_id,
			   (unsigned char *) databuf,
			   datalen,
			   htonl(SUA_PPI),
			   SCTP_USE_PRIMARY,
			   SCTP_NO_CONTEXT,
			   SCTP_INFINITE_LIFETIME,
			   delivery_type,
			   SCTP_BUNDLING_DISABLED
			   );

      error_value = result;
      delete databuf;
#ifdef DEBUG
      cout << "sua_asp_mgnt.c:result BEAT_ACK sctp send = "<< result << "\n";
#endif
    }
  
  return(error_value);
}

/***********************************************************************/
/* sua_send_ASPAC                                                      */
/***********************************************************************/
int sua_send_ASPAC( unsigned int            Sua_assoc_id,
		    Sua_traffic_mode_type   traff_mode,
		    boolean                 routing_context_present,
		    uint32_t                routing_context
		  )
{
  Sua_container msg;
  Sua_syntax_error_struct error;
  int error_value = 0;
  int string_size, datalen;
  signed int sctp_assoc_id;
  short stream_id = 0;
  int delivery_type, result;

  // init the message
  msg.sua_init();

  // fill in the main sua header
  msg.sua_prim.hdr_msg_class = sua_asptm;
  msg.sua_prim.hdr_msg_type.asptm = asptm_act;
  /* traffic mode is optional mandatory */
  if (traff_mode == tmt_unknown)
    msg.sua_prim.traf_mode_pres = false;
  else {
    msg.sua_prim.traf_mode_pres = true;
    msg.sua_prim.traf_mode = traff_mode;
  }
 
  /* rest is optional */
  msg.sua_prim.rout_con_pres = routing_context_present;
  if (msg.sua_prim.rout_con_pres)
    msg.sua_prim.rout_con = routing_context;

  msg.sua_prim.info_pres = FALSE;
  
  // encode the SUA unitdata message
  error = msg.sua_encode();
  string_size = msg.sua_msg.size();
  
  delivery_type = SCTP_UNORDERED_DELIVERY;

  sctp_assoc_id =  sua.AssocDB.instance[Sua_assoc_id].SCTP_assoc_id;

  /* does association exist? */
  if (sctp_assoc_id > 0)
    {
      // send data to SCTP
      char* databuf = new char[msg.sua_msg.length()];
      msg.sua_msg.copy(databuf, msg.sua_msg.length());
      datalen = msg.sua_msg.length();

      /* yes it does, continue, no problem, send the msg */
#ifdef DEBUG
      // display byte array
      display_byte_array(databuf , msg.sua_msg.length());
#endif
      char logstring[100];
      sprintf(logstring, "SUA encoded message, ready for being send to SCTP assoc %d", sctp_assoc_id);
      event_log("sua_asp_mgnt.c",logstring);
      log_byte_array("sua_asp_mgnt.c", databuf,msg.sua_msg.length());
      
      result = sctp_send ( sctp_assoc_id,
			   stream_id,
			   (unsigned char *) databuf,
			   datalen,
			   htonl(SUA_PPI),
			   SCTP_USE_PRIMARY,
			   SCTP_NO_CONTEXT,
			   SCTP_INFINITE_LIFETIME,
			   delivery_type,
			   SCTP_BUNDLING_DISABLED
			   );

      error_value = result;
      delete databuf;
#ifdef DEBUG
      cout << "sua_asp_mgnt.c:result ASPAC sctp send = "<< result << "\n";
#endif
    }
  
  return(error_value);
}

/***********************************************************************/
/* sua_send_ASPAC_ACK                                                      */
/***********************************************************************/
int sua_send_ASPAC_ACK( unsigned int            Sua_assoc_id,
			boolean                 traffic_mode_present,
			Sua_traffic_mode_type   traffic_mode,
			boolean                 routing_context_present,
			uint32_t                routing_context
		      )
{
  Sua_container msg;
  Sua_syntax_error_struct error;
  int error_value = 0;
  int string_size, datalen;
  signed int sctp_assoc_id;
  short stream_id = 0;
  int delivery_type, result;
  
  // init the message
  msg.sua_init();

  // fill in the main sua header
  msg.sua_prim.hdr_msg_class = sua_asptm;
  msg.sua_prim.hdr_msg_type.asptm = asptm_act_ack;

  /* rest is optional */
  msg.sua_prim.traf_mode_pres = traffic_mode_present;
  if ( traffic_mode_present)
    msg.sua_prim.traf_mode = traffic_mode;
    
  msg.sua_prim.rout_con_pres = routing_context_present;
  if (routing_context_present)
      msg.sua_prim.rout_con = routing_context;

  msg.sua_prim.info_pres = FALSE;
  
  // encode the SUA unitdata message
  error = msg.sua_encode();
  string_size = msg.sua_msg.size();
  
  delivery_type = SCTP_UNORDERED_DELIVERY;

  sctp_assoc_id =  sua.AssocDB.instance[Sua_assoc_id].SCTP_assoc_id;

  /* does association exist? */
  if (sctp_assoc_id > 0)
    {
      // send data to SCTP
      char* databuf = new char[msg.sua_msg.length()];
      msg.sua_msg.copy(databuf, msg.sua_msg.length());
      datalen = msg.sua_msg.length();

      /* yes it does, continue, no problem, send the msg */
#ifdef DEBUG
      // display byte array
      display_byte_array(databuf , msg.sua_msg.length());
#endif
      char logstring[100];
      sprintf(logstring, "SUA encoded message, ready for being send to SCTP assoc %d", sctp_assoc_id);
      event_log("sua_asp_mgnt.c",logstring);
      log_byte_array("sua_asp_mgnt.c", databuf,msg.sua_msg.length());
      
      result = sctp_send ( sctp_assoc_id,
			   stream_id,
			   (unsigned char *) databuf,
			   datalen,
			   htonl(SUA_PPI),
			   SCTP_USE_PRIMARY,
			   SCTP_NO_CONTEXT,
			   SCTP_INFINITE_LIFETIME,
			   delivery_type,
			   SCTP_BUNDLING_DISABLED
			   );

      error_value = result;
      delete databuf;
#ifdef DEBUG
      cout << "sua_asp_mgnt.c:result ASPAC_ACK sctp send = "<< result << "\n";
#endif
    }
  
  return(error_value);
}


/***********************************************************************/
/* sua_send_ASPINAC                                                    */
/***********************************************************************/
int sua_send_ASPINAC( unsigned int            Sua_assoc_id,
		      boolean                 routing_context_present,
		      uint32_t                routing_context
		    )
{
  Sua_container msg;
  Sua_syntax_error_struct error;
  int error_value = 0;
  int string_size, datalen;
  signed int sctp_assoc_id;
  short stream_id = 0;
  int delivery_type, result;
  
  // init the message
  msg.sua_init();

  // fill in the main sua header
  msg.sua_prim.hdr_msg_class = sua_asptm;
  msg.sua_prim.hdr_msg_type.asptm = asptm_inact;
  msg.sua_prim.rout_con_pres = routing_context_present;
  if (  msg.sua_prim.rout_con_pres)
    msg.sua_prim.rout_con = routing_context;

  msg.sua_prim.info_pres = FALSE;
  
  // encode the SUA unitdata message
  error = msg.sua_encode();
  string_size = msg.sua_msg.size();
  
  delivery_type = SCTP_UNORDERED_DELIVERY;

  sctp_assoc_id =  sua.AssocDB.instance[Sua_assoc_id].SCTP_assoc_id;

  /* does association exist? */
  if (sctp_assoc_id > 0)
    {
      // send data to SCTP
      char* databuf = new char[msg.sua_msg.length()];
      msg.sua_msg.copy(databuf, msg.sua_msg.length());
      datalen = msg.sua_msg.length();

      /* yes it does, continue, no problem, send the msg */
#ifdef DEBUG
      // display byte array
      display_byte_array(databuf , msg.sua_msg.length());
#endif
      char logstring[100];
      sprintf(logstring, "SUA encoded message, ready for being send to SCTP assoc %d", sctp_assoc_id);
      event_log("sua_asp_mgnt.c",logstring);
      log_byte_array("sua_asp_mgnt.c", databuf,msg.sua_msg.length());
      
      result = sctp_send ( sctp_assoc_id,
			   stream_id,
			   (unsigned char *) databuf,
			   datalen,
			   htonl(SUA_PPI),
			   SCTP_USE_PRIMARY,
			   SCTP_NO_CONTEXT,
			   SCTP_INFINITE_LIFETIME,
			   delivery_type,
			   SCTP_BUNDLING_DISABLED
			   );

      error_value = result;
      delete databuf;
#ifdef DEBUG
      cout << "sua_asp_mgnt.c:result ASPINAC sctp send = "<< result << "\n";
#endif
    }
  
  return(error_value);
}


/***********************************************************************/
/* sua_send_ASPINAC_ACK                                                */
/***********************************************************************/
int sua_send_ASPINAC_ACK( unsigned int            Sua_assoc_id,
			  boolean                 routing_context_present,
			  uint32_t                routing_context
		        )
{
  Sua_container msg;
  Sua_syntax_error_struct error;
  int error_value = 0;
  int string_size, datalen;
  signed int sctp_assoc_id;
  short stream_id = 0;
  int delivery_type, result;
  
  // init the message
  msg.sua_init();

  // fill in the main sua header
  msg.sua_prim.hdr_msg_class = sua_asptm;
  msg.sua_prim.hdr_msg_type.asptm = asptm_inact_ack;
  msg.sua_prim.rout_con_pres = routing_context_present;
  if (  msg.sua_prim.rout_con_pres)
    msg.sua_prim.rout_con = routing_context;

  msg.sua_prim.info_pres = FALSE;
  
  // encode the SUA unitdata message
  error = msg.sua_encode();
  string_size = msg.sua_msg.size();
  
  delivery_type = SCTP_UNORDERED_DELIVERY;

  sctp_assoc_id =  sua.AssocDB.instance[Sua_assoc_id].SCTP_assoc_id;

  /* does association exist? */
  if (sctp_assoc_id > 0)
    {
      // send data to SCTP
      char* databuf = new char[msg.sua_msg.length()];
      msg.sua_msg.copy(databuf, msg.sua_msg.length());
      datalen = msg.sua_msg.length();

      /* yes it does, continue, no problem, send the msg */
#ifdef DEBUG
      // display byte array
      display_byte_array(databuf , msg.sua_msg.length());
#endif
      char logstring[100];
      sprintf(logstring, "SUA encoded message, ready for being send to SCTP assoc %d", sctp_assoc_id);
      event_log("sua_asp_mgnt.c",logstring);
      log_byte_array("sua_asp_mgnt.c", databuf,msg.sua_msg.length());
      
      result = sctp_send ( sctp_assoc_id,
			   stream_id,
			   (unsigned char *) databuf,
			   datalen,
			   htonl(SUA_PPI),
			   SCTP_USE_PRIMARY,
			   SCTP_NO_CONTEXT,
			   SCTP_INFINITE_LIFETIME,
			   delivery_type,
			   SCTP_BUNDLING_DISABLED
			   );

      error_value = result;
      delete databuf;
#ifdef DEBUG
      cout << "sua_asp_mgnt.c:result ASPINAC_ACK sctp send = "<< result << "\n";
#endif
    }
  
  return(error_value);
}

/***********************************************************************/
/* sua_send_NOTIFY                                                     */
/***********************************************************************/
int sua_send_NOTIFY( unsigned int            Sua_assoc_id,
		     boolean                 ASP_id_pres,
		     uint32_t                ASP_id,
		     boolean                 routing_context_present,
		     uint32_t                routing_context,
		     uint8_t                 status_type,
		     uint8_t                 status_id
		   )
{
  Sua_container msg;
  Sua_syntax_error_struct error;
  int error_value = 0;
  int string_size, datalen;
  signed int sctp_assoc_id;
  short stream_id = 0;
  int delivery_type, result;

  // init the message
  msg.sua_init();

  // fill in the main sua header
  msg.sua_prim.hdr_msg_class = sua_mngt;
  msg.sua_prim.hdr_msg_type.mngt = mngt_notify;
  msg.sua_prim.status_pres = true;
  msg.sua_prim.status.status_type = status_type ;
  msg.sua_prim.status.status_ID = status_id;
  msg.sua_prim.ASP_id_pres = ASP_id_pres;
  msg.sua_prim.ASP_id = ASP_id;
  msg.sua_prim.rout_con_pres = routing_context_present;
  if (routing_context_present)
      msg.sua_prim.rout_con = routing_context;
  msg.sua_prim.info_pres = FALSE;
  
  // encode the SUA notify message
  error = msg.sua_encode();
  string_size = msg.sua_msg.size();
  
  delivery_type = SCTP_UNORDERED_DELIVERY;

  sctp_assoc_id =  sua.AssocDB.instance[Sua_assoc_id].SCTP_assoc_id;

  /* does association exist? */
  if (sctp_assoc_id > 0)
    {
      // send data to SCTP
      char* databuf = new char[msg.sua_msg.length()];
      msg.sua_msg.copy(databuf, msg.sua_msg.length());
      datalen = msg.sua_msg.length();

      /* yes it does, continue, no problem, send the msg */
#ifdef DEBUG
      // display byte array
      display_byte_array(databuf , msg.sua_msg.length());
#endif
      char logstring[100];
      sprintf(logstring, "SUA encoded message, ready for being send to SCTP assoc %d", sctp_assoc_id);
      event_log("sua_asp_mgnt.c",logstring);
      log_byte_array("sua_asp_mgnt.c", databuf,msg.sua_msg.length());
      
      result = sctp_send ( sctp_assoc_id,
			   stream_id,
			   (unsigned char *) databuf,
			   datalen,
			   htonl(SUA_PPI),
			   SCTP_USE_PRIMARY,
			   SCTP_NO_CONTEXT,
			   SCTP_INFINITE_LIFETIME,
			   delivery_type,
			   SCTP_BUNDLING_DISABLED
			   );

      error_value = result;
      delete databuf;
#ifdef DEBUG
      cout << "sua_asp_mgnt.c:result NOTIFY sctp send = "<< result << "\n";
#endif
    }
  
  return(error_value);
}


/***********************************************************************/
/* Receiving SUA Management msg                                        */
/***********************************************************************/
/***********************************************************************/
/* sua_process_ASPUP_msg                                               */
/***********************************************************************/
int process_ASPUP_msg ( unsigned int     sua_assoc_id,
			int	         local_sua_id,
			int	         remote_sua_id,
			Sua_container	 sua_asp_msg
		      )
{
  
  int error_value;

#ifdef DEBUG
  cout << "sua_asp_mgnt.c:ASPUP received, send back a ASPUP_ACK and set state to ASP-INACTIVE.\n";
#endif  

  sua.AssocDB.up(sua_assoc_id,0);
   
  error_value = sua_send_ASPUP_ACK( sua_assoc_id ,
				    sua_asp_msg.sua_prim.ASP_id_pres,
				    sua_asp_msg.sua_prim.ASP_id
				  );
#ifdef DEBUG
  cout << "sua_asp_mgnt.c:result send ASPUP_ACK = "<< error_value << "\n";
  cout << "sua_asp_mgnt.c:ASPUP processing state = "<<  sua.AssocDB.instance[sua_assoc_id].asp.status << "\n";
#endif
  
#ifdef IPSP_SINGLE
  error_value = sua_send_ASPAC( sua_assoc_id );
#ifdef DEBUG
  cout << "sua_asp_mgnt.c:result send ASPAC = "<< error_value << "\n";
  cout << "sua_asp_mgnt.c:ASPAC processing state = "<<  sua.AssocDB.instance[sua_assoc_id].asp.status << "\n";
#endif
#endif
  
  return(error_value);
}

/***********************************************************************/
/* sua_process_ASPUP_ACK_msg                                           */
/***********************************************************************/
int process_ASPUP_ACK_msg ( unsigned int         sua_assoc_id,
			    int	                 local_sua_id,
			    int	                 remote_sua_id,
			    Sua_container	 sua_asp_msg
			  )
{
  int error_value = 0;
#ifdef DEBUG
  cout << "sua_asp_mgnt.c:ASPUP_ACK received, looks like remote is inactive.\n";
#endif
  
  sua.AssocDB.up(sua_assoc_id,0);

#ifndef IPSP_SINGLE
  /* send activation to remote */
  error_value = sua_send_ASPAC( sua_assoc_id,
				tmt_unknown,
				true,
				1
			      );

#ifdef DEBUG
  cout << "sua_asp_mgnt.c:result send ASPAC = "<< error_value << "\n";
  cout << "sua_asp_mgnt.c:ASPUP_ACK processing state = "<<  sua.AssocDB.instance[sua_assoc_id].asp.status << "\n";
#endif  
#endif

  return(error_value);
} 

/***********************************************************************/
/* sua_process_ASPDOWN_msg                                             */
/***********************************************************************/
int process_ASPDOWN_msg ( unsigned int   sua_assoc_id,
			  int	         local_sua_id,
			  int	         remote_sua_id,
			  Sua_container	 sua_asp_msg
			)
{
  
  int error_value;

#ifdef DEBUG
  cout << "sua_asp_mgnt.c:ASPDOWN received, send back a ASPDOWN_ACK and set state to ASP-DOWN.\n";
#endif  

  sua.AssocDB.down(sua_assoc_id,0);
   
 
  error_value = sua_send_ASPDOWN_ACK( sua_assoc_id );
#ifdef DEBUG
  cout << "sua_asp_mgnt.c:result send ASPDOWN_ACK = "<< error_value << "\n";
#endif
  
  return(error_value);
}

/***********************************************************************/
/* sua_process_ASPDOWN_ACK_msg                                         */
/***********************************************************************/
int process_ASPDOWN_ACK_msg ( unsigned int         sua_assoc_id,
			      int	                 local_sua_id,
			      int	                 remote_sua_id,
			      Sua_container	 sua_asp_msg
			    )
{
  int error_value = 0;
#ifdef DEBUG
  cout << "sua_asp_mgnt.c:ASPDOWN_ACK received, remote looks down.\n";
#endif

   sua.AssocDB.down(sua_assoc_id,0);

  return(error_value);
} 

/***********************************************************************/
/* sua_process_BEAT_msg                                                */
/***********************************************************************/
int process_BEAT_msg ( unsigned int      sua_assoc_id,
		       int	         local_sua_id,
		       int	         remote_sua_id,
		       Sua_container	 sua_asp_msg
		     )
{
  
  int error_value;

#ifdef DEBUG
  cout << "sua_asp_mgnt.c:HEARTBEAT received, send back a HEARTBEAT_ACK.\n";
#endif  
 
  error_value = sua_send_BEAT_ACK( sua_assoc_id,
				   sua_asp_msg.sua_prim.hb_data_pres,
				   sua_asp_msg.sua_prim.hb_data
				   );
#ifdef DEBUG
  cout << "sua_asp_mgnt.c:result send HEARTBEAT_ACK = "<< error_value << "\n";
#endif
  
  return(error_value);
}

/***********************************************************************/
/* sua_process_BEAT_ACK_msg                                            */
/***********************************************************************/
int process_BEAT_ACK_msg ( unsigned int          sua_assoc_id,
			   int	                 local_sua_id,
			   int	                 remote_sua_id,
			   Sua_container	 sua_asp_msg
			 )
{
  int error_value = 0;
#ifdef DEBUG
  cout << "sua_asp_mgnt.c:HEARTBEAT_ACK received, nothing further to do.\n";
#endif
  return(error_value);
} 

/***********************************************************************/
/* sua_process_ASPAC_msg                                               */
/***********************************************************************/
int process_ASPAC_msg ( unsigned int     sua_assoc_id,
			int	         local_sua_id,
			int	         remote_sua_id,
			Sua_container	 sua_asp_msg
		      )
{
  bool holdtraffic_pres = FALSE;
  int error_value;
  tcb_Sua_msg_elem   sua_msg;
  int datalen;
  signed int sctp_assoc_id;
  int result;

#ifdef DEBUG
  cout << "sua_asp_mgnt.c:ASPAC received, send back a ASPAC_ACK and set state to ASP-ACTIVE.\n";
#endif  

  if (sua_asp_msg.sua_prim.traf_mode_pres)
    holdtraffic_pres = sua.AssocDB.activate(sua_assoc_id,
					  sua_asp_msg.sua_prim.traf_mode); 
  else
    holdtraffic_pres = sua.AssocDB.activate(sua_assoc_id,0);

  error_value = sua_send_ASPAC_ACK( sua_assoc_id,
				    sua_asp_msg.sua_prim.traf_mode_pres,
				    sua_asp_msg.sua_prim.traf_mode,
				    sua_asp_msg.sua_prim.rout_con_pres,
				    sua_asp_msg.sua_prim.rout_con
				  );

#ifdef DEBUG
  cout << "sua_asp_mgnt.c:result ASPAC_ACK send = "<< error_value << "\n";
  cout << "sua_asp_mgnt.c:ASPAC processing state = "<<  sua.AssocDB.instance[sua_assoc_id].asp.status << "\n";
#endif

  if (holdtraffic_pres)
    {
      /* send all stored msg for this association to remote side */
      /* should be looping to get all waiting messages */
      /* - get the saved msg ansd send it on the association that is setup */
      sua_msg = msg_store.get_msg ( sua_assoc_id );

      while ( sua_msg.valid)
	{
	  char* databuf = new char[sua_msg.byte.length()];
	  sua_msg.byte.copy(databuf, sua_msg.byte.length());
	  datalen = sua_msg.byte.length();

	  /* msg retrieved and copied, may now remove it from queue */
	  msg_store.delete_msg ( sua_assoc_id );

	  sctp_assoc_id = sua.AssocDB.instance[sua_assoc_id].SCTP_assoc_id;

#ifdef DEBUG
	  display_byte_array(databuf , sua_msg.byte.length());
#endif
	  char logstring[100];
	  sprintf(logstring, "SUA encoded message, ready for being send to SCTP assoc %d", sctp_assoc_id);
	  event_log("sua_distribution.c",logstring);
	  log_byte_array("sua_distribution.c", databuf,sua_msg.byte.length());
	  
	  result = sctp_send ( sctp_assoc_id,
			       sua_msg.stream_id,
			       (unsigned char *) databuf,
			       datalen,
			       SUA_PPI,
			       SCTP_USE_PRIMARY,
			       SCTP_NO_CONTEXT,
			       SCTP_INFINITE_LIFETIME,
			       sua_msg.delivery_type,
			       SCTP_BUNDLING_DISABLED
			     );

	  delete databuf;
#ifdef DEBUG
	  cout << "sua_asp_mgnt.c:result sctp send = "<< result << "\n";
#endif
	   /* get the next saved msg ansd send if any ?  */
	  sua_msg = msg_store.get_msg ( sua_assoc_id );

	}

    }
  return(error_value);
}

/***********************************************************************/
/* sua_process_ASPAC_ACK_msg                                           */
/***********************************************************************/
int process_ASPAC_ACK_msg ( unsigned int         sua_assoc_id,
			    int	                 local_sua_id,
			    int	                 remote_sua_id,
			    Sua_container	 sua_asp_msg
			  )
{
  int error_value = 0;
  bool holdtraffic_pres = FALSE;

  /*#ifdef SG_ASP_MODE*/
  if (sua_asp_msg.sua_prim.traf_mode_pres)
    holdtraffic_pres = sua.AssocDB.activate(sua_assoc_id,
					  sua_asp_msg.sua_prim.traf_mode); 
  else
    holdtraffic_pres = sua.AssocDB.activate(sua_assoc_id,0);

  /*#endif*/

#ifdef DEBUG
  cout << "sua_asp_mgnt.c:ASPAC_ACK received, nothing further to do.\n";
#endif  
  return(error_value);
} 

/***********************************************************************/
/* sua_process_ASPINAC_msg                                             */
/***********************************************************************/
int process_ASPINAC_msg ( unsigned int     sua_assoc_id,
			  int	         local_sua_id,
			  int	         remote_sua_id,
			  Sua_container	 sua_asp_msg
		        )
{
  int error_value;
  tcb_Sua_msg_elem   sua_msg;

#ifdef DEBUG
  cout << "sua_asp_mgnt.c:ASPINAC received, send back a ASPINAC_ACK and set state to ASP-INACTIVE.\n";
#endif  
  sua.AssocDB.deactivate(sua_assoc_id,0);

  error_value = sua_send_ASPINAC_ACK( sua_assoc_id,
				      sua_asp_msg.sua_prim.rout_con_pres,
				      sua_asp_msg.sua_prim.rout_con
				    );
#ifdef DEBUG
  cout << "sua_asp_mgnt.c:result ASPINAC_ACK send = "<< error_value << "\n";
#endif

  return(error_value);
}

/***********************************************************************/
/* sua_process_ASPINAC_ACK_msg                                         */
/***********************************************************************/
int process_ASPINAC_ACK_msg ( unsigned int         sua_assoc_id,
			      int	           local_sua_id,
			      int	           remote_sua_id,
			      Sua_container	   sua_asp_msg
			    )
{
  int error_value = 0;

  sua.AssocDB.deactivate(sua_assoc_id,0);

#ifdef DEBUG
  cout << "sua_asp_mgnt.c:ASPINAC_ACK received, nothing further to do.\n";
#endif  
  return(error_value);
} 

/***********************************************************************/
/* Asp_mngt_standby                                                    */
/***********************************************************************/
void  Asp_mngt_standby ( unsigned int         sua_AS_id,
			 unsigned int         sua_asp_id,
			 short                mode
		       )
{
  sua.ApplicServ.override_ASP( sua_asp_id,
			       sua_AS_id,
			       mode
			     );
}

/***********************************************************************/
/* Asp_activate_override                                               */
/***********************************************************************/
void asp_activate_override ( unsigned int asp_sua_assoc_id )
{
  sua.AssocDB.activate(asp_sua_assoc_id,0);
}

/***********************************************************************/
/* Asp_asp_deactivate                                                  */
/***********************************************************************/
void asp_deactivate ( unsigned int asp_sua_assoc_id)
{
  sua.AssocDB.deactivate(asp_sua_assoc_id,0);
}


//end of module sua_asp_mgnt.c++













