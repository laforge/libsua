/***************************************************************************
                          sua_syntax.h  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: sua_syntax.h,v 1.3 2003/08/26 11:43:57 p82609 Exp $
 *
 * SUA implementation according to SUA draft issue 5.
 *
 * Author(s): Gery Verwimp
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * The alternative comment
 * inspiration : Kathleen
 * "Computers/networks must be female, because if they were male, "
 * "they would be perfectably predictable and computers/networks are "
 * "never predictable. If you do not believe this, just ask any women."
 *
 * Purpose: This header-file defines the SUA syntax class for SUA 
 *          primitives and messages, including the en/decoding routines.
 */

#ifndef SUA_SYNTAX_H
#define SUA_SYNTAX_H

#include "sua.h"
#include <string>

/********************************/
/* 0. SUA message header syntax */
/********************************/

enum Sua_version {
  Sua_unknown,
  Sua_version1
};

enum Sua_msg_class {
  sua_mngt,
  sua_reserved1,
  sua_ssnm,
  sua_aspsm,
  sua_asptm,
  sua_reserved5,
  sua_reserved6,
  sua_cl,
  sua_co,
  sua_rkm,
  sua_max
};

enum Sua_mngt_msg_type {
  mngt_error,
  mngt_notify,
  mngt_max
};

enum Sua_ssnm_msg_type {
  ssnm_reserved0,
  ssnm_duna,
  ssnm_dava,
  ssnm_daud,
  ssnm_scon,
  ssnm_dupu,
  ssnm_drst,
  ssnm_max
};

enum Sua_aspsm_msg_type {
  aspsm_reserved0,
  aspsm_up,
  aspsm_down,
  aspsm_beat,
  aspsm_up_ack,
  aspsm_down_ack,
  aspsm_beat_ack,
  aspsm_max
};

enum Sua_asptm_msg_type {
  asptm_reserved0,
  asptm_act,
  asptm_inact,
  asptm_act_ack,
  asptm_inact_ack,
  asptm_max
};

enum Sua_cl_msg_type {
  cl_reserved0,
  cl_data_transfer, /* unitdata */
  cl_data_response, /* unitdata service */
  cl_max
};

enum Sua_co_msg_type {
  co_reserved0,
  co_core,     /* connection request */
  co_coak,     /* connection confirm */
  co_coref,    /* connection refused */
  co_relre,    /* release request */
  co_relco,    /* release confirm */
  co_resco,    /* reset confirm */
  co_resre,    /* reset request */
  co_data,     /* data transfer, expedited data */
  co_data_ack, /* (expedited) data acknowledge */
  co_err,      /* PDU error */
  co_it,       /* inactivity test */
  co_max
};

enum Sua_rkm_msg_type {
  rkm_reserved0,
  rkm_reg_req,    /* registration request */
  rkm_reg_resp,   /* registration response */
  rkm_dereg_req,  /* deregistration request */
  rkm_dereg_resp, /* deregistration response */
  rkm_max
};

union Sua_msg_type {
  Sua_mngt_msg_type  mngt;
  Sua_ssnm_msg_type  ssnm;
  Sua_aspsm_msg_type aspsm;
  Sua_asptm_msg_type asptm;
  Sua_cl_msg_type    cl;
  Sua_co_msg_type    co;
  Sua_rkm_msg_type   rkm;
};

/***********************************/
/* 1. SUA management message modes */
/***********************************/

enum Sua_traffic_mode_type {
  tmt_unknown,
  tmt_override,
  tmt_loadshare,
  tmt_broadcast,
  tmt_max
};

enum Sua_error_code_type {
  ect_unknown,
  ect_invalid_version,
  ect_invalid_interface_id,
  ect_unsupported_msg_class,
  ect_unsupported_msg_type,
  ect_unsupported_traffic_handling_mode,
  ect_unexpected_msg,
  ect_protocol_error,
  ect_unsupported_interface_id_type, /* not used in SUA */
  ect_invalid_stream_id,
  ect_unassigned_TEI,                /* not used in SUA */
  ect_unrecognized_SAPI,             /* not used in SUA */
  ect_invalid_TEI_SAPI_comb,         /* not used in SUA */
  ect_refused_mngt_blocking,
  ect_ASP_id_required,
  ect_invalid_ASP_id,
  ect_invalid_routing_context,
  ect_invalid_parameter_value,
  ect_parameter_field_error,
  ect_unexpected_parameter,
  ect_destination_status_unknown,
  ect_invalid_network_appearance,
  ect_missing_parameter,
  ect_routing_key_change_refused,
  ect_invalid_loadsharing_label,
  ect_max
};

typedef struct {
  uint8_t status_type;
  uint8_t status_ID;
} Sua_status_struct;

/* ASP capabilities */

enum Sua_interworking_type {
  iw_none,  /* no interworking with SS7 */
  iw_asp,   /* IP endpoint ASP */
  iw_sg,    /* Signalling Gateway */
  iw_relay, /* IP relay point */
  iw_max
};

typedef struct {
  boolean  cl0_supp;
  boolean  cl1_supp;
  boolean  cl2_supp;
  boolean  cl3_supp;
  uint8_t  interworking;
} Sua_asp_cap_struct;

/*********************************/
/* 2. SUA protocol message modes */
/*********************************/

enum Sua_protocol_class {
  prot_class_0,
  prot_class_1,
  prot_class_2,
  prot_class_3,
  prot_max
};

typedef struct {
  Sua_protocol_class pcl;
  boolean return_option;
} Sua_protocol_class_struct;

/* The SUA address parameter contains following fields : 
   - routing indicator : see below
   - address indicator : for SS7 interworking, defines inclusion of SSN, PC or GT in SS7 address
   - address parameters : combination of GT, hostname, IPaddress, SSN, PC
*/

enum Sua_rout_ind {
  ri_reserved0,
  ri_route_GT,
  ri_route_PC_SSN,
  ri_route_hostname,
  ri_route_IP_SSN,
  ri_max
};

typedef struct {
  uint8_t translation_type;
  uint8_t num_plan;
  uint8_t nat_addr;
  uint8_t nr_of_digits;
  uint8_t digits[24];
} SCCP_GT_struct;

enum Sua_ip_addr_type {
  ip_reserved0,
  ip_v4,
  ip_v6,
  ip_max
};

union Ip_address {
  sockaddr_in  ipv4;
  sockaddr_in6 ipv6;
};

typedef struct {
  Sua_rout_ind  rout_ind;
  /* Flags for address indicator */
  boolean  ssn_incl;
  boolean  gt_incl;
  boolean  pc_incl;
  /* Indication of elements present */
  boolean  pc_pres;
  boolean  ip_addr_pres;
  boolean  gt_pres;
  boolean  hostname_pres;
  /* An SSN must always be present on the primitive interface, even if unknown (=0) */
  uint8_t  ssn;
  int      pc;
  Sua_ip_addr_type ip_addr_type;
  Ip_address ip_addr;
  SCCP_GT_struct gt;
  string   hostname;
} Sua_address_struct;

/* The SCCP Cause parameter can contain the following :
   - return cause (in CLDR messages)
   - refusal cause (in COREF messages)
   - release cause (in RELRE messages)
   - reset cause (in RESRE messages)
   - error cause (in COERR messages)
*/
enum SCCP_cause_type {
  ctp_reserved0,
  ctp_return_cause,
  ctp_refusal_cause,
  ctp_release_cause,
  ctp_reset_cause,
  ctp_error_cause,
  ctp_max
};

typedef struct {
  SCCP_cause_type cause_type;
  uint16_t        cause_value;
} SCCP_cause_struct;

/* The sequence number parameter contains :
   - the sent and received seq numbers (0..127) for protocol class 3
   - the more data indicator for protocol classes 2 and 3
*/

typedef struct {
  uint8_t sent_seq_nr;
  uint8_t rcvd_seq_nr;
  boolean more_data;
} Sua_seq_nr_struct;

/* The segmentation parameter contains :
   - first segment indication
   - number of remaining segments (0..15)
   - segmentation reference (0..2~24-1)
*/

typedef struct {
  boolean  first;
  uint8_t  remain;
  uint32_t segm_ref;
} Sua_segm_struct;

/* The label parameter contains :
   - start of the label bits (0..31)
   - end of the label bits (0..31)
   - label value (0..2~16-1)
*/

typedef struct {
  uint8_t  start;
  uint8_t  end;
  uint16_t label_value;
} Sua_label_struct;

/* the affected Pointcode contains:
   - number of aff_pc in msg(max 10 )
   - pointcodes (0..n-1)
*/
typedef struct {
  uint8_t   num_aff_pc;
  uint32_t  pc[10];
} Sua_affected_pc_struct;

typedef struct {
  boolean  aff_pc_pres;
  boolean  SSN_pres;
  boolean  SMI_pres;
  boolean  cong_lvl_pres;
  Sua_affected_pc_struct aff_pc;
  uint8_t  SSN;
  uint32_t SMI;
  uint8_t  cong_lvl;
} Sua_ssnm_struct;

/* And now, we present to you the SUA container, mother of all interfaces */

typedef struct {
  /* 0. Mandatory header */
  Sua_version   hdr_version;
  Sua_msg_class hdr_msg_class;
  Sua_msg_type  hdr_msg_type;
  /* 1. Common parameter part presence */
  boolean data_pres;
  boolean info_pres;
  boolean rout_con_pres;
  boolean diag_info_pres;
  boolean hb_data_pres;
  boolean traf_mode_pres;
  boolean err_code_pres;
  boolean status_pres;
  boolean ASP_id_pres;
  /* 1. Common parameter part */
  string  data_string;
  string  info_string;
  uint32_t rout_con;
  string  diag_info;
  string  hb_data;
  Sua_traffic_mode_type traf_mode;
  Sua_error_code_type err_code;
  Sua_status_struct status;
  Sua_ssnm_struct  ssnm_parm;
  uint32_t ASP_id;
  /* 2. SUA specific parameter part presence */
  boolean netw_app_pres;
  boolean hop_count_pres;
  boolean source_addr_pres;
  boolean dest_addr_pres;
  boolean source_ref_pres;
  boolean dest_ref_pres;
  boolean SCCP_cause_pres;
  boolean seq_nr_pres;
  boolean rec_seq_nr_pres;
  boolean asp_cap_pres;
  boolean credit_pres;
  boolean importance_pres;
  boolean prot_class_pres;
  boolean seq_control_pres;
  boolean segm_pres;
  boolean TID_label_pres;
  boolean DRN_label_pres;
  /* 2. SUA specific parameter part */
  uint32_t netw_app;
  Sua_address_struct source_addr;
  Sua_address_struct dest_addr;
  uint32_t source_ref;
  uint32_t dest_ref;
  SCCP_cause_struct SCCP_cause;
  Sua_seq_nr_struct seq_nr;
  Sua_asp_cap_struct asp_cap;
  Sua_protocol_class_struct prot_class;
  uint8_t  hop_count;
  uint8_t  rec_seq_nr;
  uint8_t  credit;
  uint8_t  importance;
  uint32_t seq_control;
  Sua_segm_struct segm;
  Sua_label_struct TID_label;
  Sua_label_struct DRN_label;
} Sua_primitive_struct;

typedef struct {
  int    msg_index;  // position in transfer syntax
  boolean no_error;
  string error_text; // error text, specifying details
} Sua_syntax_error_struct;

class Sua_container {
 public:
  Sua_primitive_struct sua_prim;
  string sua_msg;
  void sua_init ();
  Sua_syntax_error_struct sua_encode ();
  Sua_syntax_error_struct sua_decode (); 
};

#endif  // SUA_SYNTAX_H	






