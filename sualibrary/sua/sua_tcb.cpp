/***************************************************************************
                          sua_tcb.cpp  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: sua_tcb.cpp,v 1.1 2003/01/14 14:15:37 p82609 Exp $
 *
 * SUA implementation according to SUA draft issue 6.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * The alternative comment
 * inspiration : Daphne
 * "Listen very carefully, I shall say this only once:"
 * "a TCB, a TCB, my kingdom for a TCB"
 *
 * Purpose: This code-file defines the SUA Transaction Control Block(TCB) pool
 *          functions for(needed for SUA connection oriented):
 *          - initialising a single TCB
 *          - initialising the TCB pool
 *          - allocate a TCB
 *          - get a TCB
 *          - release a TCB
 *          And for the Saved SUA msg queue pool
 *          - add SUA msg to queue
 *          - get sua msg from queue
 *          - release the SUA msg from the queue     
 */

#include "sua_debug.h"
#include "sua_tcb.h"
#include "sua_database.h"
#include "sua_logging.h"
#include "sua.h"

#include <cstdio>
#include <iostream>
#include <cstdlib>
#include <string>

#include <netinet/in_systm.h>
#include <netinet/ip.h>         /* includes <netinet/in.h> also ! */
#include <netinet/ip_icmp.h>
#include <netdb.h>
#include <arpa/inet.h>

using namespace std;

/* sua SCOC tcb pool */ 
tcb_Sua_TCB_arr  tcb_pool;

/* sua temporary message store */
tcb_Sua_msgqueue_pool  msg_store; 

/***********************************************************************/
/* Sua SCOC TCB: initialise TCB element                                */
/***********************************************************************/
void tcb_Sua_TCB_str::init_TCB_elem()
{
  state =  scoc_idle;
  Dest_LR = 0;
  Source_LR = 0;
  User_ref_id = 0;
  sctp_Association_id = 0;
  scoc_tcb_id = 0;
  prcl_class =  class0 ;
  return_option = FALSE;
}

/***********************************************************************/
/* Sua SCOC TCB arr: initialise TCB pool                               */
/***********************************************************************/
void tcb_Sua_TCB_arr::init_TCB_pool()
{
  cout << "Initialising ?? \n";
  last_allocated_tcb = 0;
}


/***********************************************************************/
/* Sua SCOC TCB arr: allocate a TCB                                    */
/***********************************************************************/
tcb_Sua_TCB_str  *tcb_Sua_TCB_arr:: allocate_TCB(unsigned int &local_ref){

  last_allocated_tcb++;
  // allocate the TCB
  tcb_Sua_TCB_str new_tcb;
  new_tcb.init_TCB_elem();
  tcb_sua_pair_str tcb_ref_pair(last_allocated_tcb,new_tcb);
  
  tcb_sua_map_iterator_str p = tcb.insert(tcb_ref_pair);
 
  if (p.second)
    {
      char logstring[100];
      sprintf(logstring, "TCB %d allocated",last_allocated_tcb);
      event_log("sua_tcb.c",logstring);
    }
  else
    {
      char logstring[100];
      sprintf(logstring, "No TCB allocated, damm");
      event_log("sua_tcb.c",logstring);
    } 
 
  local_ref = last_allocated_tcb;
  cout << "tcb local ref = " << local_ref << "\n";
  return(&tcb[last_allocated_tcb]);
}

/***********************************************************************/
/* Sua SCOC TCB arr: get address of TCB                                */
/***********************************************************************/
tcb_Sua_TCB_str  *tcb_Sua_TCB_arr:: get_tcb(unsigned int local_ref) {

  return(&tcb[local_ref]);
}
  
/***********************************************************************/
/* Sua SCOC TCB arr: release the TCB                                   */
/***********************************************************************/
void tcb_Sua_TCB_arr:: release_TCB(unsigned int local_reference){

  int count = tcb.erase(local_reference);
 
  if (count != 0)
    {
      char logstring[100];
      sprintf(logstring, "TCB instances %d of LR %d Released : ",count,local_reference);
      event_log("sua_tcb.c",logstring);
    }
  else
    {
      char logstring[100];
      sprintf(logstring, "TCB instance not released");
      event_log("sua_tcb.c",logstring);
    }
  
}

/***********************************************************************/
/* Sua_msg : save msg                                                  */
/***********************************************************************/
void tcb_Sua_msgqueue_pool:: add_msg( unsigned int       sua_assoc_idx,
				      tcb_Sua_msg_elem   sua_msg
				    )
{
  sua_msg.valid = true;
  msg_store.instance[sua_assoc_idx].push(sua_msg);
}

/***********************************************************************/
/* Sua_msg : get msg                                                   */
/***********************************************************************/
tcb_Sua_msg_elem  tcb_Sua_msgqueue_pool:: get_msg( unsigned int sua_assoc_idx )
{
  tcb_Sua_msg_elem   temp_sua_msg;

  temp_sua_msg.valid = false;
  if ( !msg_store.instance[sua_assoc_idx].empty() )
    {
      return( msg_store.instance[sua_assoc_idx].front());
    }
  else
    {
      
      return(temp_sua_msg);
    }
}

/***********************************************************************/
/* Sua_msg : Delete msg                                                */
/***********************************************************************/
void tcb_Sua_msgqueue_pool:: delete_msg( unsigned int sua_assoc_idx )
{
  if ( !msg_store.instance[sua_assoc_idx].empty() )
    msg_store.instance[sua_assoc_idx].pop();
}

// end of module sua_tcb.c




