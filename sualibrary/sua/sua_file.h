/***************************************************************************
                          sua_file.h  -  description
                             -------------------
    begin                : Tue Jan 8 2002
    copyright            : (C) 2002 by Lode Coene
    email                : lode.coene@siemens.atea.be
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
/*
 *  $Id: sua_file.h,v 1.1 2003/01/14 14:15:37 p82609 Exp $
 *
 * SUA implementation according to SUA draft issue 6.
 *
 * Author(s): Lode Coene
 *            
 *
 * Copyright (C) 2001 by Siemens Atea, Herentals, Belgium.
 *
 * Realized in co-operation between Siemens Atea and 
 * Siemens AG, Munich, Germany.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Contact: gery.verwimp@siemens.atea.be
 *          lode.coene@siemens.atea.be
 *
 * The alternative comment
 * inspiration : Annemie 
 * ""
 *
 * Purpose: This header-file defines the SUA file access functions for:
 *          - get a argument in the line(delimited by a space)
 *          - read the sua file(containing arguments for the SUA database)     
 */

#ifndef SUA_FILE_H
#define SUA_FILE_H


#include <string>

using namespace std;

int read_sua_file(string filename, 
		  db_Sua_LocalList&        local_sua, 
		  db_Sua_RemoteList&       remote_sua,
		  db_Sua_AssociationList&  Assoc_sua,
		  db_Sua_NameList&         NameDB_sua,
		  db_Sua_ASList&           ApplicServ_sua
		 );


int read_sua_conf_file( string              filename, 
		        db_Sua_DatabaseList &sua
			);

#endif  // SUA_FILE_H	

// end of module sua_file.h
